
# python3中的模块

# Python 模块(Module)，是一个 Python 文件，以 .py 结尾，包含了 Python 对象定义和Python语句。
# 模块让你能够有逻辑地组织你的 Python 代码段。
# 把相关的代码分配到一个模块里能让你的代码更好用，更易懂。
# 模块能定义函数，类和变量，模块里也能包含可执行的代码。

def print_func(par):
   print("Hello : ", par)
   return

def  function1(name):
   return name

print(function1("哈哈哈，这是一个函数"))
# print_func("python")
print("这里执行了一次")