
list = ["a","b","c","d","a"]
# it 为一个迭代器
it = iter(list)
print(next(it))
print(next(it))

list=[1,2,3,4]
it = iter(list)    # 创建迭代器对象
for x in it:
    print(x)

import sys  # 引入 sys 模块

list = [1, 2, 3, 4]
it = iter(list)  # 创建迭代器对象

# while True:
#     try:
#         print(next(it))
#     except StopIteration:
#         sys.exit()

'''
生成器 generator
为什么要有生成器
列表所有数据都在内存中，如果有海量数据的话将会非常耗内存。
如：仅仅需要访问前面几个元素，那后面绝大多数元素占用的空间都白白浪费了。
如果列表元素按照某种算法推算出来，那我们就可以在循环的过程中不断推算出后续的元素，这样就不必创建完整的list，从而节省大量的空间。
简单一句话：我又想要得到庞大的数据，又想让它占用空间少，那就用生成器！
'''

ls = [x for x in range(10)]
generator1 =(x for x in range(10)) #这是一个生成器generator
print(ls)
print(generator1)
print(next(generator1))
while True:
    try:
        x = next(generator1)
        print(x)
    except StopIteration as e:
        print("error")
        break
