# 当一个模块被导入到一个脚本，模块顶层部分的代码只会被执行一次。
# 因此，如果你想重新执行模块里顶层部分的代码，可以用 reload() 函数。该函数会重新导入之前导入过的模块。语法如下：
# 以后每次用到它的时候都用它的别名代替它，这时就需要用到import…as
from importlib import reload
from Introduction.firstMoudle import print_func
import Introduction.firstMoudle as firstMoudle
from base.numTest import *


print(print_func("java"))

print(getNumber(111))

reload(firstMoudle)

