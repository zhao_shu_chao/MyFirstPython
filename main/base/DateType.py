# Python 标准数据类型学习

# 标准数据类型
# Python3 中有六个标准的数据类型：

# Number（数字）
# String（字符串）
# List（列表）  list1 = [1, 2, 3, 4, 5, 6]
# Tuple（元组）  tuple = ('abcd', 786, 2.23, 'runoob', 70.2,"runoob")
# Set（集合） student = {'Tom', 'Jim', 'Mary', 'Tom', 'Jack', 'Rose'}
# Dictionary（字典）

# Python3 的六个标准数据类型中：
# 不可变数据（3 个）：Number（数字）、String（字符串）、Tuple（元组）；
# 可变数据（3 个）：List（列表）、Dictionary（字典）、Set（集合）。






# Number（数字）
# Python3 支持 int、float、bool、complex（复数）。
# 在Python 3里，只有一种整数类型 int，表示为长整型，没有 python2 中的 Long。
# 像大多数语言一样，数值类型的赋值和计算都是很直观的。
# 内置的 type() 函数可以用来查询变量所指的对象类型。
# 在 Python2 中是没有布尔型的，它用数字 0 表示 False，用 1 表示 True。
# 到 Python3 中，把 True 和 False 定义成关键字了，但它们的值还是 1 和 0，它们可以和数字相加。

a, b, c, d = 20, 5.5, True, 4+3j
print(type(a))
print(type(b))
print(type(c))
print(type(d))
# 输出结果为：<class 'int'> <class 'float'> <class 'bool'> <class 'complex'>

# 此外还可以用 isinstance 来判断：
# isinstance 和 type 的区别在于：
# type()不会认为子类是一种父类类型。
# isinstance()会认为子类是一种父类类型。
print(isinstance(a,int))



# 数值运算

# 注意：
# 1、Python可以同时为多个变量赋值，如a, b = 1, 2。
# 2、一个变量可以通过赋值指向不同类型的对象。
# 3、数值的除法包含两个运算符：/ 返回一个浮点数，// 返回一个整数。
# 4、在混合计算时，Python会把整型转换成为浮点数。
# 5、Python还支持复数，复数由实数部分和虚数部分构成，可以用a + bj,或者complex(a,b)表示， 复数的实部a和虚部b都是浮点型
a = 5 + 4   # 加法
print(a)    # 9
b = 4.3 - 2 # 减法
print(b)    # 2.3
c = 3 * 7   # 乘法
print(c)    # 21
d = 5 / 2   # 除法，得到一个浮点数
print(d)    # 2.5
e = 5 // 2  # 除法，得到一个整数
print(e)    # 2
f = 17 % 3  # 取余
print(f)    # 2
g = 2 ** 5  # 乘方
print(g)    #32







# String（字符串）
# Python中的字符串用单引号 ' 或双引号 " 括起来，同时使用反斜杠 \ 转义特殊字符。
# 使用三引号('''或""")可以指定一个多行字符串。
# 反斜杠可以用来转义，使用r可以让反斜杠不发生转义。。 如 r"this is a line with \n" 则\n会显示，并不是换行。
# 按字面意义级联字符串，如"this " "is " "string"会被自动转换为this is string。
# 字符串可以用 + 运算符连接在一起，用 * 运算符重复。
# Python中的字符串不能改变。
# Python 没有单独的字符类型，一个字符就是长度为 1 的字符串。
#
# 字符串的截取的语法格式如下：
# 变量[头下标:尾下标]
# 索引值以 0 为开始值，-1 为从末尾的开始位置。

# 从后面的索引： -6  -5  -4  -3  -2  -1
# 从前面的索引：  0   1   2   3   4   5
#               a   b   c   d   e   f

# str[x:y]:如果y是负数，则表示截取到倒数第（y+1）位,如果y是正数，则表示截取到第y位，如果是[x:]则表示从第(x+1)位截取到最后
str = 'python'
print('------------------------------')
print(len(str)) #len(str)表示字符串长度，一个字符的长度就是1
print(str)  # 输入字符串：python
print(str[0:-1]) # 输出第一个到倒数第二个的所有字符
print(str[0])  # 输出字符串第一个字符
print(str[2:5])  # 输出从第三个开始到第五个的字符
print(str[2:])  # 输出从第三个开始后的所有字符
print(str * 2)  # 输出字符串两次
print(str + '你好')  # 连接字符串
print('------------------------------')
print('hello\nrunoob')  # 使用反斜杠(\)+n转义特殊字符
print(r'hello\nrunoob')  # 在字符串前面添加一个 r，表示原始字符串，不会发生转义







# List（列表）
# List（列表） 是 Python 中使用最频繁的数据类型。
# 列表可以完成大多数集合类的数据结构实现。列表中元素的类型可以不相同，它支持数字，字符串甚至可以包含列表（所谓嵌套）。
# 列表是写在方括号 [] 之间、用逗号分隔开的元素列表。
# 和字符串一样，列表同样可以被索引和截取，列表被截取后返回一个包含所需元素的新列表。
#
# 列表截取的语法格式如下：
# 变量[头下标:尾下标]
# 索引值以 0 为开始值，-1 为从末尾的开始位置。
# 定义两个List:list,tinylist
list = [ 'abcd', 786 , 2.23, 'runoob', 70.2,70.2 ]
tinylist = [123, 'runoob']
print (list)            # 输出完整列表
print (list[0])         # 输出列表第一个元素
print (list[1:3])       # 从第二个开始输出到第三个元素
print (list[2:])
print("***********")# 输出从第三个元素开始的所有元素
print (tinylist * 2)    # 输出两次列表
print (list + tinylist) # 连接列表

# 结果为：
# ['abcd', 786, 2.23, 'runoob', 70.2]
# abcd
# [786, 2.23]
# [2.23, 'runoob', 70.2]
# [123, 'runoob', 123, 'runoob']
# ['abcd', 786, 2.23, 'runoob', 70.2, 123, 'runoob']


# 与Python字符串不一样的是，列表中的元素是可以改变的：
# 实例
list1 = [1, 2, 3, 4, 5, 6]
list1[0] = 9  # 将a中的第一位换成9
list1[2:5] = [13, 14, 15]  # 将第三位到第五位换成[13,14,15]
print(list1)  # 输出应为：[9, 2, 13, 14, 15, 6]
list1[2:5] = []   # 将对应的元素值设置为 []即为空
print(list1)  # [9, 2, 6]

# Python 列表截取可以接收第三个参数，参数作用是截取的步长
# 如果第三个参数为负数表示逆向读取，以下实例用于翻转字符串

list2 = [1, 2, 3, 4]
# 翻转列表
print(list2[-1::-1])  # [4, 3, 2, 1]






# Tuple（元组）
# 元组（tuple）与列表类似，不同之处在于元组的元素不能修改。元组写在小括号 () 里，元素之间用逗号隔开。
# 元组中的元素类型也可以不相同

# 元组与字符串类似，可以被索引且下标索引从0开始，-1 为从末尾开始的位置。也可以进行截取（看上面，这里不再赘述）。
# 其实，可以把字符串看作一种特殊的元组。
print("元组***********")
tuple = ('abcd', 786, 2.23, 'runoob', 70.2,"runoob")
tinytuple = (123, 'runoob')

print(tuple)  # 输出完整元组  ('abcd', 786, 2.23, 'runoob', 70.2)
print(tuple[0])  # 输出元组的第一个元素  abcd
print(tuple[1:3])  # 输出从第二个元素开始到第三个元素  (786, 2.23)
print(tuple[2:])  # 输出从第三个元素开始的所有元素  (2.23, 'runoob', 70.2)
print(tinytuple * 2)  # 输出两次元组  (123, 'runoob', 123, 'runoob')
print(tuple + tinytuple)  # 连接元组  ('abcd', 786, 2.23, 'runoob', 70.2, 123, 'runoob')


# 构造包含 0 个或 1 个元素的元组比较特殊，所以有一些额外的语法规则：
#
tup1 = ()    # 空元组
tup2 = (20,) # 一个元素，需要在元素后添加逗号









# Set（集合）
# 集合（set）是由一个或数个形态各异的大小整体组成的，构成集合的事物或对象称作元素或是成员。
# 基本功能是进行成员关系测试和删除重复元素。
# 可以使用大括号 {}或者 set()
# 函数创建集合，注意：创建一个空集合必须用 set()
# 而不是 {}，因为 {} 是用来创建一个空字典。
# 创建格式：
# parame = {value01, value02, ...}
# set(value)
# 实例
student = {'Tom', 'Jim', 'Mary', 'Tom', 'Jack', 'Rose'}

print(student)  # 输出集合，重复的元素被自动去掉

# set可以进行集合运算
a = set('abracadabra')
b = set('alacazam')

print("--------------------")
print(a)  # {'b', 'r', 'd', 'c', 'a'}

print(a - b)  # a 和 b 的差集  {'d', 'r', 'b'}

print(a | b)  # a 和 b 的并集  {'b', 'r', 'l', 'm', 'd', 'c', 'a', 'z'}

print(a & b)  # a 和 b 的交集  {'c', 'a'}

print(a ^ b)  # a 和 b 中不同时存在的元素  {'d', 'm', 'b', 'r', 'l', 'z'}







# Dictionary（字典）
# 字典（dictionary）是Python中另一个非常有用的内置数据类型。
# 列表是有序的对象集合，字典是无序的对象集合。两者之间的区别在于：字典当中的元素是通过键来存取的，而不是通过偏移存取。
# 字典是一种映射类型，字典用 { } 标识，它是一个无序的 键(key) : 值(value) 的集合。
# 键(key)必须使用不可变类型。
# 在同一个字典中，键(key)必须是唯一的。

dict = {}
dict['one'] = "1 - 菜鸟教程"
dict[2] = "2 - 菜鸟工具"

tinydict = {'name': 'runoob', 'code': 1, 'site': 'www.runoob.com'}

print(dict['one'])  # 输出键为 'one' 的值
print(dict[2])  # 输出键为 2 的值
print(tinydict)  # 输出完整的字典
print(tinydict.keys())  # 输出所有键
print(tinydict.values())  # 输出所有值

# 输出结果
# 1 - 菜鸟教程
# 2 - 菜鸟工具
# {'name': 'runoob', 'code': 1, 'site': 'www.runoob.com'}
# dict_keys(['name', 'code', 'site'])
# dict_values(['runoob', 1, 'www.runoob.com'])

# 注意：
# 1、字典是一种映射类型，它的元素是键值对。
# 2、字典的关键字必须为不可变类型，且不能重复。
# 3、创建空字典使用 { }。





# Python数据类型转换
# 有时候，我们需要对数据内置的类型进行转换，数据类型的转换，你只需要将数据类型作为函数名即可。
# 以下几个内置的函数可以执行数据类型之间的转换。这些函数返回一个新的对象，表示转换的值。

'''
int(x [,base])   将x转换为一个整数
float(x)    将x转换到一个浮点数
complex(real [,imag])   创建一个复数
str(x)   将对象 x 转换为字符串
repr(x)  将对象 x 转换为表达式字符串
eval(str)   用来计算在字符串中的有效Python表达式,并返回一个对象
tuple(s)   将序列 s 转换为一个元组

list = ["a","b","c","d"]
print(type(tuple(list)))

list(s)   将序列 s 转换为一个列表
set(s)   转换为可变集合
dict(d)   创建一个字典。d 必须是一个 (key, value)元组序列。
frozenset(s)   转换为不可变集合
chr(x)   将一个整数转换为一个字符
ord(x)   将一个字符转换为它的整数值
hex(x)   将一个整数转换为一个十六进制字符串

oct(x)   将一个整数转换为一个八进制字符串
'''




