
#Python中简单语法练习
# if ... else
# while....
# while ... else
# for X in ....
# range()函数
# break 和 continue



a, b = 0, 1
while b < 10:
    print(b)
    a, b = b, a+b
# 第一行包含了一个复合赋值：变量 a 和 b 同时得到新值 0 和 1。
# 最后一行再次使用了同样的方法，可以看到，右边的表达式会在赋值变动之前执行。右边表达式的执行顺序是从左往右的。



# 需要注意冒号和缩进。另外，在 Python 中没有 do..while 循环。
# 以下实例使用了 while 来计算 1 到 100 的总和：

# 第一种
n = 100
sum = 0
count = 1
while count <= n:
    sum += count
    count += 1
print("1到%d的和为：%d"%(n,sum))   # %d 表示格式整数  %s 表示格式化字符串

# 第二种
n = 0
sum = 0
for n in range(0,101):# n 范围 0-100
    sum += n
print(sum)

# 第三种
# print(sum(range(101)))



# 遍历列表的两种方式
languages = ["C", "C++", "Perl", "Python"]
for x in languages:
    print (x)

# enumerate() 函数用于将一个可遍历的数据对象(如列表、元组或字符串)组合为一个索引序列，同时列出数据和数据下标，一般用在 for 循环当中。
sequence = [12, 34, 34, 23, 45, 76, 89]
for i, j in enumerate(sequence):
     print(i, j)



# range()函数
# 如果你需要遍历数字序列，可以使用内置range()函数。它会生成数列，例如:
for i in range(5):
    print(i)  # 0  1  2  3  4
# 你也可以使用range指定区间的值：
for i in range(5, 9):
    print(i)  # 5  6  7  8

# 也可以使range以指定数字开始并指定不同的增量(甚至可以是负数，有时这也叫做'步长'):
for i in range(0, 10, 3) :
    print(i) #0 3 6 9


# break 语句可以跳出 for 和 while 的循环体。如果你从 for 或 while 循环中终止，任何对应的循环 else 块将不执行。
# continue 语句被用来告诉 Python 跳过当前循环块中的剩余语句，然后继续进行下一轮循环。 （两者同Java类似）



# pass 语句
# Python pass是空语句，是为了保持程序结构的完整性。
# pass 不做任何事情，一般用做占位语句，如下实例



# 使用循环嵌套来实现99乘法法则:
i=1
while i<=9:
     j=1
     while j<=i:
          mut =j*i
          print("%d*%d=%d"%(j,i,mut), end="  ")
          j+=1
     print("")
     i+=1