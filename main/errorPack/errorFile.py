import sys


# 一个except子句可以同时处理多个异常，这些异常将被放在一个括号里成为一个元组，例如:

# PS :Python pass 是空语句，是为了保持程序结构的完整性。pass 不做任何事情，一般用做占位语句。
try:
    print(10 * (1/0))
except (RuntimeError, TypeError, NameError,ZeroDivisionError):
    print("发生了异常")
    pass


try:
    f = open('myfile.txt')
    s = f.readline()
    i = int(s.strip())
except OSError as err:
    print("OS error: {0}".format(err))
except ValueError:
    print("Could not convert data to an integer.")
except:
    print("Unexpected error:", sys.exc_info()[0])
    raise #手动异常

def errorFunction(a):
    if a < 5:
        raise ValueError("a必须为大于5的数")
        pass
    else:
        return "SUCCESS"
# print(errorFunction(6))
# print(errorFunction(1))

#try/except...else
#使用 else 子句比把所有的语句都放在 try 子句里面要好，这样可以避免一些意想不到，而 except 又无法捕获的异常。
# 异常处理并不仅仅处理那些直接发生在 try 子句中的异常，而且还能处理子句中调用的函数（甚至间接调用的函数）里抛出的异常。例如:

try:
    print(10 * (1/10))
except (RuntimeError, TypeError, NameError,ZeroDivisionError):
    print("发生了异常1111")
    pass
else:
    print("这里没事")

#  finally 语句无论异常是否发生都会执行：
try:
    runoob()
except AssertionError as error:
    print(error)
else:
    try:
        with open('file.log') as file:
            read_data = file.read()
    except FileNotFoundError as fnf_error:
        print(fnf_error)
finally:
        print('这句话，无论异常是否发生都会执行。')

