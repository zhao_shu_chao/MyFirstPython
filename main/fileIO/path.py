# Python3 OS 文件/目录方法
# https://www.runoob.com/python3/python3-os-file-methods.html

#1. 权限验证
'''
os.F_OK: 作为access()的mode参数，测试path是否存在。
os.R_OK: 包含在access()的mode参数中 ， 测试path是否可读。
os.W_OK 包含在access()的mode参数中 ， 测试path是否可写。
os.X_OK 包含在access()的mode参数中 ，测试path是否可执行。
'''
import os
from shutil import copyfile

ret = os.access("C:\\Users\\18875\Desktop\\test11.txt", os.F_OK)
print ("F_OK - 返回值 %s"% ret)

ret = os.access("C:\\Users\\18875\Desktop\\test11.txt", os.R_OK)
print ("R_OK - 返回值 %s"% ret)

ret = os.access("C:\\Users\\18875\Desktop\\test11.txt", os.W_OK)
print ("W_OK - 返回值 %s"% ret)

ret = os.access("C:\\Users\\18875\Desktop\\test11.txt", os.X_OK)
print ("X_OK - 返回值 %s"% ret)


# 2. 切换工作目录
# 查看当前工作目录
retval = os.getcwd()
print ("当前工作目录为 %s" % retval)
path = retval + "\\test.txt"
foo = open(path,"w+")
foo.write("this is path")
foo.close()
# os.chdir("G:\yuanjun\pythonWork\\auto\MyFirstPython\main\\base")
retval = os.getcwd()
print ("目录修改成功 %s" % retval)

copyfile("hello.txt","hello2.txt")
# # 修改当前工作目录
# os.chdir( path )
#
# # 查看修改后的工作目录
# retval = os.getcwd()
#
# print ("目录修改成功 %s" % retval)

