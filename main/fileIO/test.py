# python2 raw_input([prompt]) 函数从标准输入读取一个行，并返回一个字符串（去掉结尾的换行符）：
# Python3将raw_input和input进行整合成了input....去除了raw_input()函数....
# 其接受任意输入, 将所有输入默认为字符串处理,并返回字符串类型

print("你叫什么名字?"),
name = input()
print("这是你的第几篇博客?"),
num = input()
print("博客的内容是关于哪方面的?"),
aspect = input()

print("大家好，我是 %s ,这是我第 %s 篇关于 %s 的学习博客. " %(name ,num ,aspect))