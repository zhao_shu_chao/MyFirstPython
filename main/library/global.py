num = 1
# 果想要在函数内定义全局作用域，需要加上global修饰符。
# 在函数中有再赋值/定义（因为python是弱类型语言，赋值语句和其定义变量的语句一样），则会产生引用了未定义变量的错误：
def fun1():
    global num  # 需要使用 global 关键字声明
    print(num)
    num = 123  # 若没有这个在赋值的语句，那么不加global也正常运行
    print(num)
fun1()
