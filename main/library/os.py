

# 操作系统接口
# 建议使用 "import os" 风格而非 "from os import *"。这样可以保证随操作系统不同而有所变化的 os.open() 不会覆盖内置函数 open()。
import os
import sys
# 针对日常的文件和目录管理任务
import shutil
import glob
# 返回工作目录
print(os.getcwd())
# 执行系统命令 mkdir
# os.system('mkdir today')
# 复制文件
# shutil.copyfile("global.py","aaa.py")
# 移动文件
# shutil.move('aaa.py', 'today/aa.py')

# 搜索文件
print(glob.glob("*.py"))

print(sys.argv)
