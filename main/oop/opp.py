

class firstName:
    # age = 1
    # name = "小明"

    def __init__(self,age,name):
        self.age = age
        self.name = name

    def f(self):
        return "helloworld"

x = firstName(2,"小李")
print("MyClass 类的属性 i 为：", x.age,x.name)
print("MyClass 类的方法 f 输出为：", x.f())

# 类的方法与普通的函数只有一个特别的区别——它们必须有一个额外的第一个参数名称, 按照惯例它的名称是 self。
class Complex:
    def __init__(self, realpart, imagpart):
        self.r = realpart
        self.i = imagpart
x = Complex(3.0, -4.5)
print(x.r, x.i)   # 输出结果：3.0 -4.5