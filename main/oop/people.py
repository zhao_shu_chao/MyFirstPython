

class people:
    name = ''
    age = 0
    #  #定义私有属性,私有属性在类外部无法直接进行访问
    __weight = 0
    def __init__(self,nage,age,weight):
        self.name = nage
        self.age = age
        self.__weight = weight
    def speak(self):
        print("%s 说: 我 %d 岁。" %(self.name,self.age))


# 实例化类
p = people('runoob',10,30)
p.speak()

class student(people):
    grade = ''

    def __init__(self, name, age, weight, grade):
        # 调用父类的构函
        people.__init__(self, name, age, weight)
        self.grade = grade
    #覆写父类的方法
    def speak(self):
        print("%s 说: 我 %d 岁了，我在读 %d 年级"%(self.name,self.age,self.grade))

s = student('ken',10,60,3)
s.speak()