
# 选择排序
arr = [64, 25, 12, 22, 11]

for i in range(len(arr)):
    min_idx = i
    for j in range(i+1, len(arr)):
        if arr[min_idx] > arr[j]:
            min_idx = j

    arr[i],arr[min_idx] = arr[min_idx], arr[i]


print ("排序后的数组：")
for i in range(len(arr)):
    print("%d" %arr[i])
