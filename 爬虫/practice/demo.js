// new ((function i(t){var e={};function r(i){if(e[i])return e[i].exports;var a=e[i]={i:i,l:!1,exports:{}};return t[i].call(a.exports,a,a.exports,r),a.l=!0,a.exports}r.m=t,r.c=e,r.i=function(t){return t},r.d=function(t,e,i){r.o(t,e)||Object.defineProperty(t,e,{configurable:!1,enumerable:!0,get:i})},r.r=function(t){Object.defineProperty(t,"__esModule",{value:!0})},r.n=function(t){var e=t&&t.__esModule?function(){return t.default}:function(){return t};return r.d(e,"a",e),e},r.o=function(t,e){return Object.prototype.hasOwnProperty.call(t,e)},r.p="/",r.oe=function(t){throw console.error(t),t};var i=r(r.s=18);return i.default||i})({18: function(t,e,r){"use strict";r.r(e),r.d(e,"default",(function(){return o}));var i=r(9),a=r(0),n=r(1),s=r(13);function o(t){var e=new s.EventEmitter,r=function(e,r){t.postMessage({event:e,data:r})};e.on(a.a.FRAG_DECRYPTED,r),e.on(a.a.ERROR,r),t.addEventListener("message",(function(a){var s=a.data;switch(s.cmd){case"init":var o=JSON.parse(s.config);t.transmuxer=new i.c(e,s.typeSupported,o,s.vendor),Object(n.a)(o.debug),r("init",null);break;case"configure":t.transmuxer.configure(s.config);break;case"demux":var u=t.transmuxer.push(s.data,s.decryptdata,s.chunkMeta,s.state);Object(i.d)(u)?u.then((function(e){l(t,e)})):l(t,u);break;case"flush":var d=s.chunkMeta,c=t.transmuxer.flush(d);Object(i.d)(c)?c.then((function(e){h(t,e,d)})):h(t,c,d)}}))}function l(t,e){if((r=e.remuxResult).audio||r.video||r.text||r.id3||r.initSegment){var r,i=[],a=e.remuxResult,n=a.audio,s=a.video;n&&u(i,n),s&&u(i,s),t.postMessage({event:"transmuxComplete",data:e},i)}}function u(t,e){e.data1&&t.push(e.data1.buffer),e.data2&&t.push(e.data2.buffer)}function h(t,e,r){e.forEach((function(e){l(t,e)})),t.postMessage({event:"flush",data:r})}},13: function(t,e,r){"use strict";var i=Object.prototype.hasOwnProperty,a="~";function n(){}function s(t,e,r){this.fn=t,this.context=e,this.once=r||!1}function o(t,e,r,i,n){if("function"!=typeof r)throw new TypeError("The listener must be a function");var o=new s(r,i||t,n),l=a?a+e:e;return t._events[l]?t._events[l].fn?t._events[l]=[t._events[l],o]:t._events[l].push(o):(t._events[l]=o,t._eventsCount++),t}function l(t,e){0==--t._eventsCount?t._events=new n:delete t._events[e]}function u(){this._events=new n,this._eventsCount=0}Object.create&&(n.prototype=Object.create(null),(new n).__proto__||(a=!1)),u.prototype.eventNames=function(){var t,e,r=[];if(0===this._eventsCount)return r;for(e in t=this._events)i.call(t,e)&&r.push(a?e.slice(1):e);return Object.getOwnPropertySymbols?r.concat(Object.getOwnPropertySymbols(t)):r},u.prototype.listeners=function(t){var e=a?a+t:t,r=this._events[e];if(!r)return[];if(r.fn)return[r.fn];for(var i=0,n=r.length,s=new Array(n);i<n;i++)s[i]=r[i].fn;return s},u.prototype.listenerCount=function(t){var e=a?a+t:t,r=this._events[e];return r?r.fn?1:r.length:0},u.prototype.emit=function(t,e,r,i,n,s){var o=a?a+t:t;if(!this._events[o])return!1;var l,u,h=this._events[o],d=arguments.length;if(h.fn){switch(h.once&&this.removeListener(t,h.fn,void 0,!0),d){case 1:return h.fn.call(h.context),!0;case 2:return h.fn.call(h.context,e),!0;case 3:return h.fn.call(h.context,e,r),!0;case 4:return h.fn.call(h.context,e,r,i),!0;case 5:return h.fn.call(h.context,e,r,i,n),!0;case 6:return h.fn.call(h.context,e,r,i,n,s),!0}for(u=1,l=new Array(d-1);u<d;u++)l[u-1]=arguments[u];h.fn.apply(h.context,l)}else{var c,f=h.length;for(u=0;u<f;u++)switch(h[u].once&&this.removeListener(t,h[u].fn,void 0,!0),d){case 1:h[u].fn.call(h[u].context);break;case 2:h[u].fn.call(h[u].context,e);break;case 3:h[u].fn.call(h[u].context,e,r);break;case 4:h[u].fn.call(h[u].context,e,r,i);break;default:if(!l)for(c=1,l=new Array(d-1);c<d;c++)l[c-1]=arguments[c];h[u].fn.apply(h[u].context,l)}}return!0},u.prototype.on=function(t,e,r){return o(this,t,e,r,!1)},u.prototype.once=function(t,e,r){return o(this,t,e,r,!0)},u.prototype.removeListener=function(t,e,r,i){var n=a?a+t:t;if(!this._events[n])return this;if(!e)return l(this,n),this;var s=this._events[n];if(s.fn)s.fn!==e||i&&!s.once||r&&s.context!==r||l(this,n);else{for(var o=0,u=[],h=s.length;o<h;o++)(s[o].fn!==e||i&&!s[o].once||r&&s[o].context!==r)&&u.push(s[o]);u.length?this._events[n]=1===u.length?u[0]:u:l(this,n)}return this},u.prototype.removeAllListeners=function(t){var e;return t?(e=a?a+t:t,this._events[e]&&l(this,e)):(this._events=new n,this._eventsCount=0),this},u.prototype.off=u.prototype.removeListener,u.prototype.addListener=u.prototype.on,u.prefixed=a,u.EventEmitter=u,t.exports=u},1: function(t,e,r){"use strict";r.d(e,"a",(function(){return o})),r.d(e,"b",(function(){return l}));var i=function(){},a={trace:i,debug:i,log:i,warn:i,info:i,error:i},n=a;function s(t){var e=self.console[t];return e?e.bind(self.console,"["+t+"] >"):i}function o(t){if(self.console&&!0===t||"object"==typeof t){!function(t){for(var e=arguments.length,r=new Array(e>1?e-1:0),i=1;i<e;i++)r[i-1]=arguments[i];r.forEach((function(e){n[e]=t[e]?t[e].bind(t):s(e)}))}(t,"debug","log","info","warn","error");try{n.log()}catch(t){n=a}}else n=a}var l=a},0: function(t,e,r){"use strict";var i;r.d(e,"a",(function(){return i})),function(t){t.MEDIA_ATTACHING="hlsMediaAttaching",t.MEDIA_ATTACHED="hlsMediaAttached",t.MEDIA_DETACHING="hlsMediaDetaching",t.MEDIA_DETACHED="hlsMediaDetached",t.BUFFER_RESET="hlsBufferReset",t.BUFFER_CODECS="hlsBufferCodecs",t.BUFFER_CREATED="hlsBufferCreated",t.BUFFER_APPENDING="hlsBufferAppending",t.BUFFER_APPENDED="hlsBufferAppended",t.BUFFER_EOS="hlsBufferEos",t.BUFFER_FLUSHING="hlsBufferFlushing",t.BUFFER_FLUSHED="hlsBufferFlushed",t.MANIFEST_LOADING="hlsManifestLoading",t.MANIFEST_LOADED="hlsManifestLoaded",t.MANIFEST_PARSED="hlsManifestParsed",t.LEVEL_SWITCHING="hlsLevelSwitching",t.LEVEL_SWITCHED="hlsLevelSwitched",t.LEVEL_LOADING="hlsLevelLoading",t.LEVEL_LOADED="hlsLevelLoaded",t.LEVEL_UPDATED="hlsLevelUpdated",t.LEVEL_PTS_UPDATED="hlsLevelPtsUpdated",t.LEVELS_UPDATED="hlsLevelsUpdated",t.AUDIO_TRACKS_UPDATED="hlsAudioTracksUpdated",t.AUDIO_TRACK_SWITCHING="hlsAudioTrackSwitching",t.AUDIO_TRACK_SWITCHED="hlsAudioTrackSwitched",t.AUDIO_TRACK_LOADING="hlsAudioTrackLoading",t.AUDIO_TRACK_LOADED="hlsAudioTrackLoaded",t.SUBTITLE_TRACKS_UPDATED="hlsSubtitleTracksUpdated",t.SUBTITLE_TRACKS_CLEARED="hlsSubtitleTracksCleared",t.SUBTITLE_TRACK_SWITCH="hlsSubtitleTrackSwitch",t.SUBTITLE_TRACK_LOADING="hlsSubtitleTrackLoading",t.SUBTITLE_TRACK_LOADED="hlsSubtitleTrackLoaded",t.SUBTITLE_FRAG_PROCESSED="hlsSubtitleFragProcessed",t.CUES_PARSED="hlsCuesParsed",t.NON_NATIVE_TEXT_TRACKS_FOUND="hlsNonNativeTextTracksFound",t.INIT_PTS_FOUND="hlsInitPtsFound",t.FRAG_LOADING="hlsFragLoading",t.FRAG_LOAD_EMERGENCY_ABORTED="hlsFragLoadEmergencyAborted",t.FRAG_LOADED="hlsFragLoaded",t.FRAG_DECRYPTED="hlsFragDecrypted",t.FRAG_PARSING_INIT_SEGMENT="hlsFragParsingInitSegment",t.FRAG_PARSING_USERDATA="hlsFragParsingUserdata",t.FRAG_PARSING_METADATA="hlsFragParsingMetadata",t.FRAG_PARSED="hlsFragParsed",t.FRAG_BUFFERED="hlsFragBuffered",t.FRAG_CHANGED="hlsFragChanged",t.FPS_DROP="hlsFpsDrop",t.FPS_DROP_LEVEL_CAPPING="hlsFpsDropLevelCapping",t.ERROR="hlsError",t.DESTROYING="hlsDestroying",t.KEY_LOADING="hlsKeyLoading",t.KEY_LOADED="hlsKeyLoaded",t.LIVE_BACK_BUFFER_REACHED="hlsLiveBackBufferReached",t.BACK_BUFFER_REACHED="hlsBackBufferReached"}(i||(i={}))},9: function(t,e,r){"use strict";r.d(e,"c",(function(){return lt})),r.d(e,"d",(function(){return ht})),r.d(e,"a",(function(){return dt})),r.d(e,"b",(function(){return ct}));var i=r(0),a=r(2),n=r(14),s=r(3),o=r(6);var l=r(5),u=r(8),h=function(){function t(){this._audioTrack=void 0,this._id3Track=void 0,this.frameIndex=0,this.cachedData=null,this.initPTS=null}var e=t.prototype;return e.resetInitSegment=function(t,e,r){this._id3Track={type:"id3",id:0,pid:-1,inputTimeScale:9e4,sequenceNumber:0,samples:[],dropped:0}},e.resetTimeStamp=function(){},e.resetContiguity=function(){},e.canParse=function(t,e){return!1},e.appendFrame=function(t,e,r){},e.demux=function(t,e){this.cachedData&&(t=Object(l.a)(this.cachedData,t),this.cachedData=null);var r,i,a=o.b(t,0),n=a?a.length:0,s=this._audioTrack,h=this._id3Track,c=a?o.d(a):void 0,f=t.length;for(0!==this.frameIndex&&null!==this.initPTS||(this.initPTS=d(c,e)),a&&a.length>0&&h.samples.push({pts:this.initPTS,dts:this.initPTS,data:a}),i=this.initPTS;n<f;){if(this.canParse(t,n)){var g=this.appendFrame(s,t,n);g?(this.frameIndex++,i=g.sample.pts,r=n+=g.length):n=f}else o.a(t,n)?(a=o.b(t,n),h.samples.push({pts:i,dts:i,data:a}),r=n+=a.length):n++;if(n===f&&r!==f){var v=Object(u.a)(t,r);this.cachedData?this.cachedData=Object(l.a)(this.cachedData,v):this.cachedData=v}}return{audioTrack:s,avcTrack:{type:"",id:-1,pid:-1,inputTimeScale:9e4,sequenceNumber:-1,samples:[],dropped:0},id3Track:h,textTrack:{type:"",id:-1,pid:-1,inputTimeScale:9e4,sequenceNumber:-1,samples:[],dropped:0}}},e.demuxSampleAes=function(t,e,r){return Promise.reject(new Error("["+this+"] This demuxer does not support Sample-AES decryption"))},e.flush=function(t){var e=this.cachedData;return e&&(this.cachedData=null,this.demux(e,0)),this.frameIndex=0,{audioTrack:this._audioTrack,avcTrack:{type:"",id:-1,pid:-1,inputTimeScale:9e4,sequenceNumber:-1,samples:[],dropped:0},id3Track:this._id3Track,textTrack:{type:"",id:-1,pid:-1,inputTimeScale:9e4,sequenceNumber:-1,samples:[],dropped:0}}},e.destroy=function(){},t}(),d=function(t,e){return Object(s.a)(t)?90*t:9e4*e},c=h,f=r(1);function g(t,e){return 255===t[e]&&240==(246&t[e+1])}function v(t,e){return 1&t[e+1]?7:9}function p(t,e){return(3&t[e+3])<<11|t[e+4]<<3|(224&t[e+5])>>>5}function m(t,e){return e+1<t.length&&g(t,e)}function y(t,e){if(m(t,e)){var r=v(t,e);if(e+r>=t.length)return!1;var i=p(t,e);if(i<=r)return!1;var a=e+i;return a===t.length||m(t,a)}return!1}function T(t,e,r,n,s){if(!t.samplerate){var o=function(t,e,r,n){var s,o,l,u,h=navigator.userAgent.toLowerCase(),d=n,c=[96e3,88200,64e3,48e3,44100,32e3,24e3,22050,16e3,12e3,11025,8e3,7350];s=1+((192&e[r+2])>>>6);var g=(60&e[r+2])>>>2;if(!(g>c.length-1))return l=(1&e[r+2])<<2,l|=(192&e[r+3])>>>6,f.b.log("manifest codec:"+n+", ADTS type:"+s+", samplingIndex:"+g),/firefox/i.test(h)?g>=6?(s=5,u=new Array(4),o=g-3):(s=2,u=new Array(2),o=g):-1!==h.indexOf("android")?(s=2,u=new Array(2),o=g):(s=5,u=new Array(4),n&&(-1!==n.indexOf("mp4a.40.29")||-1!==n.indexOf("mp4a.40.5"))||!n&&g>=6?o=g-3:((n&&-1!==n.indexOf("mp4a.40.2")&&(g>=6&&1===l||/vivaldi/i.test(h))||!n&&1===l)&&(s=2,u=new Array(2)),o=g)),u[0]=s<<3,u[0]|=(14&g)>>1,u[1]|=(1&g)<<7,u[1]|=l<<3,5===s&&(u[1]|=(14&o)>>1,u[2]=(1&o)<<7,u[2]|=8,u[3]=0),{config:u,samplerate:c[g],channelCount:l,codec:"mp4a.40."+s,manifestCodec:d};t.trigger(i.a.ERROR,{type:a.b.MEDIA_ERROR,details:a.a.FRAG_PARSING_ERROR,fatal:!0,reason:"invalid ADTS sampling index:"+g})}(e,r,n,s);if(!o)return;t.config=o.config,t.samplerate=o.samplerate,t.channelCount=o.channelCount,t.codec=o.codec,t.manifestCodec=o.manifestCodec,f.b.log("parsed codec:"+t.codec+", rate:"+o.samplerate+", channels:"+o.channelCount)}}function E(t){return 9216e4/t}function b(t,e,r,i,a){var n=function(t,e,r,i,a){var n=t.length,s=v(t,e),o=p(t,e);if((o-=s)>0&&e+s+o<=n)return{headerLength:s,frameLength:o,stamp:r+i*a}}(e,r,i,a,E(t.samplerate));if(n){var s=n.stamp,o=n.headerLength,l=n.frameLength,u={unit:e.subarray(r+o,r+o+l),pts:s,dts:s};return t.samples.push(u),{sample:u,length:l+o}}}function S(t,e){return(S=Object.setPrototypeOf||function(t,e){return t.__proto__=e,t})(t,e)}var L=function(t){var e,r;function i(e,r){var i;return(i=t.call(this)||this).observer=void 0,i.config=void 0,i.observer=e,i.config=r,i}r=t,(e=i).prototype=Object.create(r.prototype),e.prototype.constructor=e,S(e,r);var a=i.prototype;return a.resetInitSegment=function(e,r,i){t.prototype.resetInitSegment.call(this,e,r,i),this._audioTrack={container:"audio/adts",type:"audio",id:0,pid:-1,sequenceNumber:0,isAAC:!0,samples:[],manifestCodec:e,duration:i,inputTimeScale:9e4,dropped:0}},i.probe=function(t){if(!t)return!1;for(var e=(o.b(t,0)||[]).length,r=t.length;e<r;e++)if(y(t,e))return f.b.log("ADTS sync word found !"),!0;return!1},a.canParse=function(t,e){return function(t,e){return function(t,e){return e+5<t.length}(t,e)&&g(t,e)&&p(t,e)<=t.length-e}(t,e)},a.appendFrame=function(t,e,r){return T(t,this.observer,e,r,t.manifestCodec),b(t,e,r,this.initPTS,this.frameIndex)},i}(c);L.minProbeByteLength=9;var A=L,R=function(){function t(t,e){this.remainderData=null,this.config=void 0,this.config=e}var e=t.prototype;return e.resetTimeStamp=function(){},e.resetInitSegment=function(){},e.resetContiguity=function(){},t.probe=function(t){return Object(l.b)({data:t,start:0,end:Math.min(t.length,16384)},["moof"]).length>0},e.demux=function(t){var e=t,r={type:"",id:-1,pid:-1,inputTimeScale:9e4,sequenceNumber:-1,samples:[],dropped:0};if(this.config.progressive){this.remainderData&&(e=Object(l.a)(this.remainderData,t));var i=Object(l.h)(e);this.remainderData=i.remainder,r.samples=i.valid||new Uint8Array}else r.samples=e;return{audioTrack:{type:"",id:-1,pid:-1,inputTimeScale:9e4,sequenceNumber:-1,samples:[],dropped:0},avcTrack:r,id3Track:{type:"",id:-1,pid:-1,inputTimeScale:9e4,sequenceNumber:-1,samples:[],dropped:0},textTrack:{type:"",id:-1,pid:-1,inputTimeScale:9e4,sequenceNumber:-1,samples:[],dropped:0}}},e.flush=function(){var t={type:"",id:-1,pid:-1,inputTimeScale:9e4,sequenceNumber:-1,samples:[],dropped:0};return t.samples=this.remainderData||new Uint8Array,this.remainderData=null,{audioTrack:{type:"",id:-1,pid:-1,inputTimeScale:9e4,sequenceNumber:-1,samples:[],dropped:0},avcTrack:t,id3Track:{type:"",id:-1,pid:-1,inputTimeScale:9e4,sequenceNumber:-1,samples:[],dropped:0},textTrack:{type:"",id:-1,pid:-1,inputTimeScale:9e4,sequenceNumber:-1,samples:[],dropped:0}}},e.demuxSampleAes=function(t,e,r){return Promise.reject(new Error("The MP4 demuxer does not support SAMPLE-AES decryption"))},e.destroy=function(){},t}();R.minProbeByteLength=1024;var D=R,k=null,_=[32,64,96,128,160,192,224,256,288,320,352,384,416,448,32,48,56,64,80,96,112,128,160,192,224,256,320,384,32,40,48,56,64,80,96,112,128,160,192,224,256,320,32,48,56,64,80,96,112,128,144,160,176,192,224,256,8,16,24,32,40,48,56,64,80,96,112,128,144,160],I=[44100,48e3,32e3,22050,24e3,16e3,11025,12e3,8e3],C=[[0,72,144,12],[0,0,0,0],[0,72,144,12],[0,144,144,12]],w=[0,1,1,4];function O(t,e,r,i,a){if(!(r+24>e.length)){var n=x(e,r);if(n&&r+n.frameLength<=e.length){var s=i+a*(9e4*n.samplesPerFrame/n.sampleRate),o={unit:e.subarray(r,r+n.frameLength),pts:s,dts:s};return t.config=[],t.channelCount=n.channelCount,t.samplerate=n.sampleRate,t.samples.push(o),{sample:o,length:n.frameLength}}}}function x(t,e){var r=t[e+1]>>3&3,i=t[e+1]>>1&3,a=t[e+2]>>4&15,n=t[e+2]>>2&3;if(1!==r&&0!==a&&15!==a&&3!==n){var s=t[e+2]>>1&1,o=t[e+3]>>6,l=1e3*_[14*(3===r?3-i:3===i?3:4)+a-1],u=I[3*(3===r?0:2===r?1:2)+n],h=3===o?1:2,d=C[r][i],c=w[i],f=8*d*c,g=Math.floor(d*l/u+s)*c;if(null===k){var v=(navigator.userAgent||"").match(/Chrome\/(\d+)/i);k=v?parseInt(v[1]):0}return!!k&&k<=87&&2===i&&l>=224e3&&0===o&&(t[e+3]=128|t[e+3]),{sampleRate:u,channelCount:h,frameLength:g,samplesPerFrame:f}}}function P(t,e){return 255===t[e]&&224==(224&t[e+1])&&0!=(6&t[e+1])}function M(t,e){return e+1<t.length&&P(t,e)}function F(t,e){if(e+1<t.length&&P(t,e)){var r=x(t,e),i=4;null!=r&&r.frameLength&&(i=r.frameLength);var a=e+i;return a===t.length||M(t,a)}return!1}var N=function(){function t(t){this.data=void 0,this.bytesAvailable=void 0,this.word=void 0,this.bitsAvailable=void 0,this.data=t,this.bytesAvailable=t.byteLength,this.word=0,this.bitsAvailable=0}var e=t.prototype;return e.loadWord=function(){var t=this.data,e=this.bytesAvailable,r=t.byteLength-e,i=new Uint8Array(4),a=Math.min(4,e);if(0===a)throw new Error("no bytes available");i.set(t.subarray(r,r+a)),this.word=new DataView(i.buffer).getUint32(0),this.bitsAvailable=8*a,this.bytesAvailable-=a},e.skipBits=function(t){var e;this.bitsAvailable>t?(this.word<<=t,this.bitsAvailable-=t):(t-=this.bitsAvailable,t-=(e=t>>3)>>3,this.bytesAvailable-=e,this.loadWord(),this.word<<=t,this.bitsAvailable-=t)},e.readBits=function(t){var e=Math.min(this.bitsAvailable,t),r=this.word>>>32-e;return t>32&&f.b.error("Cannot read more than 32 bits at a time"),this.bitsAvailable-=e,this.bitsAvailable>0?this.word<<=e:this.bytesAvailable>0&&this.loadWord(),(e=t-e)>0&&this.bitsAvailable?r<<e|this.readBits(e):r},e.skipLZ=function(){var t;for(t=0;t<this.bitsAvailable;++t)if(0!=(this.word&2147483648>>>t))return this.word<<=t,this.bitsAvailable-=t,t;return this.loadWord(),t+this.skipLZ()},e.skipUEG=function(){this.skipBits(1+this.skipLZ())},e.skipEG=function(){this.skipBits(1+this.skipLZ())},e.readUEG=function(){var t=this.skipLZ();return this.readBits(t+1)-1},e.readEG=function(){var t=this.readUEG();return 1&t?1+t>>>1:-1*(t>>>1)},e.readBoolean=function(){return 1===this.readBits(1)},e.readUByte=function(){return this.readBits(8)},e.readUShort=function(){return this.readBits(16)},e.readUInt=function(){return this.readBits(32)},e.skipScalingList=function(t){for(var e=8,r=8,i=0;i<t;i++)0!==r&&(r=(e+this.readEG()+256)%256),e=0===r?e:r},e.readSPS=function(){var t,e,r,i=0,a=0,n=0,s=0,o=this.readUByte.bind(this),l=this.readBits.bind(this),u=this.readUEG.bind(this),h=this.readBoolean.bind(this),d=this.skipBits.bind(this),c=this.skipEG.bind(this),f=this.skipUEG.bind(this),g=this.skipScalingList.bind(this);o();var v=o();if(l(5),d(3),o(),f(),100===v||110===v||122===v||244===v||44===v||83===v||86===v||118===v||128===v){var p=u();if(3===p&&d(1),f(),f(),d(1),h())for(e=3!==p?8:12,r=0;r<e;r++)h()&&g(r<6?16:64)}f();var m=u();if(0===m)u();else if(1===m)for(d(1),c(),c(),t=u(),r=0;r<t;r++)c();f(),d(1);var y=u(),T=u(),E=l(1);0===E&&d(1),d(1),h()&&(i=u(),a=u(),n=u(),s=u());var b=[1,1];if(h()&&h())switch(o()){case 1:b=[1,1];break;case 2:b=[12,11];break;case 3:b=[10,11];break;case 4:b=[16,11];break;case 5:b=[40,33];break;case 6:b=[24,11];break;case 7:b=[20,11];break;case 8:b=[32,11];break;case 9:b=[80,33];break;case 10:b=[18,11];break;case 11:b=[15,11];break;case 12:b=[64,33];break;case 13:b=[160,99];break;case 14:b=[4,3];break;case 15:b=[3,2];break;case 16:b=[2,1];break;case 255:b=[o()<<8|o(),o()<<8|o()]}return{width:Math.ceil(16*(y+1)-2*i-2*a),height:(2-E)*(T+1)*16-(E?2:4)*(n+s),pixelRatio:b}},e.readSliceType=function(){return this.readUByte(),this.readUEG(),this.readUEG()},t}(),U=function(){function t(t,e,r){this.keyData=void 0,this.decrypter=void 0,this.keyData=r,this.decrypter=new n.a(t,e,{removePKCS7Padding:!1})}var e=t.prototype;return e.decryptBuffer=function(t,e){this.decrypter.decrypt(t,this.keyData.key.buffer,this.keyData.iv.buffer,e)},e.decryptAacSample=function(t,e,r,i){var a=t[e].unit,n=a.subarray(16,a.length-a.length%16),s=n.buffer.slice(n.byteOffset,n.byteOffset+n.length),o=this;this.decryptBuffer(s,(function(n){var s=new Uint8Array(n);a.set(s,16),i||o.decryptAacSamples(t,e+1,r)}))},e.decryptAacSamples=function(t,e,r){for(;;e++){if(e>=t.length)return void r();if(!(t[e].unit.length<32)){var i=this.decrypter.isSync();if(this.decryptAacSample(t,e,r,i),!i)return}}},e.getAvcEncryptedData=function(t){for(var e=16*Math.floor((t.length-48)/160)+16,r=new Int8Array(e),i=0,a=32;a<=t.length-16;a+=160,i+=16)r.set(t.subarray(a,a+16),i);return r},e.getAvcDecryptedUnit=function(t,e){for(var r=new Uint8Array(e),i=0,a=32;a<=t.length-16;a+=160,i+=16)t.set(r.subarray(i,i+16),a);return t},e.decryptAvcSample=function(t,e,r,i,a,n){var s=q(a.data),o=this.getAvcEncryptedData(s),l=this;this.decryptBuffer(o.buffer,(function(o){a.data=l.getAvcDecryptedUnit(s,o),n||l.decryptAvcSamples(t,e,r+1,i)}))},e.decryptAvcSamples=function(t,e,r,i){if(t instanceof Uint8Array)throw new Error("Cannot decrypt samples of type Uint8Array");for(;;e++,r=0){if(e>=t.length)return void i();for(var a=t[e].units;!(r>=a.length);r++){var n=a[r];if(!(n.data.length<=48||1!==n.type&&5!==n.type)){var s=this.decrypter.isSync();if(this.decryptAvcSample(t,e,r,i,n,s),!s)return}}}},t}(),B={video:1,audio:2,id3:3,text:4},G=function(){function t(t,e,r){this.observer=void 0,this.config=void 0,this.typeSupported=void 0,this.sampleAes=null,this.pmtParsed=!1,this.audioCodec=void 0,this.videoCodec=void 0,this._duration=0,this.aacLastPTS=null,this._initPTS=null,this._initDTS=null,this._pmtId=-1,this._avcTrack=void 0,this._audioTrack=void 0,this._id3Track=void 0,this._txtTrack=void 0,this.aacOverFlow=null,this.avcSample=null,this.remainderData=null,this.observer=t,this.config=e,this.typeSupported=r}t.probe=function(e){var r=t.syncOffset(e);return!(r<0)&&(r&&f.b.warn("MPEG2-TS detected but first sync word found @ offset "+r+", junk ahead ?"),!0)},t.syncOffset=function(t){for(var e=Math.min(1e3,t.length-564),r=0;r<e;){if(71===t[r]&&71===t[r+188]&&71===t[r+376])return r;r++}return-1},t.createTrack=function(t,e){return{container:"video"===t||"audio"===t?"video/mp2t":void 0,type:t,id:B[t],pid:-1,inputTimeScale:9e4,sequenceNumber:0,samples:[],dropped:0,duration:"audio"===t?e:void 0}};var e=t.prototype;return e.resetInitSegment=function(e,r,i){this.pmtParsed=!1,this._pmtId=-1,this._avcTrack=t.createTrack("video",i),this._audioTrack=t.createTrack("audio",i),this._id3Track=t.createTrack("id3",i),this._txtTrack=t.createTrack("text",i),this._audioTrack.isAAC=!0,this.aacOverFlow=null,this.aacLastPTS=null,this.avcSample=null,this.audioCodec=e,this.videoCodec=r,this._duration=i},e.resetTimeStamp=function(){},e.resetContiguity=function(){var t=this._audioTrack,e=this._avcTrack,r=this._id3Track;t&&(t.pesData=null),e&&(e.pesData=null),r&&(r.pesData=null),this.aacOverFlow=null,this.aacLastPTS=null},e.demux=function(e,r,n,s){var o;void 0===n&&(n=!1),void 0===s&&(s=!1),n||(this.sampleAes=null);var u=this._avcTrack,h=this._audioTrack,d=this._id3Track,c=u.pid,g=u.pesData,v=h.pid,p=d.pid,m=h.pesData,y=d.pesData,T=!1,E=this.pmtParsed,b=this._pmtId,S=e.length;if(this.remainderData&&(S=(e=Object(l.a)(this.remainderData,e)).length,this.remainderData=null),S<188&&!s)return this.remainderData=e,{audioTrack:h,avcTrack:u,id3Track:d,textTrack:this._txtTrack};var L=Math.max(0,t.syncOffset(e));(S-=(S+L)%188)<e.byteLength&&!s&&(this.remainderData=new Uint8Array(e.buffer,S,e.buffer.byteLength-S));for(var A=L;A<S;A+=188)if(71===e[A]){var R=!!(64&e[A+1]),D=((31&e[A+1])<<8)+e[A+2],k=void 0;if((48&e[A+3])>>4>1){if((k=A+5+e[A+4])===A+188)continue}else k=A+4;switch(D){case c:R&&(g&&(o=V(g))&&this.parseAVCPES(o,!1),g={data:[],size:0}),g&&(g.data.push(e.subarray(k,A+188)),g.size+=A+188-k);break;case v:R&&(m&&(o=V(m))&&(h.isAAC?this.parseAACPES(o):this.parseMPEGPES(o)),m={data:[],size:0}),m&&(m.data.push(e.subarray(k,A+188)),m.size+=A+188-k);break;case p:R&&(y&&(o=V(y))&&this.parseID3PES(o),y={data:[],size:0}),y&&(y.data.push(e.subarray(k,A+188)),y.size+=A+188-k);break;case 0:R&&(k+=e[k]+1),b=this._pmtId=j(e,k);break;case b:R&&(k+=e[k]+1);var _=H(e,k,!0===this.typeSupported.mpeg||!0===this.typeSupported.mp3,n);(c=_.avc)>0&&(u.pid=c),(v=_.audio)>0&&(h.pid=v,h.isAAC=_.isAAC),(p=_.id3)>0&&(d.pid=p),T&&!E&&(f.b.log("reparse from beginning"),T=!1,A=L-188),E=this.pmtParsed=!0;break;case 17:case 8191:break;default:T=!0}}else this.observer.emit(i.a.ERROR,i.a.ERROR,{type:a.b.MEDIA_ERROR,details:a.a.FRAG_PARSING_ERROR,fatal:!1,reason:"TS packet did not start with 0x47"});u.pesData=g,h.pesData=m,d.pesData=y;var I={audioTrack:h,avcTrack:u,id3Track:d,textTrack:this._txtTrack};return s&&this.extractRemainingSamples(I),I},e.flush=function(){var t,e=this.remainderData;return this.remainderData=null,t=e?this.demux(e,-1,!1,!0):{audioTrack:this._audioTrack,avcTrack:this._avcTrack,textTrack:this._txtTrack,id3Track:this._id3Track},this.extractRemainingSamples(t),this.sampleAes?this.decrypt(t,this.sampleAes):t},e.extractRemainingSamples=function(t){var e,r=t.audioTrack,i=t.avcTrack,a=t.id3Track,n=i.pesData,s=r.pesData,o=a.pesData;n&&(e=V(n))?(this.parseAVCPES(e,!0),i.pesData=null):i.pesData=n,s&&(e=V(s))?(r.isAAC?this.parseAACPES(e):this.parseMPEGPES(e),r.pesData=null):(null!=s&&s.size&&f.b.log("last AAC PES packet truncated,might overlap between fragments"),r.pesData=s),o&&(e=V(o))?(this.parseID3PES(e),a.pesData=null):a.pesData=o},e.demuxSampleAes=function(t,e,r){var i=this.demux(t,r,!0,!this.config.progressive),a=this.sampleAes=new U(this.observer,this.config,e);return this.decrypt(i,a)},e.decrypt=function(t,e){return new Promise((function(r){var i=t.audioTrack,a=t.avcTrack;i.samples&&i.isAAC?e.decryptAacSamples(i.samples,0,(function(){a.samples?e.decryptAvcSamples(a.samples,0,0,(function(){r(t)})):r(t)})):a.samples&&e.decryptAvcSamples(a.samples,0,0,(function(){r(t)}))}))},e.destroy=function(){this._initPTS=this._initDTS=null,this._duration=0},e.parseAVCPES=function(t,e){var r,i=this,a=this._avcTrack,n=this.parseAVCNALu(t.data),s=this.avcSample,l=!1;t.data=null,s&&n.length&&!a.audFound&&(W(s,a),s=this.avcSample=K(!1,t.pts,t.dts,"")),n.forEach((function(e){switch(e.type){case 1:r=!0,s||(s=i.avcSample=K(!0,t.pts,t.dts,"")),s.frame=!0;var n=e.data;if(l&&n.length>4){var u=new N(n).readSliceType();2!==u&&4!==u&&7!==u&&9!==u||(s.key=!0)}break;case 5:r=!0,s||(s=i.avcSample=K(!0,t.pts,t.dts,"")),s.key=!0,s.frame=!0;break;case 6:r=!0;var h=new N(q(e.data));h.readUByte();for(var d=0,c=0,f=!1,g=0;!f&&h.bytesAvailable>1;){d=0;do{d+=g=h.readUByte()}while(255===g);c=0;do{c+=g=h.readUByte()}while(255===g);if(4===d&&0!==h.bytesAvailable){if(f=!0,181===h.readUByte())if(49===h.readUShort())if(1195456820===h.readUInt())if(3===h.readUByte()){for(var v=h.readUByte(),p=31&v,m=[v,h.readUByte()],y=0;y<p;y++)m.push(h.readUByte()),m.push(h.readUByte()),m.push(h.readUByte());Y(i._txtTrack.samples,{type:3,pts:t.pts,bytes:m})}}else if(5===d&&0!==h.bytesAvailable){if(f=!0,c>16){for(var T=[],E=0;E<16;E++)T.push(h.readUByte().toString(16)),3!==E&&5!==E&&7!==E&&9!==E||T.push("-");for(var b=c-16,S=new Uint8Array(b),L=0;L<b;L++)S[L]=h.readUByte();Y(i._txtTrack.samples,{pts:t.pts,payloadType:d,uuid:T.join(""),userData:Object(o.f)(S),userDataBytes:S})}}else if(c<h.bytesAvailable)for(var A=0;A<c;A++)h.readUByte()}break;case 7:if(r=!0,l=!0,!a.sps){var R=new N(e.data).readSPS();a.width=R.width,a.height=R.height,a.pixelRatio=R.pixelRatio,a.sps=[e.data],a.duration=i._duration;for(var D=e.data.subarray(1,4),k="avc1.",_=0;_<3;_++){var I=D[_].toString(16);I.length<2&&(I="0"+I),k+=I}a.codec=k}break;case 8:r=!0,a.pps||(a.pps=[e.data]);break;case 9:r=!1,a.audFound=!0,s&&W(s,a),s=i.avcSample=K(!1,t.pts,t.dts,"");break;case 12:r=!1;break;default:r=!1,s&&(s.debug+="unknown NAL "+e.type+" ")}s&&r&&s.units.push(e)})),e&&s&&(W(s,a),this.avcSample=null)},e.getLastNalUnit=function(){var t,e,r=this.avcSample;if(!r||0===r.units.length){var i=this._avcTrack.samples;r=i[i.length-1]}if(null!==(t=r)&&void 0!==t&&t.units){var a=r.units;e=a[a.length-1]}return e},e.parseAVCNALu=function(t){var e,r,i=t.byteLength,a=this._avcTrack,n=a.naluState||0,s=n,o=[],l=0,u=-1,h=0;for(-1===n&&(u=0,h=31&t[0],n=0,l=1);l<i;)if(e=t[l++],n)if(1!==n)if(e)if(1===e){if(u>=0){var d={data:t.subarray(u,l-n-1),type:h};o.push(d)}else{var c=this.getLastNalUnit();if(c&&(s&&l<=4-s&&c.state&&(c.data=c.data.subarray(0,c.data.byteLength-s)),(r=l-n-1)>0)){var f=new Uint8Array(c.data.byteLength+r);f.set(c.data,0),f.set(t.subarray(0,r),c.data.byteLength),c.data=f}}l<i?(u=l,h=31&t[l],n=0):n=-1}else n=0;else n=3;else n=e?0:2;else n=e?0:1;if(u>=0&&n>=0){var g={data:t.subarray(u,i),type:h,state:n};o.push(g)}if(0===o.length){var v=this.getLastNalUnit();if(v){var p=new Uint8Array(v.data.byteLength+t.byteLength);p.set(v.data,0),p.set(t,v.data.byteLength),v.data=p}}return a.naluState=n,o},e.parseAACPES=function(t){var e,r,n,s,o=this._audioTrack,l=this.aacLastPTS,u=this.aacOverFlow,h=t.data;if(u){var d=new Uint8Array(u.byteLength+h.byteLength);d.set(u,0),d.set(h,u.byteLength),h=d}for(e=0,r=h.length;e<r-1&&!m(h,e);e++);if(e&&(e<r-1?(n="AAC PES did not start with ADTS header,offset:"+e,s=!1):(n="no ADTS header found in AAC PES",s=!0),f.b.warn("parsing error:"+n),this.observer.emit(i.a.ERROR,i.a.ERROR,{type:a.b.MEDIA_ERROR,details:a.a.FRAG_PARSING_ERROR,fatal:s,reason:n}),s))return;T(o,this.observer,h,e,this.audioCodec);var c,g=0,v=E(o.samplerate);if(void 0!==t.pts)c=t.pts;else{if(null===l)return void f.b.warn("[tsdemuxer]: AAC PES unknown PTS");c=l}if(u&&null!==l){var p=l+v;Math.abs(p-c)>1&&(f.b.log("[tsdemuxer]: AAC: align PTS for overlapping frames by "+Math.round((p-c)/90)),c=p)}for(var y=null;e<r;){if(m(h,e)){if(e+5<r){var S=b(o,h,e,c,g);if(S){e+=S.length,y=S.sample.pts,g++;continue}}break}e++}this.aacOverFlow=e<r?h.subarray(e,r):null,this.aacLastPTS=y},e.parseMPEGPES=function(t){var e=t.data,r=e.length,i=0,a=0,n=t.pts;if(void 0!==n)for(;a<r;)if(M(e,a)){var s=O(this._audioTrack,e,a,n,i);if(!s)break;a+=s.length,i++}else a++;else f.b.warn("[tsdemuxer]: MPEG PES unknown PTS")},e.parseID3PES=function(t){void 0!==t.pts?this._id3Track.samples.push(t):f.b.warn("[tsdemuxer]: ID3 PES unknown PTS")},t}();function K(t,e,r,i){return{key:t,frame:!1,pts:e,dts:r,units:[],debug:i,length:0}}function j(t,e){return(31&t[e+10])<<8|t[e+11]}function H(t,e,r,i){var a={audio:-1,avc:-1,id3:-1,isAAC:!0},n=e+3+((15&t[e+1])<<8|t[e+2])-4;for(e+=12+((15&t[e+10])<<8|t[e+11]);e<n;){var s=(31&t[e+1])<<8|t[e+2];switch(t[e]){case 207:if(!i){f.b.log("ADTS AAC with AES-128-CBC frame encryption found in unencrypted stream");break}case 15:-1===a.audio&&(a.audio=s);break;case 21:-1===a.id3&&(a.id3=s);break;case 219:if(!i){f.b.log("H.264 with AES-128-CBC slice encryption found in unencrypted stream");break}case 27:-1===a.avc&&(a.avc=s);break;case 3:case 4:r?-1===a.audio&&(a.audio=s,a.isAAC=!1):f.b.log("MPEG audio found, not supported in this browser");break;case 36:f.b.warn("Unsupported HEVC stream type found")}e+=5+((15&t[e+3])<<8|t[e+4])}return a}function V(t){var e,r,i,a,n,s=0,o=t.data;if(!t||0===t.size)return null;for(;o[0].length<19&&o.length>1;){var l=new Uint8Array(o[0].length+o[1].length);l.set(o[0]),l.set(o[1],o[0].length),o[0]=l,o.splice(1,1)}if(1===((e=o[0])[0]<<16)+(e[1]<<8)+e[2]){if((r=(e[4]<<8)+e[5])&&r>t.size-6)return null;var u=e[7];192&u&&(a=536870912*(14&e[9])+4194304*(255&e[10])+16384*(254&e[11])+128*(255&e[12])+(254&e[13])/2,64&u?a-(n=536870912*(14&e[14])+4194304*(255&e[15])+16384*(254&e[16])+128*(255&e[17])+(254&e[18])/2)>54e5&&(f.b.warn(Math.round((a-n)/9e4)+"s delta between PTS and DTS, align them"),a=n):n=a);var h=(i=e[8])+9;if(t.size<=h)return null;t.size-=h;for(var d=new Uint8Array(t.size),c=0,g=o.length;c<g;c++){var v=(e=o[c]).byteLength;if(h){if(h>v){h-=v;continue}e=e.subarray(h),v-=h,h=0}d.set(e,s),s+=v}return r&&(r-=i+3),{data:d,pts:a,dts:n,len:r}}return null}function W(t,e){if(t.units.length&&t.frame){if(void 0===t.pts){var r=e.samples,i=r.length;if(!i)return void e.dropped++;var a=r[i-1];t.pts=a.pts,t.dts=a.dts}e.samples.push(t)}t.debug.length&&f.b.log(t.pts+"/"+t.dts+":"+t.debug)}function Y(t,e){var r=t.length;if(r>0){if(e.pts>=t[r-1].pts)t.push(e);else for(var i=r-1;i>=0;i--)if(e.pts<t[i].pts){t.splice(i,0,e);break}}else t.push(e)}function q(t){for(var e=t.byteLength,r=[],i=1;i<e-2;)0===t[i]&&0===t[i+1]&&3===t[i+2]?(r.push(i+2),i+=2):i++;if(0===r.length)return t;var a=e-r.length,n=new Uint8Array(a),s=0;for(i=0;i<a;s++,i++)s===r[0]&&(s++,r.shift()),n[i]=t[s];return n}G.minProbeByteLength=188;var X=G;function z(t,e){return(z=Object.setPrototypeOf||function(t,e){return t.__proto__=e,t})(t,e)}var Q=function(t){var e,r;function i(){return t.apply(this,arguments)||this}r=t,(e=i).prototype=Object.create(r.prototype),e.prototype.constructor=e,z(e,r);var a=i.prototype;return a.resetInitSegment=function(e,r,i){t.prototype.resetInitSegment.call(this,e,r,i),this._audioTrack={container:"audio/mpeg",type:"audio",id:0,pid:-1,sequenceNumber:0,isAAC:!1,samples:[],manifestCodec:e,duration:i,inputTimeScale:9e4,dropped:0}},i.probe=function(t){if(!t)return!1;for(var e=(o.b(t,0)||[]).length,r=t.length;e<r;e++)if(F(t,e))return f.b.log("MPEG Audio sync word found !"),!0;return!1},a.canParse=function(t,e){return function(t,e){return P(t,e)&&4<=t.length-e}(t,e)},a.appendFrame=function(t,e,r){if(null!==this.initPTS)return O(t,e,r,this.initPTS,this.frameIndex)},i}(c);Q.minProbeByteLength=4;var $=Q,J=r(15),Z=r(4),tt=function(){function t(){this.emitInitSegment=!1,this.audioCodec=void 0,this.videoCodec=void 0,this.initData=void 0,this.initPTS=void 0,this.initTracks=void 0,this.lastEndDTS=null}var e=t.prototype;return e.destroy=function(){},e.resetTimeStamp=function(t){this.initPTS=t,this.lastEndDTS=null},e.resetNextTimestamp=function(){this.lastEndDTS=null},e.resetInitSegment=function(t,e,r){this.audioCodec=e,this.videoCodec=r,this.generateInitSegment(t),this.emitInitSegment=!0},e.generateInitSegment=function(t){var e=this.audioCodec,r=this.videoCodec;if(!t||!t.byteLength)return this.initTracks=void 0,void(this.initData=void 0);var i=this.initData=Object(l.f)(t);e||(e=rt(i.audio,Z.a.AUDIO)),r||(r=rt(i.video,Z.a.VIDEO));var a={};i.audio&&i.video?a.audiovideo={container:"video/mp4",codec:e+","+r,initSegment:t,id:"main"}:i.audio?a.audio={container:"audio/mp4",codec:e,initSegment:t,id:"audio"}:i.video?a.video={container:"video/mp4",codec:r,initSegment:t,id:"main"}:f.b.warn("[passthrough-remuxer.ts]: initSegment does not contain moov or trak boxes."),this.initTracks=a},e.remux=function(t,e,r,i,a){var n=this.initPTS,o=this.lastEndDTS,u={audio:void 0,video:void 0,text:i,id3:r,initSegment:void 0};Object(s.a)(o)||(o=this.lastEndDTS=a||0);var h=e.samples;if(!h||!h.length)return u;var d={initPTS:void 0,timescale:1},c=this.initData;if(c&&c.length||(this.generateInitSegment(h),c=this.initData),!c||!c.length)return f.b.warn("[passthrough-remuxer.ts]: Failed to generate initSegment."),u;this.emitInitSegment&&(d.tracks=this.initTracks,this.emitInitSegment=!1),Object(s.a)(n)||(this.initPTS=d.initPTS=n=et(c,h,o));var g=Object(l.c)(h,c),v=o,p=g+v;Object(l.e)(c,h,n),g>0?this.lastEndDTS=p:(f.b.warn("Duration parsed from mp4 should be greater than zero"),this.resetNextTimestamp());var m=!!c.audio,y=!!c.video,T="";m&&(T+="audio"),y&&(T+="video");var E={data1:h,startPTS:v,startDTS:v,endPTS:p,endDTS:p,type:T,hasAudio:m,hasVideo:y,nb:1,dropped:0};return u.audio="audio"===E.type?E:void 0,u.video="audio"!==E.type?E:void 0,u.text=i,u.id3=r,u.initSegment=d,u},t}(),et=function(t,e,r){return Object(l.d)(t,e)-r};function rt(t,e){var r=null==t?void 0:t.codec;return r&&r.length>4?r:"hvc1"===r?"hvc1.1.c.L120.90":"av01"===r?"av01.0.04M.08":"avc1"===r||e===Z.a.VIDEO?"avc1.42e01e":"mp4a.40.5"}var it,at=tt,nt=r(12);try{it=self.performance.now.bind(self.performance)}catch(t){f.b.debug("Unable to use Performance API on this environment"),it=self.Date.now}var st=[{demux:X,remux:J.a},{demux:D,remux:at},{demux:A,remux:J.a},{demux:$,remux:J.a}],ot=1024;st.forEach((function(t){var e=t.demux;ot=Math.max(ot,e.minProbeByteLength)}));var lt=function(){function t(t,e,r,i){this.observer=void 0,this.typeSupported=void 0,this.config=void 0,this.vendor=void 0,this.demuxer=void 0,this.remuxer=void 0,this.decrypter=void 0,this.probe=void 0,this.decryptionPromise=null,this.transmuxConfig=void 0,this.currentTransmuxState=void 0,this.cache=new nt.a,this.observer=t,this.typeSupported=e,this.config=r,this.vendor=i}var e=t.prototype;return e.configure=function(t){this.transmuxConfig=t,this.decrypter&&this.decrypter.reset()},e.push=function(t,e,r,i){var a=this,n=r.transmuxing;n.executeStart=it();var s=new Uint8Array(t),o=this.cache,u=this.config,h=this.currentTransmuxState,d=this.transmuxConfig;i&&(this.currentTransmuxState=i);var c=function(t,e){var r=null;t.byteLength>0&&null!=e&&null!=e.key&&null!==e.iv&&null!=e.method&&(r=e);return r}(s,e);if(c&&"AES-128"===c.method){var f=this.getDecrypter();if(!u.enableSoftwareAES)return this.decryptionPromise=f.webCryptoDecrypt(s,c.key.buffer,c.iv.buffer).then((function(t){var e=a.push(t,null,r);return a.decryptionPromise=null,e})),this.decryptionPromise;var g=f.softwareDecrypt(s,c.key.buffer,c.iv.buffer);if(!g)return n.executeEnd=it(),ut(r);s=new Uint8Array(g)}var v=i||h,p=v.contiguous,m=v.discontinuity,y=v.trackSwitch,T=v.accurateTimeOffset,E=v.timeOffset,b=d.audioCodec,S=d.videoCodec,L=d.defaultInitPts,A=d.duration,R=d.initSegmentData;if((m||y)&&this.resetInitSegment(R,b,S,A),m&&this.resetInitialTimestamp(L),p||this.resetContiguity(),this.needsProbing(s,m,y)){if(o.dataLength){var D=o.flush();s=Object(l.a)(D,s)}this.configureTransmuxer(s,d)}var k=this.transmux(s,c,E,T,r),_=this.currentTransmuxState;return _.contiguous=!0,_.discontinuity=!1,_.trackSwitch=!1,n.executeEnd=it(),k},e.flush=function(t){var e=this,r=t.transmuxing;r.executeStart=it();var n=this.decrypter,s=this.cache,o=this.currentTransmuxState,l=this.decryptionPromise;if(l)return l.then((function(){return e.flush(t)}));var u=[],h=o.timeOffset;if(n){var d=n.flush();d&&u.push(this.push(d,null,t))}var c=s.dataLength;s.reset();var f=this.demuxer,g=this.remuxer;if(!f||!g)return c>=ot&&this.observer.emit(i.a.ERROR,i.a.ERROR,{type:a.b.MEDIA_ERROR,details:a.a.FRAG_PARSING_ERROR,fatal:!0,reason:"no demux matching with content found"}),r.executeEnd=it(),[ut(t)];var v=f.flush(h);return ht(v)?v.then((function(r){return e.flushRemux(u,r,t),u})):(this.flushRemux(u,v,t),u)},e.flushRemux=function(t,e,r){var i=e.audioTrack,a=e.avcTrack,n=e.id3Track,s=e.textTrack,o=this.currentTransmuxState,l=o.accurateTimeOffset,u=o.timeOffset;f.b.log("[transmuxer.ts]: Flushed fragment "+r.sn+(r.part>-1?" p: "+r.part:"")+" of level "+r.level);var h=this.remuxer.remux(i,a,n,s,u,l,!0);t.push({remuxResult:h,chunkMeta:r}),r.transmuxing.executeEnd=it()},e.resetInitialTimestamp=function(t){var e=this.demuxer,r=this.remuxer;e&&r&&(e.resetTimeStamp(t),r.resetTimeStamp(t))},e.resetContiguity=function(){var t=this.demuxer,e=this.remuxer;t&&e&&(t.resetContiguity(),e.resetNextTimestamp())},e.resetInitSegment=function(t,e,r,i){var a=this.demuxer,n=this.remuxer;a&&n&&(a.resetInitSegment(e,r,i),n.resetInitSegment(t,e,r))},e.destroy=function(){this.demuxer&&(this.demuxer.destroy(),this.demuxer=void 0),this.remuxer&&(this.remuxer.destroy(),this.remuxer=void 0)},e.transmux=function(t,e,r,i,a){return e&&"SAMPLE-AES"===e.method?this.transmuxSampleAes(t,e,r,i,a):this.transmuxUnencrypted(t,r,i,a)},e.transmuxUnencrypted=function(t,e,r,i){var a=this.demuxer.demux(t,e,!1,!this.config.progressive),n=a.audioTrack,s=a.avcTrack,o=a.id3Track,l=a.textTrack;return{remuxResult:this.remuxer.remux(n,s,o,l,e,r,!1),chunkMeta:i}},e.transmuxSampleAes=function(t,e,r,i,a){var n=this;return this.demuxer.demuxSampleAes(t,e,r).then((function(t){return{remuxResult:n.remuxer.remux(t.audioTrack,t.avcTrack,t.id3Track,t.textTrack,r,i,!1),chunkMeta:a}}))},e.configureTransmuxer=function(t,e){for(var r,i=this.config,a=this.observer,n=this.typeSupported,s=this.vendor,o=e.audioCodec,l=e.defaultInitPts,u=e.duration,h=e.initSegmentData,d=e.videoCodec,c=0,g=st.length;c<g;c++)if(st[c].demux.probe(t)){r=st[c];break}r||(f.b.warn("Failed to find demuxer by probing frag, treating as mp4 passthrough"),r={demux:D,remux:at});var v=this.demuxer,p=this.remuxer,m=r.remux,y=r.demux;p&&p instanceof m||(this.remuxer=new m(a,i,n,s)),v&&v instanceof y||(this.demuxer=new y(a,i,n),this.probe=y.probe),this.resetInitSegment(h,o,d,u),this.resetInitialTimestamp(l)},e.needsProbing=function(t,e,r){return!this.demuxer||!this.remuxer||e||r},e.getDecrypter=function(){var t=this.decrypter;return t||(t=this.decrypter=new n.a(this.observer,this.config)),t},t}();var ut=function(t){return{remuxResult:{},chunkMeta:t}};function ht(t){return"then"in t&&t.then instanceof Function}var dt=function(t,e,r,i,a){this.audioCodec=void 0,this.videoCodec=void 0,this.initSegmentData=void 0,this.duration=void 0,this.defaultInitPts=void 0,this.audioCodec=t,this.videoCodec=e,this.initSegmentData=r,this.duration=i,this.defaultInitPts=a},ct=function(t,e,r,i,a){this.discontinuity=void 0,this.contiguous=void 0,this.accurateTimeOffset=void 0,this.trackSwitch=void 0,this.timeOffset=void 0,this.discontinuity=t,this.contiguous=e,this.accurateTimeOffset=r,this.trackSwitch=i,this.timeOffset=a}},12: function(t,e,r){"use strict";r.d(e,"a",(function(){return i}));var i=function(){function t(){this.chunks=[],this.dataLength=0}var e=t.prototype;return e.push=function(t){this.chunks.push(t),this.dataLength+=t.length},e.flush=function(){var t,e=this.chunks,r=this.dataLength;return e.length?(t=1===e.length?e[0]:function(t,e){for(var r=new Uint8Array(e),i=0,a=0;a<t.length;a++){var n=t[a];r.set(n,i),i+=n.length}return r}(e,r),this.reset(),t):new Uint8Array(0)},e.reset=function(){this.chunks.length=0,this.dataLength=0},t}()},4: function(t,e,r){"use strict";r.d(e,"a",(function(){return i})),r.d(e,"b",(function(){return g})),r.d(e,"c",(function(){return v}));var i,a=r(3),n=r(10),s=r(1),o=r(16),l=r(11);function u(t,e){t.prototype=Object.create(e.prototype),t.prototype.constructor=t,h(t,e)}function h(t,e){return(h=Object.setPrototypeOf||function(t,e){return t.__proto__=e,t})(t,e)}function d(t,e){for(var r=0;r<e.length;r++){var i=e[r];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(t,i.key,i)}}function c(t,e,r){return e&&d(t.prototype,e),r&&d(t,r),t}!function(t){t.AUDIO="audio",t.VIDEO="video",t.AUDIOVIDEO="audiovideo"}(i||(i={}));var f=function(){function t(t){var e;this._byteRange=null,this._url=null,this.baseurl=void 0,this.relurl=void 0,this.elementaryStreams=((e={})[i.AUDIO]=null,e[i.VIDEO]=null,e[i.AUDIOVIDEO]=null,e),this.baseurl=t}return t.prototype.setByteRange=function(t,e){var r=t.split("@",2),i=[];1===r.length?i[0]=e?e.byteRangeEndOffset:0:i[0]=parseInt(r[1]),i[1]=parseInt(r[0])+i[0],this._byteRange=i},c(t,[{key:"byteRange",get:function(){return this._byteRange?this._byteRange:[]}},{key:"byteRangeStartOffset",get:function(){return this.byteRange[0]}},{key:"byteRangeEndOffset",get:function(){return this.byteRange[1]}},{key:"url",get:function(){return!this._url&&this.baseurl&&this.relurl&&(this._url=Object(n.buildAbsoluteURL)(this.baseurl,this.relurl,{alwaysNormalize:!0})),this._url||""},set:function(t){this._url=t}}]),t}(),g=function(t){function e(e,r){var i;return(i=t.call(this,r)||this)._decryptdata=null,i.rawProgramDateTime=null,i.programDateTime=null,i.tagList=[],i.duration=0,i.sn=0,i.levelkey=void 0,i.type=void 0,i.loader=null,i.level=-1,i.cc=0,i.startPTS=void 0,i.endPTS=void 0,i.appendedPTS=void 0,i.startDTS=void 0,i.endDTS=void 0,i.start=0,i.deltaPTS=void 0,i.maxStartPTS=void 0,i.minEndPTS=void 0,i.stats=new l.a,i.urlId=0,i.data=void 0,i.bitrateTest=!1,i.title=null,i.type=e,i}u(e,t);var r=e.prototype;return r.createInitializationVector=function(t){for(var e=new Uint8Array(16),r=12;r<16;r++)e[r]=t>>8*(15-r)&255;return e},r.setDecryptDataFromLevelKey=function(t,e){var r=t;return"AES-128"===(null==t?void 0:t.method)&&t.uri&&!t.iv&&((r=o.a.fromURI(t.uri)).method=t.method,r.iv=this.createInitializationVector(e),r.keyFormat="identity"),r},r.setElementaryStreamInfo=function(t,e,r,i,a,n){void 0===n&&(n=!1);var s=this.elementaryStreams,o=s[t];o?(o.startPTS=Math.min(o.startPTS,e),o.endPTS=Math.max(o.endPTS,r),o.startDTS=Math.min(o.startDTS,i),o.endDTS=Math.max(o.endDTS,a)):s[t]={startPTS:e,endPTS:r,startDTS:i,endDTS:a,partial:n}},r.clearElementaryStreamInfo=function(){var t=this.elementaryStreams;t[i.AUDIO]=null,t[i.VIDEO]=null,t[i.AUDIOVIDEO]=null},c(e,[{key:"decryptdata",get:function(){if(!this.levelkey&&!this._decryptdata)return null;if(!this._decryptdata&&this.levelkey){var t=this.sn;"number"!=typeof t&&(this.levelkey&&"AES-128"===this.levelkey.method&&!this.levelkey.iv&&s.b.warn('missing IV for initialization segment with method="'+this.levelkey.method+'" - compliance issue'),t=0),this._decryptdata=this.setDecryptDataFromLevelKey(this.levelkey,t)}return this._decryptdata}},{key:"end",get:function(){return this.start+this.duration}},{key:"endProgramDateTime",get:function(){if(null===this.programDateTime)return null;if(!Object(a.a)(this.programDateTime))return null;var t=Object(a.a)(this.duration)?this.duration:0;return this.programDateTime+1e3*t}},{key:"encrypted",get:function(){var t;return!(null===(t=this.decryptdata)||void 0===t||!t.keyFormat||!this.decryptdata.uri)}}]),e}(f),v=function(t){function e(e,r,i,a,n){var s;(s=t.call(this,i)||this).fragOffset=0,s.duration=0,s.gap=!1,s.independent=!1,s.relurl=void 0,s.fragment=void 0,s.index=void 0,s.stats=new l.a,s.duration=e.decimalFloatingPoint("DURATION"),s.gap=e.bool("GAP"),s.independent=e.bool("INDEPENDENT"),s.relurl=e.enumeratedString("URI"),s.fragment=r,s.index=a;var o=e.enumeratedString("BYTERANGE");return o&&s.setByteRange(o,n),n&&(s.fragOffset=n.fragOffset+n.duration),s}return u(e,t),c(e,[{key:"start",get:function(){return this.fragment.start+this.fragOffset}},{key:"end",get:function(){return this.start+this.duration}},{key:"loaded",get:function(){var t=this.elementaryStreams;return!!(t.audio||t.video||t.audiovideo)}}]),e}(f)},11: function(t,e,r){"use strict";r.d(e,"a",(function(){return i}));var i=function(){this.aborted=!1,this.loaded=0,this.retry=0,this.total=0,this.chunkCount=0,this.bwEstimate=0,this.loading={start:0,first:0,end:0},this.parsing={start:0,end:0},this.buffering={start:0,first:0,end:0}}},16: function(t,e,r){"use strict";r.d(e,"a",(function(){return n}));var i=r(10);function a(t,e){for(var r=0;r<e.length;r++){var i=e[r];i.enumerable=i.enumerable||!1,i.configurable=!0,"value"in i&&(i.writable=!0),Object.defineProperty(t,i.key,i)}}var n=function(){function t(t,e){this._uri=null,this.method=null,this.keyFormat=null,this.keyFormatVersions=null,this.keyID=null,this.key=null,this.iv=null,this._uri=e?Object(i.buildAbsoluteURL)(t,e,{alwaysNormalize:!0}):t}var e,r,n;return t.fromURL=function(e,r){return new t(e,r)},t.fromURI=function(e){return new t(e)},e=t,(r=[{key:"uri",get:function(){return this._uri}}])&&a(e.prototype,r),n&&a(e,n),t}()},10: function(t,e,r){var i,a,n,s,o;i=/^((?:[a-zA-Z0-9+\-.]+:)?)(\/\/[^\/?#]*)?((?:[^\/?#]*\/)*[^;?#]*)?(;[^?#]*)?(\?[^#]*)?(#[^]*)?$/,a=/^([^\/?#]*)([^]*)$/,n=/(?:\/|^)\.(?=\/)/g,s=/(?:\/|^)\.\.\/(?!\.\.\/)[^\/]*(?=\/)/g,o={buildAbsoluteURL:function(t,e,r){if(r=r||{},t=t.trim(),!(e=e.trim())){if(!r.alwaysNormalize)return t;var i=o.parseURL(t);if(!i)throw new Error("Error trying to parse base URL.");return i.path=o.normalizePath(i.path),o.buildURLFromParts(i)}var n=o.parseURL(e);if(!n)throw new Error("Error trying to parse relative URL.");if(n.scheme)return r.alwaysNormalize?(n.path=o.normalizePath(n.path),o.buildURLFromParts(n)):e;var s=o.parseURL(t);if(!s)throw new Error("Error trying to parse base URL.");if(!s.netLoc&&s.path&&"/"!==s.path[0]){var l=a.exec(s.path);s.netLoc=l[1],s.path=l[2]}s.netLoc&&!s.path&&(s.path="/");var u={scheme:s.scheme,netLoc:n.netLoc,path:null,params:n.params,query:n.query,fragment:n.fragment};if(!n.netLoc&&(u.netLoc=s.netLoc,"/"!==n.path[0]))if(n.path){var h=s.path,d=h.substring(0,h.lastIndexOf("/")+1)+n.path;u.path=o.normalizePath(d)}else u.path=s.path,n.params||(u.params=s.params,n.query||(u.query=s.query));return null===u.path&&(u.path=r.alwaysNormalize?o.normalizePath(n.path):n.path),o.buildURLFromParts(u)},parseURL:function(t){var e=i.exec(t);return e?{scheme:e[1]||"",netLoc:e[2]||"",path:e[3]||"",params:e[4]||"",query:e[5]||"",fragment:e[6]||""}:null},normalizePath:function(t){for(t=t.split("").reverse().join("").replace(n,"");t.length!==(t=t.replace(s,"")).length;);return t.split("").reverse().join("")},buildURLFromParts:function(t){return t.scheme+t.netLoc+t.path+t.params+t.query+t.fragment}},t.exports=o},3: function(t,e,r){"use strict";r.d(e,"a",(function(){return i}));var i=Number.isFinite||function(t){return"number"==typeof t&&isFinite(t)};Number.MAX_SAFE_INTEGER},15: function(t,e,r){"use strict";r.d(e,"a",(function(){return p})),r.d(e,"b",(function(){return m}));var i=r(3),a=function(){function t(){}return t.getSilentFrame=function(t,e){switch(t){case"mp4a.40.2":if(1===e)return new Uint8Array([0,200,0,128,35,128]);if(2===e)return new Uint8Array([33,0,73,144,2,25,0,35,128]);if(3===e)return new Uint8Array([0,200,0,128,32,132,1,38,64,8,100,0,142]);if(4===e)return new Uint8Array([0,200,0,128,32,132,1,38,64,8,100,0,128,44,128,8,2,56]);if(5===e)return new Uint8Array([0,200,0,128,32,132,1,38,64,8,100,0,130,48,4,153,0,33,144,2,56]);if(6===e)return new Uint8Array([0,200,0,128,32,132,1,38,64,8,100,0,130,48,4,153,0,33,144,2,0,178,0,32,8,224]);break;default:if(1===e)return new Uint8Array([1,64,34,128,163,78,230,128,186,8,0,0,0,28,6,241,193,10,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,94]);if(2===e)return new Uint8Array([1,64,34,128,163,94,230,128,186,8,0,0,0,0,149,0,6,241,161,10,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,94]);if(3===e)return new Uint8Array([1,64,34,128,163,94,230,128,186,8,0,0,0,0,149,0,6,241,161,10,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,90,94])}},t}(),n=Math.pow(2,32)-1,s=function(){function t(){}return t.init=function(){var e;for(e in t.types={avc1:[],avcC:[],btrt:[],dinf:[],dref:[],esds:[],ftyp:[],hdlr:[],mdat:[],mdhd:[],mdia:[],mfhd:[],minf:[],moof:[],moov:[],mp4a:[],".mp3":[],mvex:[],mvhd:[],pasp:[],sdtp:[],stbl:[],stco:[],stsc:[],stsd:[],stsz:[],stts:[],tfdt:[],tfhd:[],traf:[],trak:[],trun:[],trex:[],tkhd:[],vmhd:[],smhd:[]},t.types)t.types.hasOwnProperty(e)&&(t.types[e]=[e.charCodeAt(0),e.charCodeAt(1),e.charCodeAt(2),e.charCodeAt(3)]);var r=new Uint8Array([0,0,0,0,0,0,0,0,118,105,100,101,0,0,0,0,0,0,0,0,0,0,0,0,86,105,100,101,111,72,97,110,100,108,101,114,0]),i=new Uint8Array([0,0,0,0,0,0,0,0,115,111,117,110,0,0,0,0,0,0,0,0,0,0,0,0,83,111,117,110,100,72,97,110,100,108,101,114,0]);t.HDLR_TYPES={video:r,audio:i};var a=new Uint8Array([0,0,0,0,0,0,0,1,0,0,0,12,117,114,108,32,0,0,0,1]),n=new Uint8Array([0,0,0,0,0,0,0,0]);t.STTS=t.STSC=t.STCO=n,t.STSZ=new Uint8Array([0,0,0,0,0,0,0,0,0,0,0,0]),t.VMHD=new Uint8Array([0,0,0,1,0,0,0,0,0,0,0,0]),t.SMHD=new Uint8Array([0,0,0,0,0,0,0,0]),t.STSD=new Uint8Array([0,0,0,0,0,0,0,1]);var s=new Uint8Array([105,115,111,109]),o=new Uint8Array([97,118,99,49]),l=new Uint8Array([0,0,0,1]);t.FTYP=t.box(t.types.ftyp,s,l,s,o),t.DINF=t.box(t.types.dinf,t.box(t.types.dref,a))},t.box=function(t){for(var e=8,r=arguments.length,i=new Array(r>1?r-1:0),a=1;a<r;a++)i[a-1]=arguments[a];for(var n=i.length,s=n;n--;)e+=i[n].byteLength;var o=new Uint8Array(e);for(o[0]=e>>24&255,o[1]=e>>16&255,o[2]=e>>8&255,o[3]=255&e,o.set(t,4),n=0,e=8;n<s;n++)o.set(i[n],e),e+=i[n].byteLength;return o},t.hdlr=function(e){return t.box(t.types.hdlr,t.HDLR_TYPES[e])},t.mdat=function(e){return t.box(t.types.mdat,e)},t.mdhd=function(e,r){r*=e;var i=Math.floor(r/(n+1)),a=Math.floor(r%(n+1));return t.box(t.types.mdhd,new Uint8Array([1,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,3,e>>24&255,e>>16&255,e>>8&255,255&e,i>>24,i>>16&255,i>>8&255,255&i,a>>24,a>>16&255,a>>8&255,255&a,85,196,0,0]))},t.mdia=function(e){return t.box(t.types.mdia,t.mdhd(e.timescale,e.duration),t.hdlr(e.type),t.minf(e))},t.mfhd=function(e){return t.box(t.types.mfhd,new Uint8Array([0,0,0,0,e>>24,e>>16&255,e>>8&255,255&e]))},t.minf=function(e){return"audio"===e.type?t.box(t.types.minf,t.box(t.types.smhd,t.SMHD),t.DINF,t.stbl(e)):t.box(t.types.minf,t.box(t.types.vmhd,t.VMHD),t.DINF,t.stbl(e))},t.moof=function(e,r,i){return t.box(t.types.moof,t.mfhd(e),t.traf(i,r))},t.moov=function(e){for(var r=e.length,i=[];r--;)i[r]=t.trak(e[r]);return t.box.apply(null,[t.types.moov,t.mvhd(e[0].timescale,e[0].duration)].concat(i).concat(t.mvex(e)))},t.mvex=function(e){for(var r=e.length,i=[];r--;)i[r]=t.trex(e[r]);return t.box.apply(null,[t.types.mvex].concat(i))},t.mvhd=function(e,r){r*=e;var i=Math.floor(r/(n+1)),a=Math.floor(r%(n+1)),s=new Uint8Array([1,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,3,e>>24&255,e>>16&255,e>>8&255,255&e,i>>24,i>>16&255,i>>8&255,255&i,a>>24,a>>16&255,a>>8&255,255&a,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,64,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,255,255,255]);return t.box(t.types.mvhd,s)},t.sdtp=function(e){var r,i,a=e.samples||[],n=new Uint8Array(4+a.length);for(r=0;r<a.length;r++)i=a[r].flags,n[r+4]=i.dependsOn<<4|i.isDependedOn<<2|i.hasRedundancy;return t.box(t.types.sdtp,n)},t.stbl=function(e){return t.box(t.types.stbl,t.stsd(e),t.box(t.types.stts,t.STTS),t.box(t.types.stsc,t.STSC),t.box(t.types.stsz,t.STSZ),t.box(t.types.stco,t.STCO))},t.avc1=function(e){var r,i,a,n=[],s=[];for(r=0;r<e.sps.length;r++)a=(i=e.sps[r]).byteLength,n.push(a>>>8&255),n.push(255&a),n=n.concat(Array.prototype.slice.call(i));for(r=0;r<e.pps.length;r++)a=(i=e.pps[r]).byteLength,s.push(a>>>8&255),s.push(255&a),s=s.concat(Array.prototype.slice.call(i));var o=t.box(t.types.avcC,new Uint8Array([1,n[3],n[4],n[5],255,224|e.sps.length].concat(n).concat([e.pps.length]).concat(s))),l=e.width,u=e.height,h=e.pixelRatio[0],d=e.pixelRatio[1];return t.box(t.types.avc1,new Uint8Array([0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,l>>8&255,255&l,u>>8&255,255&u,0,72,0,0,0,72,0,0,0,0,0,0,0,1,18,100,97,105,108,121,109,111,116,105,111,110,47,104,108,115,46,106,115,0,0,0,0,0,0,0,0,0,0,0,0,0,0,24,17,17]),o,t.box(t.types.btrt,new Uint8Array([0,28,156,128,0,45,198,192,0,45,198,192])),t.box(t.types.pasp,new Uint8Array([h>>24,h>>16&255,h>>8&255,255&h,d>>24,d>>16&255,d>>8&255,255&d])))},t.esds=function(t){var e=t.config.length;return new Uint8Array([0,0,0,0,3,23+e,0,1,0,4,15+e,64,21,0,0,0,0,0,0,0,0,0,0,0,5].concat([e]).concat(t.config).concat([6,1,2]))},t.mp4a=function(e){var r=e.samplerate;return t.box(t.types.mp4a,new Uint8Array([0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,e.channelCount,0,16,0,0,0,0,r>>8&255,255&r,0,0]),t.box(t.types.esds,t.esds(e)))},t.mp3=function(e){var r=e.samplerate;return t.box(t.types[".mp3"],new Uint8Array([0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,e.channelCount,0,16,0,0,0,0,r>>8&255,255&r,0,0]))},t.stsd=function(e){return"audio"===e.type?e.isAAC||"mp3"!==e.codec?t.box(t.types.stsd,t.STSD,t.mp4a(e)):t.box(t.types.stsd,t.STSD,t.mp3(e)):t.box(t.types.stsd,t.STSD,t.avc1(e))},t.tkhd=function(e){var r=e.id,i=e.duration*e.timescale,a=e.width,s=e.height,o=Math.floor(i/(n+1)),l=Math.floor(i%(n+1));return t.box(t.types.tkhd,new Uint8Array([1,0,0,7,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,3,r>>24&255,r>>16&255,r>>8&255,255&r,0,0,0,0,o>>24,o>>16&255,o>>8&255,255&o,l>>24,l>>16&255,l>>8&255,255&l,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,64,0,0,0,a>>8&255,255&a,0,0,s>>8&255,255&s,0,0]))},t.traf=function(e,r){var i=t.sdtp(e),a=e.id,s=Math.floor(r/(n+1)),o=Math.floor(r%(n+1));return t.box(t.types.traf,t.box(t.types.tfhd,new Uint8Array([0,0,0,0,a>>24,a>>16&255,a>>8&255,255&a])),t.box(t.types.tfdt,new Uint8Array([1,0,0,0,s>>24,s>>16&255,s>>8&255,255&s,o>>24,o>>16&255,o>>8&255,255&o])),t.trun(e,i.length+16+20+8+16+8+8),i)},t.trak=function(e){return e.duration=e.duration||4294967295,t.box(t.types.trak,t.tkhd(e),t.mdia(e))},t.trex=function(e){var r=e.id;return t.box(t.types.trex,new Uint8Array([0,0,0,0,r>>24,r>>16&255,r>>8&255,255&r,0,0,0,1,0,0,0,0,0,0,0,0,0,1,0,1]))},t.trun=function(e,r){var i,a,n,s,o,l,u=e.samples||[],h=u.length,d=12+16*h,c=new Uint8Array(d);for(r+=8+d,c.set([0,0,15,1,h>>>24&255,h>>>16&255,h>>>8&255,255&h,r>>>24&255,r>>>16&255,r>>>8&255,255&r],0),i=0;i<h;i++)n=(a=u[i]).duration,s=a.size,o=a.flags,l=a.cts,c.set([n>>>24&255,n>>>16&255,n>>>8&255,255&n,s>>>24&255,s>>>16&255,s>>>8&255,255&s,o.isLeading<<2|o.dependsOn,o.isDependedOn<<6|o.hasRedundancy<<4|o.paddingValue<<1|o.isNonSync,61440&o.degradPrio,15&o.degradPrio,l>>>24&255,l>>>16&255,l>>>8&255,255&l],12+16*i);return t.box(t.types.trun,c)},t.initSegment=function(e){t.types||t.init();var r=t.moov(e),i=new Uint8Array(t.FTYP.byteLength+r.byteLength);return i.set(t.FTYP),i.set(r,t.FTYP.byteLength),i},t}();s.types=void 0,s.HDLR_TYPES=void 0,s.STTS=void 0,s.STSC=void 0,s.STCO=void 0,s.STSZ=void 0,s.VMHD=void 0,s.SMHD=void 0,s.STSD=void 0,s.FTYP=void 0,s.DINF=void 0;var o=s,l=r(0),u=r(2),h=r(1),d=r(7);function c(){return(c=Object.assign||function(t){for(var e=1;e<arguments.length;e++){var r=arguments[e];for(var i in r)Object.prototype.hasOwnProperty.call(r,i)&&(t[i]=r[i])}return t}).apply(this,arguments)}var f=null,g=null,v=!1,p=function(){function t(t,e,r,i){if(void 0===i&&(i=""),this.observer=void 0,this.config=void 0,this.typeSupported=void 0,this.ISGenerated=!1,this._initPTS=void 0,this._initDTS=void 0,this.nextAvcDts=null,this.nextAudioPts=null,this.isAudioContiguous=!1,this.isVideoContiguous=!1,this.observer=t,this.config=e,this.typeSupported=r,this.ISGenerated=!1,null===f){var a=(navigator.userAgent||"").match(/Chrome\/(\d+)/i);f=a?parseInt(a[1]):0}if(null===g){var n=navigator.userAgent.match(/Safari\/(\d+)/i);g=n?parseInt(n[1]):0}v=!!f&&f<75||!!g&&g<600}var e=t.prototype;return e.destroy=function(){},e.resetTimeStamp=function(t){h.b.log("[mp4-remuxer]: initPTS & initDTS reset"),this._initPTS=this._initDTS=t},e.resetNextTimestamp=function(){h.b.log("[mp4-remuxer]: reset next timestamp"),this.isVideoContiguous=!1,this.isAudioContiguous=!1},e.resetInitSegment=function(){h.b.log("[mp4-remuxer]: ISGenerated flag reset"),this.ISGenerated=!1},e.getVideoStartPts=function(t){var e=!1,r=t.reduce((function(t,r){var i=r.pts-t;return i<-4294967296?(e=!0,m(t,r.pts)):i>0?t:r.pts}),t[0].pts);return e&&h.b.debug("PTS rollover detected"),r},e.remux=function(t,e,r,i,a,n,s){var o,l,u,d,c,f,g=a,v=a,p=t.pid>-1,y=e.pid>-1,T=e.samples.length,E=t.samples.length>0,b=T>1;if((!p||E)&&(!y||b)||this.ISGenerated||s){this.ISGenerated||(u=this.generateIS(t,e,a));var S=this.isVideoContiguous,L=-1;if(b&&(L=function(t){for(var e=0;e<t.length;e++)if(t[e].key)return e;return-1}(e.samples),!S&&this.config.forceKeyFrameOnDiscontinuity))if(f=!0,L>0){h.b.warn("[mp4-remuxer]: Dropped "+L+" out of "+T+" video samples due to a missing keyframe");var A=this.getVideoStartPts(e.samples);e.samples=e.samples.slice(L),e.dropped+=L,v+=(e.samples[0].pts-A)/(e.timescale||9e4)}else-1===L&&(h.b.warn("[mp4-remuxer]: No keyframe found out of "+T+" video samples"),f=!1);if(this.ISGenerated){if(E&&b){var R=this.getVideoStartPts(e.samples),D=(m(t.samples[0].pts,R)-R)/e.inputTimeScale;g+=Math.max(0,D),v+=Math.max(0,-D)}if(E){if(t.samplerate||(h.b.warn("[mp4-remuxer]: regenerate InitSegment as audio detected"),u=this.generateIS(t,e,a)),l=this.remuxAudio(t,g,this.isAudioContiguous,n,b?v:void 0),b){var k=l?l.endPTS-l.startPTS:0;e.inputTimeScale||(h.b.warn("[mp4-remuxer]: regenerate InitSegment as video detected"),u=this.generateIS(t,e,a)),o=this.remuxVideo(e,v,S,k)}}else b&&(o=this.remuxVideo(e,v,S,0));o&&(o.firstKeyFrame=L,o.independent=-1!==L)}}return this.ISGenerated&&(r.samples.length&&(c=this.remuxID3(r,a)),i.samples.length&&(d=this.remuxText(i,a))),{audio:l,video:o,initSegment:u,independent:f,text:d,id3:c}},e.generateIS=function(t,e,r){var a,n,s,l=t.samples,u=e.samples,h=this.typeSupported,d={},c=!Object(i.a)(this._initPTS),f="audio/mp4";if(c&&(a=n=1/0),t.config&&l.length&&(t.timescale=t.samplerate,t.isAAC||(h.mpeg?(f="audio/mpeg",t.codec=""):h.mp3&&(t.codec="mp3")),d.audio={id:"audio",container:f,codec:t.codec,initSegment:!t.isAAC&&h.mpeg?new Uint8Array(0):o.initSegment([t]),metadata:{channelCount:t.channelCount}},c&&(s=t.inputTimeScale,a=n=l[0].pts-Math.round(s*r))),e.sps&&e.pps&&u.length&&(e.timescale=e.inputTimeScale,d.video={id:"main",container:"video/mp4",codec:e.codec,initSegment:o.initSegment([e]),metadata:{width:e.width,height:e.height}},c)){s=e.inputTimeScale;var g=this.getVideoStartPts(u),v=Math.round(s*r);n=Math.min(n,m(u[0].dts,g)-v),a=Math.min(a,g-v)}if(Object.keys(d).length)return this.ISGenerated=!0,c&&(this._initPTS=a,this._initDTS=n),{tracks:d,initPTS:a,timescale:s}},e.remuxVideo=function(t,e,r,i){var a,n,s,g=t.inputTimeScale,p=t.samples,T=[],E=p.length,b=this._initPTS,S=this.nextAvcDts,L=8,A=Number.POSITIVE_INFINITY,R=Number.NEGATIVE_INFINITY,D=0,k=!1;r&&null!==S||(S=e*g-(p[0].pts-m(p[0].dts,p[0].pts)));for(var _=0;_<E;_++){var I=p[_];if(I.pts=m(I.pts-b,S),I.dts=m(I.dts-b,S),I.dts>I.pts){D=Math.max(Math.min(D,I.pts-I.dts),-18e3)}I.dts<p[_>0?_-1:_].dts&&(k=!0)}k&&p.sort((function(t,e){var r=t.dts-e.dts,i=t.pts-e.pts;return r||i})),n=p[0].dts,s=p[p.length-1].dts;var C=Math.round((s-n)/(E-1));if(D<0){if(D<-2*C){h.b.warn("PTS < DTS detected in video samples, offsetting DTS from PTS by "+Object(d.b)(-C,!0)+" ms");for(var w=D,O=0;O<E;O++)p[O].dts=w=Math.max(w,p[O].pts-C),p[O].pts=Math.max(w,p[O].pts)}else{h.b.warn("PTS < DTS detected in video samples, shifting DTS by "+Object(d.b)(D,!0)+" ms to overcome this issue");for(var x=0;x<E;x++)p[x].dts=p[x].dts+D}n=p[0].dts}if(r){var P=n-S,M=P>C;if(M||P<-1){M?h.b.warn("AVC: "+Object(d.b)(P,!0)+" ms ("+P+"dts) hole between fragments detected, filling it"):h.b.warn("AVC: "+Object(d.b)(-P,!0)+" ms ("+P+"dts) overlapping between fragments detected"),n=S;var F=p[0].pts-P;p[0].dts=n,p[0].pts=F,h.b.log("Video: First PTS/DTS adjusted: "+Object(d.b)(F,!0)+"/"+Object(d.b)(n,!0)+", delta: "+Object(d.b)(P,!0)+" ms")}}v&&(n=Math.max(0,n));for(var N=0,U=0,B=0;B<E;B++){for(var G=p[B],K=G.units,j=K.length,H=0,V=0;V<j;V++)H+=K[V].data.length;U+=H,N+=j,G.length=H,G.dts=Math.max(G.dts,n),G.pts=Math.max(G.pts,G.dts,0),A=Math.min(G.pts,A),R=Math.max(G.pts,R)}s=p[E-1].dts;var W,Y=U+4*N+8;try{W=new Uint8Array(Y)}catch(t){return void this.observer.emit(l.a.ERROR,l.a.ERROR,{type:u.b.MUX_ERROR,details:u.a.REMUX_ALLOC_ERROR,fatal:!1,bytes:Y,reason:"fail allocating video mdat "+Y})}var q=new DataView(W.buffer);q.setUint32(0,Y),W.set(o.types.mdat,4);for(var X=0;X<E;X++){for(var z=p[X],Q=z.units,$=0,J=0,Z=Q.length;J<Z;J++){var tt=Q[J],et=tt.data,rt=tt.data.byteLength;q.setUint32(L,rt),L+=4,W.set(et,L),L+=rt,$+=4+rt}if(X<E-1)a=p[X+1].dts-z.dts;else{var it=this.config,at=z.dts-p[X>0?X-1:X].dts;if(it.stretchShortVideoTrack&&null!==this.nextAudioPts){var nt=Math.floor(it.maxBufferHole*g),st=(i?A+i*g:this.nextAudioPts)-z.pts;st>nt?((a=st-at)<0&&(a=at),h.b.log("[mp4-remuxer]: It is approximately "+st/90+" ms to the next segment; using duration "+a/90+" ms for the last video frame.")):a=at}else a=at}var ot=Math.round(z.pts-z.dts);T.push(new y(z.key,a,$,ot))}if(T.length&&f&&f<70){var lt=T[0].flags;lt.dependsOn=2,lt.isNonSync=0}this.nextAvcDts=S=s+a,this.isVideoContiguous=!0;var ut={data1:o.moof(t.sequenceNumber++,n,c({},t,{samples:T})),data2:W,startPTS:A/g,endPTS:(R+a)/g,startDTS:n/g,endDTS:S/g,type:"video",hasAudio:!1,hasVideo:!0,nb:T.length,dropped:t.dropped};return t.samples=[],t.dropped=0,ut},e.remuxAudio=function(t,e,r,i,n){var s,d=t.inputTimeScale,f=d/(t.samplerate?t.samplerate:d),g=t.isAAC?1024:1152,v=g*f,p=this._initPTS,T=!t.isAAC&&this.typeSupported.mpeg,E=[],b=t.samples,S=T?0:8,L=this.nextAudioPts||-1,A=e*d;if(this.isAudioContiguous=r=r||b.length&&L>0&&(i&&Math.abs(A-L)<9e3||Math.abs(m(b[0].pts-p,A)-L)<20*v),b.forEach((function(t){t.pts=t.dts=m(t.pts-p,A)})),!r||L<0){if(!(b=b.filter((function(t){return t.pts>=0}))).length)return;L=0===n?0:i?Math.max(0,A):b[0].pts}if(t.isAAC)for(var R=this.config.maxAudioFramesDrift,D=0,k=L;D<b.length;){var _=b[D],I=_.pts,C=I-k,w=Math.abs(1e3*C/d);if(C<=-R*v&&void 0!==n)r||D>0?(h.b.warn("[mp4-remuxer]: Dropping 1 audio frame @ "+(k/d).toFixed(3)+"s due to "+Math.round(w)+" ms overlap."),b.splice(D,1)):(h.b.warn("Audio frame @ "+(I/d).toFixed(3)+"s overlaps nextAudioPts by "+Math.round(1e3*C/d)+" ms."),k=I+v,D++);else if(C>=R*v&&w<1e4&&void 0!==n){var O=Math.floor(C/v);k=I-O*v,h.b.warn("[mp4-remuxer]: Injecting "+O+" audio frame @ "+(k/d).toFixed(3)+"s due to "+Math.round(1e3*C/d)+" ms gap.");for(var x=0;x<O;x++){var P=Math.max(k,0);(s=a.getSilentFrame(t.manifestCodec||t.codec,t.channelCount))||(h.b.log("[mp4-remuxer]: Unable to get silent frame for given audio codec; duplicating last frame instead."),s=_.unit.subarray()),b.splice(D,0,{unit:s,pts:P,dts:P}),k+=v,D++}_.pts=_.dts=k,k+=v,D++}else _.pts=_.dts=k,k+=v,D++}for(var M,F=null,N=null,U=0,B=b.length;B--;)U+=b[B].unit.byteLength;for(var G=0,K=b.length;G<K;G++){var j=b[G],H=j.unit,V=j.pts;if(null!==N){E[G-1].duration=Math.round((V-N)/f)}else{var W=Math.round(1e3*(V-L)/d),Y=0;if(r&&t.isAAC){if(W>0&&W<1e4)Y=Math.round((V-L)/v),h.b.log("[mp4-remuxer]: "+W+" ms hole between AAC samples detected,filling it"),Y>0&&((s=a.getSilentFrame(t.manifestCodec||t.codec,t.channelCount))||(s=H.subarray()),U+=Y*s.length);else if(W<-12){h.b.log("[mp4-remuxer]: drop overlapping AAC sample, expected/parsed/delta:"+(L/d).toFixed(3)+"s/"+(V/d).toFixed(3)+"s/"+-W+"ms"),U-=H.byteLength;continue}V=L}if(F=V,!(U>0))return;U+=S;try{M=new Uint8Array(U)}catch(t){return void this.observer.emit(l.a.ERROR,l.a.ERROR,{type:u.b.MUX_ERROR,details:u.a.REMUX_ALLOC_ERROR,fatal:!1,bytes:U,reason:"fail allocating audio mdat "+U})}T||(new DataView(M.buffer).setUint32(0,U),M.set(o.types.mdat,4));for(var q=0;q<Y;q++)(s=a.getSilentFrame(t.manifestCodec||t.codec,t.channelCount))||(h.b.log("[mp4-remuxer]: Unable to get silent frame for given audio codec; duplicating the current frame instead"),s=H.subarray()),M.set(s,S),S+=s.byteLength,E.push(new y(!0,1024,s.byteLength,0))}M.set(H,S);var X=H.byteLength;S+=X,E.push(new y(!0,g,X,0)),N=V}var z=E.length;if(z){var Q=E[E.length-1];this.nextAudioPts=L=N+f*Q.duration;var $=T?new Uint8Array(0):o.moof(t.sequenceNumber++,F/f,c({},t,{samples:E}));t.samples=[];var J=F/d,Z=L/d,tt={data1:$,data2:M,startPTS:J,endPTS:Z,startDTS:J,endDTS:Z,type:"audio",hasAudio:!0,hasVideo:!1,nb:z};return this.isAudioContiguous=!0,tt}},e.remuxEmptyAudio=function(t,e,r,i){var n=t.inputTimeScale,s=n/(t.samplerate?t.samplerate:n),o=this.nextAudioPts,l=(null!==o?o:i.startDTS*n)+this._initDTS,u=i.endDTS*n+this._initDTS,d=1024*s,c=Math.ceil((u-l)/d),f=a.getSilentFrame(t.manifestCodec||t.codec,t.channelCount);if(h.b.warn("[mp4-remuxer]: remux empty Audio"),f){for(var g=[],v=0;v<c;v++){var p=l+v*d;g.push({unit:f,pts:p,dts:p})}return t.samples=g,this.remuxAudio(t,e,r,!1)}h.b.trace("[mp4-remuxer]: Unable to remuxEmptyAudio since we were unable to get a silent frame for given audio codec")},e.remuxID3=function(t,e){var r=t.samples.length;if(r){for(var i=t.inputTimeScale,a=this._initPTS,n=this._initDTS,s=0;s<r;s++){var o=t.samples[s];o.pts=m(o.pts-a,e*i)/i,o.dts=m(o.dts-n,e*i)/i}var l=t.samples;return t.samples=[],{samples:l}}},e.remuxText=function(t,e){var r=t.samples.length;if(r){for(var i=t.inputTimeScale,a=this._initPTS,n=0;n<r;n++){var s=t.samples[n];s.pts=m(s.pts-a,e*i)/i}t.samples.sort((function(t,e){return t.pts-e.pts}));var o=t.samples;return t.samples=[],{samples:o}}},t}();function m(t,e){var r;if(null===e)return t;for(r=e<t?-8589934592:8589934592;Math.abs(t-e)>4294967296;)t+=r;return t}var y=function(t,e,r,i){this.size=void 0,this.duration=void 0,this.cts=void 0,this.flags=void 0,this.duration=e,this.size=r,this.cts=i,this.flags=new T(t)},T=function(t){this.isLeading=0,this.isDependedOn=0,this.hasRedundancy=0,this.degradPrio=0,this.dependsOn=1,this.isNonSync=1,this.dependsOn=t?2:1,this.isNonSync=t?0:1}},7: function(t,e,r){"use strict";r.d(e,"c",(function(){return a})),r.d(e,"b",(function(){return n})),r.d(e,"a",(function(){return s}));function i(t,e,r,i){void 0===r&&(r=1),void 0===i&&(i=!1);var a=t*e*r;return i?Math.round(a):a}function a(t,e,r,a){return void 0===r&&(r=1),void 0===a&&(a=!1),i(t,e,1/r,a)}function n(t,e){return void 0===e&&(e=!1),i(t,1e3,1/9e4,e)}function s(t,e){return void 0===e&&(e=1),i(t,9e4,1/e)}},2: function(t,e,r){"use strict";var i,a;r.d(e,"b",(function(){return i})),r.d(e,"a",(function(){return a})),function(t){t.NETWORK_ERROR="networkError",t.MEDIA_ERROR="mediaError",t.KEY_SYSTEM_ERROR="keySystemError",t.MUX_ERROR="muxError",t.OTHER_ERROR="otherError"}(i||(i={})),function(t){t.KEY_SYSTEM_NO_KEYS="keySystemNoKeys",t.KEY_SYSTEM_NO_ACCESS="keySystemNoAccess",t.KEY_SYSTEM_NO_SESSION="keySystemNoSession",t.KEY_SYSTEM_LICENSE_REQUEST_FAILED="keySystemLicenseRequestFailed",t.KEY_SYSTEM_NO_INIT_DATA="keySystemNoInitData",t.MANIFEST_LOAD_ERROR="manifestLoadError",t.MANIFEST_LOAD_TIMEOUT="manifestLoadTimeOut",t.MANIFEST_PARSING_ERROR="manifestParsingError",t.MANIFEST_INCOMPATIBLE_CODECS_ERROR="manifestIncompatibleCodecsError",t.LEVEL_EMPTY_ERROR="levelEmptyError",t.LEVEL_LOAD_ERROR="levelLoadError",t.LEVEL_LOAD_TIMEOUT="levelLoadTimeOut",t.LEVEL_SWITCH_ERROR="levelSwitchError",t.AUDIO_TRACK_LOAD_ERROR="audioTrackLoadError",t.AUDIO_TRACK_LOAD_TIMEOUT="audioTrackLoadTimeOut",t.SUBTITLE_LOAD_ERROR="subtitleTrackLoadError",t.SUBTITLE_TRACK_LOAD_TIMEOUT="subtitleTrackLoadTimeOut",t.FRAG_LOAD_ERROR="fragLoadError",t.FRAG_LOAD_TIMEOUT="fragLoadTimeOut",t.FRAG_DECRYPT_ERROR="fragDecryptError",t.FRAG_PARSING_ERROR="fragParsingError",t.REMUX_ALLOC_ERROR="remuxAllocError",t.KEY_LOAD_ERROR="keyLoadError",t.KEY_LOAD_TIMEOUT="keyLoadTimeOut",t.BUFFER_ADD_CODEC_ERROR="bufferAddCodecError",t.BUFFER_INCOMPATIBLE_CODECS_ERROR="bufferIncompatibleCodecsError",t.BUFFER_APPEND_ERROR="bufferAppendError",t.BUFFER_APPENDING_ERROR="bufferAppendingError",t.BUFFER_STALLED_ERROR="bufferStalledError",t.BUFFER_FULL_ERROR="bufferFullError",t.BUFFER_SEEK_OVER_HOLE="bufferSeekOverHole",t.BUFFER_NUDGE_ON_STALL="bufferNudgeOnStall",t.INTERNAL_EXCEPTION="internalException",t.INTERNAL_ABORTED="aborted",t.UNKNOWN="unknown"}(a||(a={}))},8: function(t,e,r){"use strict";function i(t,e,r){return Uint8Array.prototype.slice?t.slice(e,r):new Uint8Array(Array.prototype.slice.call(t,e,r))}r.d(e,"a",(function(){return i}))},5: function(t,e,r){"use strict";r.d(e,"b",(function(){return h})),r.d(e,"g",(function(){return d})),r.d(e,"f",(function(){return c})),r.d(e,"d",(function(){return f})),r.d(e,"c",(function(){return g})),r.d(e,"e",(function(){return p})),r.d(e,"h",(function(){return m})),r.d(e,"a",(function(){return y}));var i=r(8),a=r(4),n=Math.pow(2,32)-1,s=[].push;function o(t){return String.fromCharCode.apply(null,t)}function l(t,e){"data"in t&&(e+=t.start,t=t.data);var r=t[e]<<24|t[e+1]<<16|t[e+2]<<8|t[e+3];return r<0?4294967296+r:r}function u(t,e,r){"data"in t&&(e+=t.start,t=t.data),t[e]=r>>24,t[e+1]=r>>16&255,t[e+2]=r>>8&255,t[e+3]=255&r}function h(t,e){var r,i,a,n=[];if(!e.length)return n;"data"in t?(r=t.data,i=t.start,a=t.end):(i=0,a=(r=t).byteLength);for(var u=i;u<a;){var d=l(r,u),c=d>1?u+d:a;if(o(r.subarray(u+4,u+8))===e[0])if(1===e.length)n.push({data:r,start:u+8,end:c});else{var f=h({data:r,start:u+8,end:c},e.slice(1));f.length&&s.apply(n,f)}u=c}return n}function d(t){var e=h(t,["moov"])[0],r=e?e.end:null,i=h(t,["sidx"]);if(!i||!i[0])return null;var a=[],n=i[0],s=n.data[0],o=0===s?8:16,u=l(n,o);o+=4;o+=0===s?8:16,o+=2;var d=n.end+0,c=function(t,e){"data"in t&&(e+=t.start,t=t.data);var r=t[e]<<8|t[e+1];return r<0?65536+r:r}(n,o);o+=2;for(var f=0;f<c;f++){var g=o,v=l(n,g);g+=4;var p=2147483647&v;if(1===(2147483648&v)>>>31)return console.warn("SIDX has hierarchical references (not supported)"),null;var m=l(n,g);g+=4,a.push({referenceSize:p,subsegmentDuration:m,info:{duration:m/u,start:d,end:d+p-1}}),d+=p,o=g+=4}return{earliestPresentationTime:0,timescale:u,version:s,referencesCount:c,references:a,moovEndOffset:r}}function c(t){for(var e=[],r=h(t,["moov","trak"]),i=0;i<r.length;i++){var n=r[i],s=h(n,["tkhd"])[0];if(s){var u=s.data[s.start],d=0===u?12:20,c=l(s,d),f=h(n,["mdia","mdhd"])[0];if(f){var g=l(f,d=0===(u=f.data[f.start])?12:20),v=h(n,["mdia","hdlr"])[0];if(v){var p=o(v.data.subarray(v.start+8,v.start+12)),m={soun:a.a.AUDIO,vide:a.a.VIDEO}[p];if(m){var y=h(n,["mdia","minf","stbl","stsd"])[0],T=void 0;y&&(T=o(y.data.subarray(y.start+12,y.start+16))),e[c]={timescale:g,type:m},e[m]={timescale:g,id:c,codec:T}}}}}}return h(t,["moov","mvex","trex"]).forEach((function(t){var r=l(t,4),i=e[r];i&&(i.default={duration:l(t,12),flags:l(t,20)})})),e}function f(t,e){return h(e,["moof","traf"]).reduce((function(e,r){var i=h(r,["tfdt"])[0],a=i.data[i.start],n=h(r,["tfhd"]).reduce((function(e,r){var n=l(r,4),s=t[n];if(s){var o=l(i,4);1===a&&(o*=Math.pow(2,32),o+=l(i,8));var u=o/(s.timescale||9e4);if(isFinite(u)&&(null===e||u<e))return u}return e}),null);return null!==n&&isFinite(n)&&(null===e||n<e)?n:e}),null)||0}function g(t,e){for(var r=0,i=0,n=0,s=h(t,["moof","traf"]),o=0;o<s.length;o++){var u=s[o],c=h(u,["tfhd"])[0],f=e[l(c,4)];if(f){var g=f.default,p=l(c,0)|(null==g?void 0:g.flags),m=null==g?void 0:g.duration;8&p&&(m=l(c,2&p?12:8));for(var y=f.timescale||9e4,T=h(u,["trun"]),E=0;E<T.length;E++){if(m)r=m*l(T[E],4);else r=v(T[E]);f.type===a.a.VIDEO?i+=r/y:f.type===a.a.AUDIO&&(n+=r/y)}}}if(0===i&&0===n){var b=d(t);if(null!=b&&b.references)return b.references.reduce((function(t,e){return t+e.info.duration||0}),0)}return i||n}function v(t){var e=l(t,0),r=8;1&e&&(r+=4),4&e&&(r+=4);for(var i=0,a=l(t,4),n=0;n<a;n++){if(256&e)i+=l(t,r),r+=4;512&e&&(r+=4),1024&e&&(r+=4),2048&e&&(r+=4)}return i}function p(t,e,r){h(e,["moof","traf"]).forEach((function(e){h(e,["tfhd"]).forEach((function(i){var a=l(i,4),s=t[a];if(s){var o=s.timescale||9e4;h(e,["tfdt"]).forEach((function(t){var e=t.data[t.start],i=l(t,4);if(0===e)u(t,4,i-r*o);else{i*=Math.pow(2,32),i+=l(t,8),i-=r*o,i=Math.max(i,0);var a=Math.floor(i/(n+1)),s=Math.floor(i%(n+1));u(t,4,a),u(t,8,s)}}))}}))}))}function m(t){var e={valid:null,remainder:null},r=h(t,["moof"]);if(!r)return e;if(r.length<2)return e.remainder=t,e;var a=r[r.length-1];return e.valid=Object(i.a)(t,0,a.start-8),e.remainder=Object(i.a)(t,a.start-8),e}function y(t,e){var r=new Uint8Array(t.length+e.length);return r.set(t),r.set(e,t.length),r}},6: function(t,e,r){"use strict";r.d(e,"b",(function(){return s})),r.d(e,"a",(function(){return l})),r.d(e,"d",(function(){return u})),r.d(e,"e",(function(){return h})),r.d(e,"c",(function(){return c})),r.d(e,"f",(function(){return y}));var i,a=function(t,e){return e+10<=t.length&&73===t[e]&&68===t[e+1]&&51===t[e+2]&&t[e+3]<255&&t[e+4]<255&&t[e+6]<128&&t[e+7]<128&&t[e+8]<128&&t[e+9]<128},n=function(t,e){return e+10<=t.length&&51===t[e]&&68===t[e+1]&&73===t[e+2]&&t[e+3]<255&&t[e+4]<255&&t[e+6]<128&&t[e+7]<128&&t[e+8]<128&&t[e+9]<128},s=function(t,e){for(var r=e,i=0;a(t,e);){i+=10,i+=o(t,e+6),n(t,e+10)&&(i+=10),e+=i}if(i>0)return t.subarray(r,r+i)},o=function(t,e){var r=0;return r=(127&t[e])<<21,r|=(127&t[e+1])<<14,r|=(127&t[e+2])<<7,r|=127&t[e+3]},l=function(t,e){return a(t,e)&&o(t,e+6)+10<=t.length-e},u=function(t){for(var e=c(t),r=0;r<e.length;r++){var i=e[r];if(h(i))return m(i)}},h=function(t){return t&&"PRIV"===t.key&&"com.apple.streaming.transportStreamTimestamp"===t.info},d=function(t){var e=String.fromCharCode(t[0],t[1],t[2],t[3]),r=o(t,4);return{type:e,size:r,data:t.subarray(10,10+r)}},c=function(t){for(var e=0,r=[];a(t,e);){for(var i=o(t,e+6),s=(e+=10)+i;e+8<s;){var l=d(t.subarray(e)),u=f(l);u&&r.push(u),e+=l.size+10}n(t,e)&&(e+=10)}return r},f=function(t){return"PRIV"===t.type?g(t):"W"===t.type[0]?p(t):v(t)},g=function(t){if(!(t.size<2)){var e=y(t.data,!0),r=new Uint8Array(t.data.subarray(e.length+1));return{key:t.type,info:e,data:r.buffer}}},v=function(t){if(!(t.size<2)){if("TXXX"===t.type){var e=1,r=y(t.data.subarray(e),!0);e+=r.length+1;var i=y(t.data.subarray(e));return{key:t.type,info:r,data:i}}var a=y(t.data.subarray(1));return{key:t.type,data:a}}},p=function(t){if("WXXX"===t.type){if(t.size<2)return;var e=1,r=y(t.data.subarray(e),!0);e+=r.length+1;var i=y(t.data.subarray(e));return{key:t.type,info:r,data:i}}var a=y(t.data);return{key:t.type,data:a}},m=function(t){if(8===t.data.byteLength){var e=new Uint8Array(t.data),r=1&e[3],i=(e[4]<<23)+(e[5]<<15)+(e[6]<<7)+e[7];return i/=45,r&&(i+=47721858.84),Math.round(i)}},y=function(t,e){void 0===e&&(e=!1);var r=T();if(r){var i=r.decode(t);if(e){var a=i.indexOf("\0");return-1!==a?i.substring(0,a):i}return i.replace(/\0/g,"")}for(var n,s,o,l=t.length,u="",h=0;h<l;){if(0===(n=t[h++])&&e)return u;if(0!==n&&3!==n)switch(n>>4){case 0:case 1:case 2:case 3:case 4:case 5:case 6:case 7:u+=String.fromCharCode(n);break;case 12:case 13:s=t[h++],u+=String.fromCharCode((31&n)<<6|63&s);break;case 14:s=t[h++],o=t[h++],u+=String.fromCharCode((15&n)<<12|(63&s)<<6|(63&o)<<0)}}return u};function T(){return i||void 0===self.TextDecoder||(i=new self.TextDecoder("utf-8")),i}},14: function(t,e,r){"use strict";r.d(e,"a",(function(){return u}));var i=function(){function t(t,e){this.subtle=void 0,this.aesIV=void 0,this.subtle=t,this.aesIV=e}return t.prototype.decrypt=function(t,e){return this.subtle.decrypt({name:"AES-CBC",iv:this.aesIV},e,t)},t}(),a=function(){function t(t,e){this.subtle=void 0,this.key=void 0,this.subtle=t,this.key=e}return t.prototype.expandKey=function(){return this.subtle.importKey("raw",this.key,{name:"AES-CBC"},!1,["encrypt","decrypt"])},t}(),n=r(8);var s=function(){function t(){this.rcon=[0,1,2,4,8,16,32,64,128,27,54],this.subMix=[new Uint32Array(256),new Uint32Array(256),new Uint32Array(256),new Uint32Array(256)],this.invSubMix=[new Uint32Array(256),new Uint32Array(256),new Uint32Array(256),new Uint32Array(256)],this.sBox=new Uint32Array(256),this.invSBox=new Uint32Array(256),this.key=new Uint32Array(0),this.ksRows=0,this.keySize=0,this.keySchedule=void 0,this.invKeySchedule=void 0,this.initTable()}var e=t.prototype;return e.uint8ArrayToUint32Array_=function(t){for(var e=new DataView(t),r=new Uint32Array(4),i=0;i<4;i++)r[i]=e.getUint32(4*i);return r},e.initTable=function(){var t=this.sBox,e=this.invSBox,r=this.subMix,i=r[0],a=r[1],n=r[2],s=r[3],o=this.invSubMix,l=o[0],u=o[1],h=o[2],d=o[3],c=new Uint32Array(256),f=0,g=0,v=0;for(v=0;v<256;v++)c[v]=v<128?v<<1:v<<1^283;for(v=0;v<256;v++){var p=g^g<<1^g<<2^g<<3^g<<4;p=p>>>8^255&p^99,t[f]=p,e[p]=f;var m=c[f],y=c[m],T=c[y],E=257*c[p]^16843008*p;i[f]=E<<24|E>>>8,a[f]=E<<16|E>>>16,n[f]=E<<8|E>>>24,s[f]=E,E=16843009*T^65537*y^257*m^16843008*f,l[p]=E<<24|E>>>8,u[p]=E<<16|E>>>16,h[p]=E<<8|E>>>24,d[p]=E,f?(f=m^c[c[c[T^m]]],g^=c[c[g]]):f=g=1}},e.expandKey=function(t){for(var e=this.uint8ArrayToUint32Array_(t),r=!0,i=0;i<e.length&&r;)r=e[i]===this.key[i],i++;if(!r){this.key=e;var a=this.keySize=e.length;if(4!==a&&6!==a&&8!==a)throw new Error("Invalid aes key size="+a);var n,s,o,l,u=this.ksRows=4*(a+6+1),h=this.keySchedule=new Uint32Array(u),d=this.invKeySchedule=new Uint32Array(u),c=this.sBox,f=this.rcon,g=this.invSubMix,v=g[0],p=g[1],m=g[2],y=g[3];for(n=0;n<u;n++)n<a?o=h[n]=e[n]:(l=o,n%a==0?(l=c[(l=l<<8|l>>>24)>>>24]<<24|c[l>>>16&255]<<16|c[l>>>8&255]<<8|c[255&l],l^=f[n/a|0]<<24):a>6&&n%a==4&&(l=c[l>>>24]<<24|c[l>>>16&255]<<16|c[l>>>8&255]<<8|c[255&l]),h[n]=o=(h[n-a]^l)>>>0);for(s=0;s<u;s++)n=u-s,l=3&s?h[n]:h[n-4],d[s]=s<4||n<=4?l:v[c[l>>>24]]^p[c[l>>>16&255]]^m[c[l>>>8&255]]^y[c[255&l]],d[s]=d[s]>>>0}},e.networkToHostOrderSwap=function(t){return t<<24|(65280&t)<<8|(16711680&t)>>8|t>>>24},e.decrypt=function(t,e,r){for(var i,a,n,s,o,l,u,h,d,c,f,g,v,p,m=this.keySize+6,y=this.invKeySchedule,T=this.invSBox,E=this.invSubMix,b=E[0],S=E[1],L=E[2],A=E[3],R=this.uint8ArrayToUint32Array_(r),D=R[0],k=R[1],_=R[2],I=R[3],C=new Int32Array(t),w=new Int32Array(C.length),O=this.networkToHostOrderSwap;e<C.length;){for(d=O(C[e]),c=O(C[e+1]),f=O(C[e+2]),g=O(C[e+3]),o=d^y[0],l=g^y[1],u=f^y[2],h=c^y[3],v=4,p=1;p<m;p++)i=b[o>>>24]^S[l>>16&255]^L[u>>8&255]^A[255&h]^y[v],a=b[l>>>24]^S[u>>16&255]^L[h>>8&255]^A[255&o]^y[v+1],n=b[u>>>24]^S[h>>16&255]^L[o>>8&255]^A[255&l]^y[v+2],s=b[h>>>24]^S[o>>16&255]^L[l>>8&255]^A[255&u]^y[v+3],o=i,l=a,u=n,h=s,v+=4;i=T[o>>>24]<<24^T[l>>16&255]<<16^T[u>>8&255]<<8^T[255&h]^y[v],a=T[l>>>24]<<24^T[u>>16&255]<<16^T[h>>8&255]<<8^T[255&o]^y[v+1],n=T[u>>>24]<<24^T[h>>16&255]<<16^T[o>>8&255]<<8^T[255&l]^y[v+2],s=T[h>>>24]<<24^T[o>>16&255]<<16^T[l>>8&255]<<8^T[255&u]^y[v+3],w[e]=O(i^D),w[e+1]=O(s^k),w[e+2]=O(n^_),w[e+3]=O(a^I),D=d,k=c,_=f,I=g,e+=4}return w.buffer},t}(),o=r(1),l=r(5),u=function(){function t(t,e,r){var i=(void 0===r?{}:r).removePKCS7Padding,a=void 0===i||i;if(this.logEnabled=!0,this.observer=void 0,this.config=void 0,this.removePKCS7Padding=void 0,this.subtle=null,this.softwareDecrypter=null,this.key=null,this.fastAesKey=null,this.remainderData=null,this.currentIV=null,this.currentResult=null,this.observer=t,this.config=e,this.removePKCS7Padding=a,a)try{var n=self.crypto;n&&(this.subtle=n.subtle||n.webkitSubtle)}catch(t){}null===this.subtle&&(this.config.enableSoftwareAES=!0)}var e=t.prototype;return e.destroy=function(){this.observer=null},e.isSync=function(){return this.config.enableSoftwareAES},e.flush=function(){var t=this.currentResult;if(t){var e,r,i,a=new Uint8Array(t);return this.reset(),this.removePKCS7Padding?(r=(e=a).byteLength,(i=r&&new DataView(e.buffer).getUint8(r-1))?Object(n.a)(e,0,r-i):e):a}this.reset()},e.reset=function(){this.currentResult=null,this.currentIV=null,this.remainderData=null,this.softwareDecrypter&&(this.softwareDecrypter=null)},e.decrypt=function(t,e,r,i){if(this.config.enableSoftwareAES){this.softwareDecrypt(new Uint8Array(t),e,r);var a=this.flush();a&&i(a.buffer)}else this.webCryptoDecrypt(new Uint8Array(t),e,r).then(i)},e.softwareDecrypt=function(t,e,r){var i=this.currentIV,a=this.currentResult,o=this.remainderData;this.logOnce("JS AES decrypt"),o&&(t=Object(l.a)(o,t),this.remainderData=null);var u=this.getValidChunk(t);if(!u.length)return null;i&&(r=i);var h=this.softwareDecrypter;h||(h=this.softwareDecrypter=new s),h.expandKey(e);var d=a;return this.currentResult=h.decrypt(u.buffer,0,r),this.currentIV=Object(n.a)(u,-16).buffer,d||null},e.webCryptoDecrypt=function(t,e,r){var n=this,s=this.subtle;return this.key===e&&this.fastAesKey||(this.key=e,this.fastAesKey=new a(s,e)),this.fastAesKey.expandKey().then((function(e){return s?new i(s,r).decrypt(t.buffer,e):Promise.reject(new Error("web crypto not initialized"))})).catch((function(i){return n.onWebCryptoError(i,t,e,r)}))},e.onWebCryptoError=function(t,e,r,i){return o.b.warn("[decrypter.ts]: WebCrypto Error, disable WebCrypto API:",t),this.config.enableSoftwareAES=!0,this.logEnabled=!0,this.softwareDecrypt(e,r,i)},e.getValidChunk=function(t){var e=t,r=t.length-t.length%16;return r!==t.length&&(e=Object(n.a)(t,0,r),this.remainderData=Object(n.a)(t,r)),e},e.logOnce=function(t){this.logEnabled&&(o.b.log("[decrypter.ts]: "+t),this.logEnabled=!1)},t}()}}))(self);


new((function i(t) {
    var e = {};
    function r(i) {
        if (e[i]) return e[i].exports;
        var a = e[i] = {
            i: i,
            l: !1,
            exports: {}
        };
        return t[i].call(a.exports, a, a.exports, r),
        a.l = !0,
        a.exports
    }
    r.m = t,
    r.c = e,
    r.i = function(t) {
        return t
    },
    r.d = function(t, e, i) {
        r.o(t, e) || Object.defineProperty(t, e, {
            configurable: !1,
            enumerable: !0,
            get: i
        })
    },
    r.r = function(t) {
        Object.defineProperty(t, "__esModule", {
            value: !0
        })
    },
    r.n = function(t) {
        var e = t && t.__esModule ?
        function() {
            return t.
        default
        }:
        function() {
            return t
        };
        return r.d(e, "a", e),
        e
    },
    r.o = function(t, e) {
        return Object.prototype.hasOwnProperty.call(t, e)
    },
    r.p = "/",
    r.oe = function(t) {
        throw console.error(t),
        t
    };
    var i = r(r.s = 18);
    return i.
default || i
})({
    18 : function(t, e, r) {
        "use strict";
        r.r(e),
        r.d(e, "default", (function() {
            return o
        }));
        var i = r(9),
        a = r(0),
        n = r(1),
        s = r(13);
        function o(t) {
            var e = new s.EventEmitter,
            r = function(e, r) {
                t.postMessage({
                    event: e,
                    data: r
                })
            };
            e.on(a.a.FRAG_DECRYPTED, r),
            e.on(a.a.ERROR, r),
            t.addEventListener("message", (function(a) {
                var s = a.data;
                switch (s.cmd) {
                case "init":
                    var o = JSON.parse(s.config);
                    t.transmuxer = new i.c(e, s.typeSupported, o, s.vendor),
                    Object(n.a)(o.debug),
                    r("init", null);
                    break;
                case "configure":
                    t.transmuxer.configure(s.config);
                    break;
                case "demux":
                    var u = t.transmuxer.push(s.data, s.decryptdata, s.chunkMeta, s.state);
                    Object(i.d)(u) ? u.then((function(e) {
                        l(t, e)
                    })) : l(t, u);
                    break;
                case "flush":
                    var d = s.chunkMeta,
                    c = t.transmuxer.flush(d);
                    Object(i.d)(c) ? c.then((function(e) {
                        h(t, e, d)
                    })) : h(t, c, d)
                }
            }))
        }
        function l(t, e) {
            if ((r = e.remuxResult).audio || r.video || r.text || r.id3 || r.initSegment) {
                var r, i = [],
                a = e.remuxResult,
                n = a.audio,
                s = a.video;
                n && u(i, n),
                s && u(i, s),
                t.postMessage({
                    event: "transmuxComplete",
                    data: e
                },
                i)
            }
        }
        function u(t, e) {
            e.data1 && t.push(e.data1.buffer),
            e.data2 && t.push(e.data2.buffer)
        }
        function h(t, e, r) {
            e.forEach((function(e) {
                l(t, e)
            })),
            t.postMessage({
                event: "flush",
                data: r
            })
        }
    },
    13 : function(t, e, r) {
        "use strict";
        var i = Object.prototype.hasOwnProperty,
        a = "~";
        function n() {}
        function s(t, e, r) {
            this.fn = t,
            this.context = e,
            this.once = r || !1
        }
        function o(t, e, r, i, n) {
            if ("function" != typeof r) throw new TypeError("The listener must be a function");
            var o = new s(r, i || t, n),
            l = a ? a + e: e;
            return t._events[l] ? t._events[l].fn ? t._events[l] = [t._events[l], o] : t._events[l].push(o) : (t._events[l] = o, t._eventsCount++),
            t
        }
        function l(t, e) {
            0 == --t._eventsCount ? t._events = new n: delete t._events[e]
        }
        function u() {
            this._events = new n,
            this._eventsCount = 0
        }
        Object.create && (n.prototype = Object.create(null), (new n).__proto__ || (a = !1)),
        u.prototype.eventNames = function() {
            var t, e, r = [];
            if (0 === this._eventsCount) return r;
            for (e in t = this._events) i.call(t, e) && r.push(a ? e.slice(1) : e);
            return Object.getOwnPropertySymbols ? r.concat(Object.getOwnPropertySymbols(t)) : r
        },
        u.prototype.listeners = function(t) {
            var e = a ? a + t: t,
            r = this._events[e];
            if (!r) return [];
            if (r.fn) return [r.fn];
            for (var i = 0,
            n = r.length,
            s = new Array(n); i < n; i++) s[i] = r[i].fn;
            return s
        },
        u.prototype.listenerCount = function(t) {
            var e = a ? a + t: t,
            r = this._events[e];
            return r ? r.fn ? 1 : r.length: 0
        },
        u.prototype.emit = function(t, e, r, i, n, s) {
            var o = a ? a + t: t;
            if (!this._events[o]) return ! 1;
            var l, u, h = this._events[o],
            d = arguments.length;
            if (h.fn) {
                switch (h.once && this.removeListener(t, h.fn, void 0, !0), d) {
                case 1:
                    return h.fn.call(h.context),
                    !0;
                case 2:
                    return h.fn.call(h.context, e),
                    !0;
                case 3:
                    return h.fn.call(h.context, e, r),
                    !0;
                case 4:
                    return h.fn.call(h.context, e, r, i),
                    !0;
                case 5:
                    return h.fn.call(h.context, e, r, i, n),
                    !0;
                case 6:
                    return h.fn.call(h.context, e, r, i, n, s),
                    !0
                }
                for (u = 1, l = new Array(d - 1); u < d; u++) l[u - 1] = arguments[u];
                h.fn.apply(h.context, l)
            } else {
                var c, f = h.length;
                for (u = 0; u < f; u++) switch (h[u].once && this.removeListener(t, h[u].fn, void 0, !0), d) {
                case 1:
                    h[u].fn.call(h[u].context);
                    break;
                case 2:
                    h[u].fn.call(h[u].context, e);
                    break;
                case 3:
                    h[u].fn.call(h[u].context, e, r);
                    break;
                case 4:
                    h[u].fn.call(h[u].context, e, r, i);
                    break;
                default:
                    if (!l) for (c = 1, l = new Array(d - 1); c < d; c++) l[c - 1] = arguments[c];
                    h[u].fn.apply(h[u].context, l)
                }
            }
            return ! 0
        },
        u.prototype.on = function(t, e, r) {
            return o(this, t, e, r, !1)
        },
        u.prototype.once = function(t, e, r) {
            return o(this, t, e, r, !0)
        },
        u.prototype.removeListener = function(t, e, r, i) {
            var n = a ? a + t: t;
            if (!this._events[n]) return this;
            if (!e) return l(this, n),
            this;
            var s = this._events[n];
            if (s.fn) s.fn !== e || i && !s.once || r && s.context !== r || l(this, n);
            else {
                for (var o = 0,
                u = [], h = s.length; o < h; o++)(s[o].fn !== e || i && !s[o].once || r && s[o].context !== r) && u.push(s[o]);
                u.length ? this._events[n] = 1 === u.length ? u[0] : u: l(this, n)
            }
            return this
        },
        u.prototype.removeAllListeners = function(t) {
            var e;
            return t ? (e = a ? a + t: t, this._events[e] && l(this, e)) : (this._events = new n, this._eventsCount = 0),
            this
        },
        u.prototype.off = u.prototype.removeListener,
        u.prototype.addListener = u.prototype.on,
        u.prefixed = a,
        u.EventEmitter = u,
        t.exports = u
    },
    1 : function(t, e, r) {
        "use strict";
        r.d(e, "a", (function() {
            return o
        })),
        r.d(e, "b", (function() {
            return l
        }));
        var i = function() {},
        a = {
            trace: i,
            debug: i,
            log: i,
            warn: i,
            info: i,
            error: i
        },
        n = a;
        function s(t) {
            var e = self.console[t];
            return e ? e.bind(self.console, "[" + t + "] >") : i
        }
        function o(t) {
            if (self.console && !0 === t || "object" == typeof t) { !
                function(t) {
                    for (var e = arguments.length,
                    r = new Array(e > 1 ? e - 1 : 0), i = 1; i < e; i++) r[i - 1] = arguments[i];
                    r.forEach((function(e) {
                        n[e] = t[e] ? t[e].bind(t) : s(e)
                    }))
                } (t, "debug", "log", "info", "warn", "error");
                try {
                    n.log()
                } catch(t) {
                    n = a
                }
            } else n = a
        }
        var l = a
    },
    0 : function(t, e, r) {
        "use strict";
        var i;
        r.d(e, "a", (function() {
            return i
        })),
        function(t) {
            t.MEDIA_ATTACHING = "hlsMediaAttaching",
            t.MEDIA_ATTACHED = "hlsMediaAttached",
            t.MEDIA_DETACHING = "hlsMediaDetaching",
            t.MEDIA_DETACHED = "hlsMediaDetached",
            t.BUFFER_RESET = "hlsBufferReset",
            t.BUFFER_CODECS = "hlsBufferCodecs",
            t.BUFFER_CREATED = "hlsBufferCreated",
            t.BUFFER_APPENDING = "hlsBufferAppending",
            t.BUFFER_APPENDED = "hlsBufferAppended",
            t.BUFFER_EOS = "hlsBufferEos",
            t.BUFFER_FLUSHING = "hlsBufferFlushing",
            t.BUFFER_FLUSHED = "hlsBufferFlushed",
            t.MANIFEST_LOADING = "hlsManifestLoading",
            t.MANIFEST_LOADED = "hlsManifestLoaded",
            t.MANIFEST_PARSED = "hlsManifestParsed",
            t.LEVEL_SWITCHING = "hlsLevelSwitching",
            t.LEVEL_SWITCHED = "hlsLevelSwitched",
            t.LEVEL_LOADING = "hlsLevelLoading",
            t.LEVEL_LOADED = "hlsLevelLoaded",
            t.LEVEL_UPDATED = "hlsLevelUpdated",
            t.LEVEL_PTS_UPDATED = "hlsLevelPtsUpdated",
            t.LEVELS_UPDATED = "hlsLevelsUpdated",
            t.AUDIO_TRACKS_UPDATED = "hlsAudioTracksUpdated",
            t.AUDIO_TRACK_SWITCHING = "hlsAudioTrackSwitching",
            t.AUDIO_TRACK_SWITCHED = "hlsAudioTrackSwitched",
            t.AUDIO_TRACK_LOADING = "hlsAudioTrackLoading",
            t.AUDIO_TRACK_LOADED = "hlsAudioTrackLoaded",
            t.SUBTITLE_TRACKS_UPDATED = "hlsSubtitleTracksUpdated",
            t.SUBTITLE_TRACKS_CLEARED = "hlsSubtitleTracksCleared",
            t.SUBTITLE_TRACK_SWITCH = "hlsSubtitleTrackSwitch",
            t.SUBTITLE_TRACK_LOADING = "hlsSubtitleTrackLoading",
            t.SUBTITLE_TRACK_LOADED = "hlsSubtitleTrackLoaded",
            t.SUBTITLE_FRAG_PROCESSED = "hlsSubtitleFragProcessed",
            t.CUES_PARSED = "hlsCuesParsed",
            t.NON_NATIVE_TEXT_TRACKS_FOUND = "hlsNonNativeTextTracksFound",
            t.INIT_PTS_FOUND = "hlsInitPtsFound",
            t.FRAG_LOADING = "hlsFragLoading",
            t.FRAG_LOAD_EMERGENCY_ABORTED = "hlsFragLoadEmergencyAborted",
            t.FRAG_LOADED = "hlsFragLoaded",
            t.FRAG_DECRYPTED = "hlsFragDecrypted",
            t.FRAG_PARSING_INIT_SEGMENT = "hlsFragParsingInitSegment",
            t.FRAG_PARSING_USERDATA = "hlsFragParsingUserdata",
            t.FRAG_PARSING_METADATA = "hlsFragParsingMetadata",
            t.FRAG_PARSED = "hlsFragParsed",
            t.FRAG_BUFFERED = "hlsFragBuffered",
            t.FRAG_CHANGED = "hlsFragChanged",
            t.FPS_DROP = "hlsFpsDrop",
            t.FPS_DROP_LEVEL_CAPPING = "hlsFpsDropLevelCapping",
            t.ERROR = "hlsError",
            t.DESTROYING = "hlsDestroying",
            t.KEY_LOADING = "hlsKeyLoading",
            t.KEY_LOADED = "hlsKeyLoaded",
            t.LIVE_BACK_BUFFER_REACHED = "hlsLiveBackBufferReached",
            t.BACK_BUFFER_REACHED = "hlsBackBufferReached"
        } (i || (i = {}))
    },
    9 : function(t, e, r) {
        "use strict";
        r.d(e, "c", (function() {
            return lt
        })),
        r.d(e, "d", (function() {
            return ht
        })),
        r.d(e, "a", (function() {
            return dt
        })),
        r.d(e, "b", (function() {
            return ct
        }));
        var i = r(0),
        a = r(2),
        n = r(14),
        s = r(3),
        o = r(6);
        var l = r(5),
        u = r(8),
        h = function() {
            function t() {
                this._audioTrack = void 0,
                this._id3Track = void 0,
                this.frameIndex = 0,
                this.cachedData = null,
                this.initPTS = null
            }
            var e = t.prototype;
            return e.resetInitSegment = function(t, e, r) {
                this._id3Track = {
                    type: "id3",
                    id: 0,
                    pid: -1,
                    inputTimeScale: 9e4,
                    sequenceNumber: 0,
                    samples: [],
                    dropped: 0
                }
            },
            e.resetTimeStamp = function() {},
            e.resetContiguity = function() {},
            e.canParse = function(t, e) {
                return ! 1
            },
            e.appendFrame = function(t, e, r) {},
            e.demux = function(t, e) {
                this.cachedData && (t = Object(l.a)(this.cachedData, t), this.cachedData = null);
                var r, i, a = o.b(t, 0),
                n = a ? a.length: 0,
                s = this._audioTrack,
                h = this._id3Track,
                c = a ? o.d(a) : void 0,
                f = t.length;
                for (0 !== this.frameIndex && null !== this.initPTS || (this.initPTS = d(c, e)), a && a.length > 0 && h.samples.push({
                    pts: this.initPTS,
                    dts: this.initPTS,
                    data: a
                }), i = this.initPTS; n < f;) {
                    if (this.canParse(t, n)) {
                        var g = this.appendFrame(s, t, n);
                        g ? (this.frameIndex++, i = g.sample.pts, r = n += g.length) : n = f
                    } else o.a(t, n) ? (a = o.b(t, n), h.samples.push({
                        pts: i,
                        dts: i,
                        data: a
                    }), r = n += a.length) : n++;
                    if (n === f && r !== f) {
                        var v = Object(u.a)(t, r);
                        this.cachedData ? this.cachedData = Object(l.a)(this.cachedData, v) : this.cachedData = v
                    }
                }
                return {
                    audioTrack: s,
                    avcTrack: {
                        type: "",
                        id: -1,
                        pid: -1,
                        inputTimeScale: 9e4,
                        sequenceNumber: -1,
                        samples: [],
                        dropped: 0
                    },
                    id3Track: h,
                    textTrack: {
                        type: "",
                        id: -1,
                        pid: -1,
                        inputTimeScale: 9e4,
                        sequenceNumber: -1,
                        samples: [],
                        dropped: 0
                    }
                }
            },
            e.demuxSampleAes = function(t, e, r) {
                return Promise.reject(new Error("[" + this + "] This demuxer does not support Sample-AES decryption"))
            },
            e.flush = function(t) {
                var e = this.cachedData;
                return e && (this.cachedData = null, this.demux(e, 0)),
                this.frameIndex = 0,
                {
                    audioTrack: this._audioTrack,
                    avcTrack: {
                        type: "",
                        id: -1,
                        pid: -1,
                        inputTimeScale: 9e4,
                        sequenceNumber: -1,
                        samples: [],
                        dropped: 0
                    },
                    id3Track: this._id3Track,
                    textTrack: {
                        type: "",
                        id: -1,
                        pid: -1,
                        inputTimeScale: 9e4,
                        sequenceNumber: -1,
                        samples: [],
                        dropped: 0
                    }
                }
            },
            e.destroy = function() {},
            t
        } (),
        d = function(t, e) {
            return Object(s.a)(t) ? 90 * t: 9e4 * e
        },
        c = h,
        f = r(1);
        function g(t, e) {
            return 255 === t[e] && 240 == (246 & t[e + 1])
        }
        function v(t, e) {
            return 1 & t[e + 1] ? 7 : 9
        }
        function p(t, e) {
            return (3 & t[e + 3]) << 11 | t[e + 4] << 3 | (224 & t[e + 5]) >>> 5
        }
        function m(t, e) {
            return e + 1 < t.length && g(t, e)
        }
        function y(t, e) {
            if (m(t, e)) {
                var r = v(t, e);
                if (e + r >= t.length) return ! 1;
                var i = p(t, e);
                if (i <= r) return ! 1;
                var a = e + i;
                return a === t.length || m(t, a)
            }
            return ! 1
        }
        function T(t, e, r, n, s) {
            if (!t.samplerate) {
                var o = function(t, e, r, n) {
                    var s, o, l, u, h = navigator.userAgent.toLowerCase(),
                    d = n,
                    c = [96e3, 88200, 64e3, 48e3, 44100, 32e3, 24e3, 22050, 16e3, 12e3, 11025, 8e3, 7350];
                    s = 1 + ((192 & e[r + 2]) >>> 6);
                    var g = (60 & e[r + 2]) >>> 2;
                    if (! (g > c.length - 1)) return l = (1 & e[r + 2]) << 2,
                    l |= (192 & e[r + 3]) >>> 6,
                    f.b.log("manifest codec:" + n + ", ADTS type:" + s + ", samplingIndex:" + g),
                    /firefox/i.test(h) ? g >= 6 ? (s = 5, u = new Array(4), o = g - 3) : (s = 2, u = new Array(2), o = g) : -1 !== h.indexOf("android") ? (s = 2, u = new Array(2), o = g) : (s = 5, u = new Array(4), n && ( - 1 !== n.indexOf("mp4a.40.29") || -1 !== n.indexOf("mp4a.40.5")) || !n && g >= 6 ? o = g - 3 : ((n && -1 !== n.indexOf("mp4a.40.2") && (g >= 6 && 1 === l || /vivaldi/i.test(h)) || !n && 1 === l) && (s = 2, u = new Array(2)), o = g)),
                    u[0] = s << 3,
                    u[0] |= (14 & g) >> 1,
                    u[1] |= (1 & g) << 7,
                    u[1] |= l << 3,
                    5 === s && (u[1] |= (14 & o) >> 1, u[2] = (1 & o) << 7, u[2] |= 8, u[3] = 0),
                    {
                        config: u,
                        samplerate: c[g],
                        channelCount: l,
                        codec: "mp4a.40." + s,
                        manifestCodec: d
                    };
                    t.trigger(i.a.ERROR, {
                        type: a.b.MEDIA_ERROR,
                        details: a.a.FRAG_PARSING_ERROR,
                        fatal: !0,
                        reason: "invalid ADTS sampling index:" + g
                    })
                } (e, r, n, s);
                if (!o) return;
                t.config = o.config,
                t.samplerate = o.samplerate,
                t.channelCount = o.channelCount,
                t.codec = o.codec,
                t.manifestCodec = o.manifestCodec,
                f.b.log("parsed codec:" + t.codec + ", rate:" + o.samplerate + ", channels:" + o.channelCount)
            }
        }
        function E(t) {
            return 9216e4 / t
        }
        function b(t, e, r, i, a) {
            var n = function(t, e, r, i, a) {
                var n = t.length,
                s = v(t, e),
                o = p(t, e);
                if ((o -= s) > 0 && e + s + o <= n) return {
                    headerLength: s,
                    frameLength: o,
                    stamp: r + i * a
                }
            } (e, r, i, a, E(t.samplerate));
            if (n) {
                var s = n.stamp,
                o = n.headerLength,
                l = n.frameLength,
                u = {
                    unit: e.subarray(r + o, r + o + l),
                    pts: s,
                    dts: s
                };
                return t.samples.push(u),
                {
                    sample: u,
                    length: l + o
                }
            }
        }
        function S(t, e) {
            return (S = Object.setPrototypeOf ||
            function(t, e) {
                return t.__proto__ = e,
                t
            })(t, e)
        }
        var L = function(t) {
            var e, r;
            function i(e, r) {
                var i;
                return (i = t.call(this) || this).observer = void 0,
                i.config = void 0,
                i.observer = e,
                i.config = r,
                i
            }
            r = t,
            (e = i).prototype = Object.create(r.prototype),
            e.prototype.constructor = e,
            S(e, r);
            var a = i.prototype;
            return a.resetInitSegment = function(e, r, i) {
                t.prototype.resetInitSegment.call(this, e, r, i),
                this._audioTrack = {
                    container: "audio/adts",
                    type: "audio",
                    id: 0,
                    pid: -1,
                    sequenceNumber: 0,
                    isAAC: !0,
                    samples: [],
                    manifestCodec: e,
                    duration: i,
                    inputTimeScale: 9e4,
                    dropped: 0
                }
            },
            i.probe = function(t) {
                if (!t) return ! 1;
                for (var e = (o.b(t, 0) || []).length, r = t.length; e < r; e++) if (y(t, e)) return f.b.log("ADTS sync word found !"),
                !0;
                return ! 1
            },
            a.canParse = function(t, e) {
                return function(t, e) {
                    return function(t, e) {
                        return e + 5 < t.length
                    } (t, e) && g(t, e) && p(t, e) <= t.length - e
                } (t, e)
            },
            a.appendFrame = function(t, e, r) {
                return T(t, this.observer, e, r, t.manifestCodec),
                b(t, e, r, this.initPTS, this.frameIndex)
            },
            i
        } (c);
        L.minProbeByteLength = 9;
        var A = L,
        R = function() {
            function t(t, e) {
                this.remainderData = null,
                this.config = void 0,
                this.config = e
            }
            var e = t.prototype;
            return e.resetTimeStamp = function() {},
            e.resetInitSegment = function() {},
            e.resetContiguity = function() {},
            t.probe = function(t) {
                return Object(l.b)({
                    data: t,
                    start: 0,
                    end: Math.min(t.length, 16384)
                },
                ["moof"]).length > 0
            },
            e.demux = function(t) {
                var e = t,
                r = {
                    type: "",
                    id: -1,
                    pid: -1,
                    inputTimeScale: 9e4,
                    sequenceNumber: -1,
                    samples: [],
                    dropped: 0
                };
                if (this.config.progressive) {
                    this.remainderData && (e = Object(l.a)(this.remainderData, t));
                    var i = Object(l.h)(e);
                    this.remainderData = i.remainder,
                    r.samples = i.valid || new Uint8Array
                } else r.samples = e;
                return {
                    audioTrack: {
                        type: "",
                        id: -1,
                        pid: -1,
                        inputTimeScale: 9e4,
                        sequenceNumber: -1,
                        samples: [],
                        dropped: 0
                    },
                    avcTrack: r,
                    id3Track: {
                        type: "",
                        id: -1,
                        pid: -1,
                        inputTimeScale: 9e4,
                        sequenceNumber: -1,
                        samples: [],
                        dropped: 0
                    },
                    textTrack: {
                        type: "",
                        id: -1,
                        pid: -1,
                        inputTimeScale: 9e4,
                        sequenceNumber: -1,
                        samples: [],
                        dropped: 0
                    }
                }
            },
            e.flush = function() {
                var t = {
                    type: "",
                    id: -1,
                    pid: -1,
                    inputTimeScale: 9e4,
                    sequenceNumber: -1,
                    samples: [],
                    dropped: 0
                };
                return t.samples = this.remainderData || new Uint8Array,
                this.remainderData = null,
                {
                    audioTrack: {
                        type: "",
                        id: -1,
                        pid: -1,
                        inputTimeScale: 9e4,
                        sequenceNumber: -1,
                        samples: [],
                        dropped: 0
                    },
                    avcTrack: t,
                    id3Track: {
                        type: "",
                        id: -1,
                        pid: -1,
                        inputTimeScale: 9e4,
                        sequenceNumber: -1,
                        samples: [],
                        dropped: 0
                    },
                    textTrack: {
                        type: "",
                        id: -1,
                        pid: -1,
                        inputTimeScale: 9e4,
                        sequenceNumber: -1,
                        samples: [],
                        dropped: 0
                    }
                }
            },
            e.demuxSampleAes = function(t, e, r) {
                return Promise.reject(new Error("The MP4 demuxer does not support SAMPLE-AES decryption"))
            },
            e.destroy = function() {},
            t
        } ();
        R.minProbeByteLength = 1024;
        var D = R,
        k = null,
        _ = [32, 64, 96, 128, 160, 192, 224, 256, 288, 320, 352, 384, 416, 448, 32, 48, 56, 64, 80, 96, 112, 128, 160, 192, 224, 256, 320, 384, 32, 40, 48, 56, 64, 80, 96, 112, 128, 160, 192, 224, 256, 320, 32, 48, 56, 64, 80, 96, 112, 128, 144, 160, 176, 192, 224, 256, 8, 16, 24, 32, 40, 48, 56, 64, 80, 96, 112, 128, 144, 160],
        I = [44100, 48e3, 32e3, 22050, 24e3, 16e3, 11025, 12e3, 8e3],
        C = [[0, 72, 144, 12], [0, 0, 0, 0], [0, 72, 144, 12], [0, 144, 144, 12]],
        w = [0, 1, 1, 4];
        function O(t, e, r, i, a) {
            if (! (r + 24 > e.length)) {
                var n = x(e, r);
                if (n && r + n.frameLength <= e.length) {
                    var s = i + a * (9e4 * n.samplesPerFrame / n.sampleRate),
                    o = {
                        unit: e.subarray(r, r + n.frameLength),
                        pts: s,
                        dts: s
                    };
                    return t.config = [],
                    t.channelCount = n.channelCount,
                    t.samplerate = n.sampleRate,
                    t.samples.push(o),
                    {
                        sample: o,
                        length: n.frameLength
                    }
                }
            }
        }
        function x(t, e) {
            var r = t[e + 1] >> 3 & 3,
            i = t[e + 1] >> 1 & 3,
            a = t[e + 2] >> 4 & 15,
            n = t[e + 2] >> 2 & 3;
            if (1 !== r && 0 !== a && 15 !== a && 3 !== n) {
                var s = t[e + 2] >> 1 & 1,
                o = t[e + 3] >> 6,
                l = 1e3 * _[14 * (3 === r ? 3 - i: 3 === i ? 3 : 4) + a - 1],
                u = I[3 * (3 === r ? 0 : 2 === r ? 1 : 2) + n],
                h = 3 === o ? 1 : 2,
                d = C[r][i],
                c = w[i],
                f = 8 * d * c,
                g = Math.floor(d * l / u + s) * c;
                if (null === k) {
                    var v = (navigator.userAgent || "").match(/Chrome\/(\d+)/i);
                    k = v ? parseInt(v[1]) : 0
                }
                return !! k && k <= 87 && 2 === i && l >= 224e3 && 0 === o && (t[e + 3] = 128 | t[e + 3]),
                {
                    sampleRate: u,
                    channelCount: h,
                    frameLength: g,
                    samplesPerFrame: f
                }
            }
        }
        function P(t, e) {
            return 255 === t[e] && 224 == (224 & t[e + 1]) && 0 != (6 & t[e + 1])
        }
        function M(t, e) {
            return e + 1 < t.length && P(t, e)
        }
        function F(t, e) {
            if (e + 1 < t.length && P(t, e)) {
                var r = x(t, e),
                i = 4;
                null != r && r.frameLength && (i = r.frameLength);
                var a = e + i;
                return a === t.length || M(t, a)
            }
            return ! 1
        }
        var N = function() {
            function t(t) {
                this.data = void 0,
                this.bytesAvailable = void 0,
                this.word = void 0,
                this.bitsAvailable = void 0,
                this.data = t,
                this.bytesAvailable = t.byteLength,
                this.word = 0,
                this.bitsAvailable = 0
            }
            var e = t.prototype;
            return e.loadWord = function() {
                var t = this.data,
                e = this.bytesAvailable,
                r = t.byteLength - e,
                i = new Uint8Array(4),
                a = Math.min(4, e);
                if (0 === a) throw new Error("no bytes available");
                i.set(t.subarray(r, r + a)),
                this.word = new DataView(i.buffer).getUint32(0),
                this.bitsAvailable = 8 * a,
                this.bytesAvailable -= a
            },
            e.skipBits = function(t) {
                var e;
                this.bitsAvailable > t ? (this.word <<= t, this.bitsAvailable -= t) : (t -= this.bitsAvailable, t -= (e = t >> 3) >> 3, this.bytesAvailable -= e, this.loadWord(), this.word <<= t, this.bitsAvailable -= t)
            },
            e.readBits = function(t) {
                var e = Math.min(this.bitsAvailable, t),
                r = this.word >>> 32 - e;
                return t > 32 && f.b.error("Cannot read more than 32 bits at a time"),
                this.bitsAvailable -= e,
                this.bitsAvailable > 0 ? this.word <<= e: this.bytesAvailable > 0 && this.loadWord(),
                (e = t - e) > 0 && this.bitsAvailable ? r << e | this.readBits(e) : r
            },
            e.skipLZ = function() {
                var t;
                for (t = 0; t < this.bitsAvailable; ++t) if (0 != (this.word & 2147483648 >>> t)) return this.word <<= t,
                this.bitsAvailable -= t,
                t;
                return this.loadWord(),
                t + this.skipLZ()
            },
            e.skipUEG = function() {
                this.skipBits(1 + this.skipLZ())
            },
            e.skipEG = function() {
                this.skipBits(1 + this.skipLZ())
            },
            e.readUEG = function() {
                var t = this.skipLZ();
                return this.readBits(t + 1) - 1
            },
            e.readEG = function() {
                var t = this.readUEG();
                return 1 & t ? 1 + t >>> 1 : -1 * (t >>> 1)
            },
            e.readBoolean = function() {
                return 1 === this.readBits(1)
            },
            e.readUByte = function() {
                return this.readBits(8)
            },
            e.readUShort = function() {
                return this.readBits(16)
            },
            e.readUInt = function() {
                return this.readBits(32)
            },
            e.skipScalingList = function(t) {
                for (var e = 8,
                r = 8,
                i = 0; i < t; i++) 0 !== r && (r = (e + this.readEG() + 256) % 256),
                e = 0 === r ? e: r
            },
            e.readSPS = function() {
                var t, e, r, i = 0,
                a = 0,
                n = 0,
                s = 0,
                o = this.readUByte.bind(this),
                l = this.readBits.bind(this),
                u = this.readUEG.bind(this),
                h = this.readBoolean.bind(this),
                d = this.skipBits.bind(this),
                c = this.skipEG.bind(this),
                f = this.skipUEG.bind(this),
                g = this.skipScalingList.bind(this);
                o();
                var v = o();
                if (l(5), d(3), o(), f(), 100 === v || 110 === v || 122 === v || 244 === v || 44 === v || 83 === v || 86 === v || 118 === v || 128 === v) {
                    var p = u();
                    if (3 === p && d(1), f(), f(), d(1), h()) for (e = 3 !== p ? 8 : 12, r = 0; r < e; r++) h() && g(r < 6 ? 16 : 64)
                }
                f();
                var m = u();
                if (0 === m) u();
                else if (1 === m) for (d(1), c(), c(), t = u(), r = 0; r < t; r++) c();
                f(),
                d(1);
                var y = u(),
                T = u(),
                E = l(1);
                0 === E && d(1),
                d(1),
                h() && (i = u(), a = u(), n = u(), s = u());
                var b = [1, 1];
                if (h() && h()) switch (o()) {
                case 1:
                    b = [1, 1];
                    break;
                case 2:
                    b = [12, 11];
                    break;
                case 3:
                    b = [10, 11];
                    break;
                case 4:
                    b = [16, 11];
                    break;
                case 5:
                    b = [40, 33];
                    break;
                case 6:
                    b = [24, 11];
                    break;
                case 7:
                    b = [20, 11];
                    break;
                case 8:
                    b = [32, 11];
                    break;
                case 9:
                    b = [80, 33];
                    break;
                case 10:
                    b = [18, 11];
                    break;
                case 11:
                    b = [15, 11];
                    break;
                case 12:
                    b = [64, 33];
                    break;
                case 13:
                    b = [160, 99];
                    break;
                case 14:
                    b = [4, 3];
                    break;
                case 15:
                    b = [3, 2];
                    break;
                case 16:
                    b = [2, 1];
                    break;
                case 255:
                    b = [o() << 8 | o(), o() << 8 | o()]
                }
                return {
                    width: Math.ceil(16 * (y + 1) - 2 * i - 2 * a),
                    height: (2 - E) * (T + 1) * 16 - (E ? 2 : 4) * (n + s),
                    pixelRatio: b
                }
            },
            e.readSliceType = function() {
                return this.readUByte(),
                this.readUEG(),
                this.readUEG()
            },
            t
        } (),
        U = function() {
            function t(t, e, r) {
                this.keyData = void 0,
                this.decrypter = void 0,
                this.keyData = r,
                this.decrypter = new n.a(t, e, {
                    removePKCS7Padding: !1
                })
            }
            var e = t.prototype;
            return e.decryptBuffer = function(t, e) {
                this.decrypter.decrypt(t, this.keyData.key.buffer, this.keyData.iv.buffer, e)
            },
            e.decryptAacSample = function(t, e, r, i) {
                var a = t[e].unit,
                n = a.subarray(16, a.length - a.length % 16),
                s = n.buffer.slice(n.byteOffset, n.byteOffset + n.length),
                o = this;
                this.decryptBuffer(s, (function(n) {
                    var s = new Uint8Array(n);
                    a.set(s, 16),
                    i || o.decryptAacSamples(t, e + 1, r)
                }))
            },
            e.decryptAacSamples = function(t, e, r) {
                for (;; e++) {
                    if (e >= t.length) return void r();
                    if (! (t[e].unit.length < 32)) {
                        var i = this.decrypter.isSync();
                        if (this.decryptAacSample(t, e, r, i), !i) return
                    }
                }
            },
            e.getAvcEncryptedData = function(t) {
                for (var e = 16 * Math.floor((t.length - 48) / 160) + 16, r = new Int8Array(e), i = 0, a = 32; a <= t.length - 16; a += 160, i += 16) r.set(t.subarray(a, a + 16), i);
                return r
            },
            e.getAvcDecryptedUnit = function(t, e) {
                for (var r = new Uint8Array(e), i = 0, a = 32; a <= t.length - 16; a += 160, i += 16) t.set(r.subarray(i, i + 16), a);
                return t
            },
            e.decryptAvcSample = function(t, e, r, i, a, n) {
                var s = q(a.data),
                o = this.getAvcEncryptedData(s),
                l = this;
                this.decryptBuffer(o.buffer, (function(o) {
                    a.data = l.getAvcDecryptedUnit(s, o),
                    n || l.decryptAvcSamples(t, e, r + 1, i)
                }))
            },
            e.decryptAvcSamples = function(t, e, r, i) {
                if (t instanceof Uint8Array) throw new Error("Cannot decrypt samples of type Uint8Array");
                for (;; e++, r = 0) {
                    if (e >= t.length) return void i();
                    for (var a = t[e].units; ! (r >= a.length); r++) {
                        var n = a[r];
                        if (! (n.data.length <= 48 || 1 !== n.type && 5 !== n.type)) {
                            var s = this.decrypter.isSync();
                            if (this.decryptAvcSample(t, e, r, i, n, s), !s) return
                        }
                    }
                }
            },
            t
        } (),
        B = {
            video: 1,
            audio: 2,
            id3: 3,
            text: 4
        },
        G = function() {
            function t(t, e, r) {
                this.observer = void 0,
                this.config = void 0,
                this.typeSupported = void 0,
                this.sampleAes = null,
                this.pmtParsed = !1,
                this.audioCodec = void 0,
                this.videoCodec = void 0,
                this._duration = 0,
                this.aacLastPTS = null,
                this._initPTS = null,
                this._initDTS = null,
                this._pmtId = -1,
                this._avcTrack = void 0,
                this._audioTrack = void 0,
                this._id3Track = void 0,
                this._txtTrack = void 0,
                this.aacOverFlow = null,
                this.avcSample = null,
                this.remainderData = null,
                this.observer = t,
                this.config = e,
                this.typeSupported = r
            }
            t.probe = function(e) {
                var r = t.syncOffset(e);
                return ! (r < 0) && (r && f.b.warn("MPEG2-TS detected but first sync word found @ offset " + r + ", junk ahead ?"), !0)
            },
            t.syncOffset = function(t) {
                for (var e = Math.min(1e3, t.length - 564), r = 0; r < e;) {
                    if (71 === t[r] && 71 === t[r + 188] && 71 === t[r + 376]) return r;
                    r++
                }
                return - 1
            },
            t.createTrack = function(t, e) {
                return {
                    container: "video" === t || "audio" === t ? "video/mp2t": void 0,
                    type: t,
                    id: B[t],
                    pid: -1,
                    inputTimeScale: 9e4,
                    sequenceNumber: 0,
                    samples: [],
                    dropped: 0,
                    duration: "audio" === t ? e: void 0
                }
            };
            var e = t.prototype;
            return e.resetInitSegment = function(e, r, i) {
                this.pmtParsed = !1,
                this._pmtId = -1,
                this._avcTrack = t.createTrack("video", i),
                this._audioTrack = t.createTrack("audio", i),
                this._id3Track = t.createTrack("id3", i),
                this._txtTrack = t.createTrack("text", i),
                this._audioTrack.isAAC = !0,
                this.aacOverFlow = null,
                this.aacLastPTS = null,
                this.avcSample = null,
                this.audioCodec = e,
                this.videoCodec = r,
                this._duration = i
            },
            e.resetTimeStamp = function() {},
            e.resetContiguity = function() {
                var t = this._audioTrack,
                e = this._avcTrack,
                r = this._id3Track;
                t && (t.pesData = null),
                e && (e.pesData = null),
                r && (r.pesData = null),
                this.aacOverFlow = null,
                this.aacLastPTS = null
            },
            e.demux = function(e, r, n, s) {
                var o;
                void 0 === n && (n = !1),
                void 0 === s && (s = !1),
                n || (this.sampleAes = null);
                var u = this._avcTrack,
                h = this._audioTrack,
                d = this._id3Track,
                c = u.pid,
                g = u.pesData,
                v = h.pid,
                p = d.pid,
                m = h.pesData,
                y = d.pesData,
                T = !1,
                E = this.pmtParsed,
                b = this._pmtId,
                S = e.length;
                if (this.remainderData && (S = (e = Object(l.a)(this.remainderData, e)).length, this.remainderData = null), S < 188 && !s) return this.remainderData = e,
                {
                    audioTrack: h,
                    avcTrack: u,
                    id3Track: d,
                    textTrack: this._txtTrack
                };
                var L = Math.max(0, t.syncOffset(e)); (S -= (S + L) % 188) < e.byteLength && !s && (this.remainderData = new Uint8Array(e.buffer, S, e.buffer.byteLength - S));
                for (var A = L; A < S; A += 188) if (71 === e[A]) {
                    var R = !!(64 & e[A + 1]),
                    D = ((31 & e[A + 1]) << 8) + e[A + 2],
                    k = void 0;
                    if ((48 & e[A + 3]) >> 4 > 1) {
                        if ((k = A + 5 + e[A + 4]) === A + 188) continue
                    } else k = A + 4;
                    switch (D) {
                    case c:
                        R && (g && (o = V(g)) && this.parseAVCPES(o, !1), g = {
                            data: [],
                            size: 0
                        }),
                        g && (g.data.push(e.subarray(k, A + 188)), g.size += A + 188 - k);
                        break;
                    case v:
                        R && (m && (o = V(m)) && (h.isAAC ? this.parseAACPES(o) : this.parseMPEGPES(o)), m = {
                            data: [],
                            size: 0
                        }),
                        m && (m.data.push(e.subarray(k, A + 188)), m.size += A + 188 - k);
                        break;
                    case p:
                        R && (y && (o = V(y)) && this.parseID3PES(o), y = {
                            data: [],
                            size: 0
                        }),
                        y && (y.data.push(e.subarray(k, A + 188)), y.size += A + 188 - k);
                        break;
                    case 0:
                        R && (k += e[k] + 1),
                        b = this._pmtId = j(e, k);
                        break;
                    case b:
                        R && (k += e[k] + 1);
                        var _ = H(e, k, !0 === this.typeSupported.mpeg || !0 === this.typeSupported.mp3, n); (c = _.avc) > 0 && (u.pid = c),
                        (v = _.audio) > 0 && (h.pid = v, h.isAAC = _.isAAC),
                        (p = _.id3) > 0 && (d.pid = p),
                        T && !E && (f.b.log("reparse from beginning"), T = !1, A = L - 188),
                        E = this.pmtParsed = !0;
                        break;
                    case 17:
                    case 8191:
                        break;
                    default:
                        T = !0
                    }
                } else this.observer.emit(i.a.ERROR, i.a.ERROR, {
                    type: a.b.MEDIA_ERROR,
                    details: a.a.FRAG_PARSING_ERROR,
                    fatal: !1,
                    reason: "TS packet did not start with 0x47"
                });
                u.pesData = g,
                h.pesData = m,
                d.pesData = y;
                var I = {
                    audioTrack: h,
                    avcTrack: u,
                    id3Track: d,
                    textTrack: this._txtTrack
                };
                return s && this.extractRemainingSamples(I),
                I
            },
            e.flush = function() {
                var t, e = this.remainderData;
                return this.remainderData = null,
                t = e ? this.demux(e, -1, !1, !0) : {
                    audioTrack: this._audioTrack,
                    avcTrack: this._avcTrack,
                    textTrack: this._txtTrack,
                    id3Track: this._id3Track
                },
                this.extractRemainingSamples(t),
                this.sampleAes ? this.decrypt(t, this.sampleAes) : t
            },
            e.extractRemainingSamples = function(t) {
                var e, r = t.audioTrack,
                i = t.avcTrack,
                a = t.id3Track,
                n = i.pesData,
                s = r.pesData,
                o = a.pesData;
                n && (e = V(n)) ? (this.parseAVCPES(e, !0), i.pesData = null) : i.pesData = n,
                s && (e = V(s)) ? (r.isAAC ? this.parseAACPES(e) : this.parseMPEGPES(e), r.pesData = null) : (null != s && s.size && f.b.log("last AAC PES packet truncated,might overlap between fragments"), r.pesData = s),
                o && (e = V(o)) ? (this.parseID3PES(e), a.pesData = null) : a.pesData = o
            },
            e.demuxSampleAes = function(t, e, r) {
                var i = this.demux(t, r, !0, !this.config.progressive),
                a = this.sampleAes = new U(this.observer, this.config, e);
                return this.decrypt(i, a)
            },
            e.decrypt = function(t, e) {
                return new Promise((function(r) {
                    var i = t.audioTrack,
                    a = t.avcTrack;
                    i.samples && i.isAAC ? e.decryptAacSamples(i.samples, 0, (function() {
                        a.samples ? e.decryptAvcSamples(a.samples, 0, 0, (function() {
                            r(t)
                        })) : r(t)
                    })) : a.samples && e.decryptAvcSamples(a.samples, 0, 0, (function() {
                        r(t)
                    }))
                }))
            },
            e.destroy = function() {
                this._initPTS = this._initDTS = null,
                this._duration = 0
            },
            e.parseAVCPES = function(t, e) {
                var r, i = this,
                a = this._avcTrack,
                n = this.parseAVCNALu(t.data),
                s = this.avcSample,
                l = !1;
                t.data = null,
                s && n.length && !a.audFound && (W(s, a), s = this.avcSample = K(!1, t.pts, t.dts, "")),
                n.forEach((function(e) {
                    switch (e.type) {
                    case 1:
                        r = !0,
                        s || (s = i.avcSample = K(!0, t.pts, t.dts, "")),
                        s.frame = !0;
                        var n = e.data;
                        if (l && n.length > 4) {
                            var u = new N(n).readSliceType();
                            2 !== u && 4 !== u && 7 !== u && 9 !== u || (s.key = !0)
                        }
                        break;
                    case 5:
                        r = !0,
                        s || (s = i.avcSample = K(!0, t.pts, t.dts, "")),
                        s.key = !0,
                        s.frame = !0;
                        break;
                    case 6:
                        r = !0;
                        var h = new N(q(e.data));
                        h.readUByte();
                        for (var d = 0,
                        c = 0,
                        f = !1,
                        g = 0; ! f && h.bytesAvailable > 1;) {
                            d = 0;
                            do {
                                d += g = h.readUByte()
                            } while ( 255 === g );
                            c = 0;
                            do {
                                c += g = h.readUByte()
                            } while ( 255 === g );
                            if (4 === d && 0 !== h.bytesAvailable) {
                                if (f = !0, 181 === h.readUByte()) if (49 === h.readUShort()) if (1195456820 === h.readUInt()) if (3 === h.readUByte()) {
                                    for (var v = h.readUByte(), p = 31 & v, m = [v, h.readUByte()], y = 0; y < p; y++) m.push(h.readUByte()),
                                    m.push(h.readUByte()),
                                    m.push(h.readUByte());
                                    Y(i._txtTrack.samples, {
                                        type: 3,
                                        pts: t.pts,
                                        bytes: m
                                    })
                                }
                            } else if (5 === d && 0 !== h.bytesAvailable) {
                                if (f = !0, c > 16) {
                                    for (var T = [], E = 0; E < 16; E++) T.push(h.readUByte().toString(16)),
                                    3 !== E && 5 !== E && 7 !== E && 9 !== E || T.push("-");
                                    for (var b = c - 16,
                                    S = new Uint8Array(b), L = 0; L < b; L++) S[L] = h.readUByte();
                                    Y(i._txtTrack.samples, {
                                        pts: t.pts,
                                        payloadType: d,
                                        uuid: T.join(""),
                                        userData: Object(o.f)(S),
                                        userDataBytes: S
                                    })
                                }
                            } else if (c < h.bytesAvailable) for (var A = 0; A < c; A++) h.readUByte()
                        }
                        break;
                    case 7:
                        if (r = !0, l = !0, !a.sps) {
                            var R = new N(e.data).readSPS();
                            a.width = R.width,
                            a.height = R.height,
                            a.pixelRatio = R.pixelRatio,
                            a.sps = [e.data],
                            a.duration = i._duration;
                            for (var D = e.data.subarray(1, 4), k = "avc1.", _ = 0; _ < 3; _++) {
                                var I = D[_].toString(16);
                                I.length < 2 && (I = "0" + I),
                                k += I
                            }
                            a.codec = k
                        }
                        break;
                    case 8:
                        r = !0,
                        a.pps || (a.pps = [e.data]);
                        break;
                    case 9:
                        r = !1,
                        a.audFound = !0,
                        s && W(s, a),
                        s = i.avcSample = K(!1, t.pts, t.dts, "");
                        break;
                    case 12:
                        r = !1;
                        break;
                    default:
                        r = !1,
                        s && (s.debug += "unknown NAL " + e.type + " ")
                    }
                    s && r && s.units.push(e)
                })),
                e && s && (W(s, a), this.avcSample = null)
            },
            e.getLastNalUnit = function() {
                var t, e, r = this.avcSample;
                if (!r || 0 === r.units.length) {
                    var i = this._avcTrack.samples;
                    r = i[i.length - 1]
                }
                if (null !== (t = r) && void 0 !== t && t.units) {
                    var a = r.units;
                    e = a[a.length - 1]
                }
                return e
            },
            e.parseAVCNALu = function(t) {
                var e, r, i = t.byteLength,
                a = this._avcTrack,
                n = a.naluState || 0,
                s = n,
                o = [],
                l = 0,
                u = -1,
                h = 0;
                for ( - 1 === n && (u = 0, h = 31 & t[0], n = 0, l = 1); l < i;) if (e = t[l++], n) if (1 !== n) if (e) if (1 === e) {
                    if (u >= 0) {
                        var d = {
                            data: t.subarray(u, l - n - 1),
                            type: h
                        };
                        o.push(d)
                    } else {
                        var c = this.getLastNalUnit();
                        if (c && (s && l <= 4 - s && c.state && (c.data = c.data.subarray(0, c.data.byteLength - s)), (r = l - n - 1) > 0)) {
                            var f = new Uint8Array(c.data.byteLength + r);
                            f.set(c.data, 0),
                            f.set(t.subarray(0, r), c.data.byteLength),
                            c.data = f
                        }
                    }
                    l < i ? (u = l, h = 31 & t[l], n = 0) : n = -1
                } else n = 0;
                else n = 3;
                else n = e ? 0 : 2;
                else n = e ? 0 : 1;
                if (u >= 0 && n >= 0) {
                    var g = {
                        data: t.subarray(u, i),
                        type: h,
                        state: n
                    };
                    o.push(g)
                }
                if (0 === o.length) {
                    var v = this.getLastNalUnit();
                    if (v) {
                        var p = new Uint8Array(v.data.byteLength + t.byteLength);
                        p.set(v.data, 0),
                        p.set(t, v.data.byteLength),
                        v.data = p
                    }
                }
                return a.naluState = n,
                o
            },
            e.parseAACPES = function(t) {
                var e, r, n, s, o = this._audioTrack,
                l = this.aacLastPTS,
                u = this.aacOverFlow,
                h = t.data;
                if (u) {
                    var d = new Uint8Array(u.byteLength + h.byteLength);
                    d.set(u, 0),
                    d.set(h, u.byteLength),
                    h = d
                }
                for (e = 0, r = h.length; e < r - 1 && !m(h, e); e++);
                if (e && (e < r - 1 ? (n = "AAC PES did not start with ADTS header,offset:" + e, s = !1) : (n = "no ADTS header found in AAC PES", s = !0), f.b.warn("parsing error:" + n), this.observer.emit(i.a.ERROR, i.a.ERROR, {
                    type: a.b.MEDIA_ERROR,
                    details: a.a.FRAG_PARSING_ERROR,
                    fatal: s,
                    reason: n
                }), s)) return;
                T(o, this.observer, h, e, this.audioCodec);
                var c, g = 0,
                v = E(o.samplerate);
                if (void 0 !== t.pts) c = t.pts;
                else {
                    if (null === l) return void f.b.warn("[tsdemuxer]: AAC PES unknown PTS");
                    c = l
                }
                if (u && null !== l) {
                    var p = l + v;
                    Math.abs(p - c) > 1 && (f.b.log("[tsdemuxer]: AAC: align PTS for overlapping frames by " + Math.round((p - c) / 90)), c = p)
                }
                for (var y = null; e < r;) {
                    if (m(h, e)) {
                        if (e + 5 < r) {
                            var S = b(o, h, e, c, g);
                            if (S) {
                                e += S.length,
                                y = S.sample.pts,
                                g++;
                                continue
                            }
                        }
                        break
                    }
                    e++
                }
                this.aacOverFlow = e < r ? h.subarray(e, r) : null,
                this.aacLastPTS = y
            },
            e.parseMPEGPES = function(t) {
                var e = t.data,
                r = e.length,
                i = 0,
                a = 0,
                n = t.pts;
                if (void 0 !== n) for (; a < r;) if (M(e, a)) {
                    var s = O(this._audioTrack, e, a, n, i);
                    if (!s) break;
                    a += s.length,
                    i++
                } else a++;
                else f.b.warn("[tsdemuxer]: MPEG PES unknown PTS")
            },
            e.parseID3PES = function(t) {
                void 0 !== t.pts ? this._id3Track.samples.push(t) : f.b.warn("[tsdemuxer]: ID3 PES unknown PTS")
            },
            t
        } ();
        function K(t, e, r, i) {
            return {
                key: t,
                frame: !1,
                pts: e,
                dts: r,
                units: [],
                debug: i,
                length: 0
            }
        }
        function j(t, e) {
            return (31 & t[e + 10]) << 8 | t[e + 11]
        }
        function H(t, e, r, i) {
            var a = {
                audio: -1,
                avc: -1,
                id3: -1,
                isAAC: !0
            },
            n = e + 3 + ((15 & t[e + 1]) << 8 | t[e + 2]) - 4;
            for (e += 12 + ((15 & t[e + 10]) << 8 | t[e + 11]); e < n;) {
                var s = (31 & t[e + 1]) << 8 | t[e + 2];
                switch (t[e]) {
                case 207:
                    if (!i) {
                        f.b.log("ADTS AAC with AES-128-CBC frame encryption found in unencrypted stream");
                        break
                    }
                case 15:
                    -1 === a.audio && (a.audio = s);
                    break;
                case 21:
                    -1 === a.id3 && (a.id3 = s);
                    break;
                case 219:
                    if (!i) {
                        f.b.log("H.264 with AES-128-CBC slice encryption found in unencrypted stream");
                        break
                    }
                case 27:
                    -1 === a.avc && (a.avc = s);
                    break;
                case 3:
                case 4:
                    r ? -1 === a.audio && (a.audio = s, a.isAAC = !1) : f.b.log("MPEG audio found, not supported in this browser");
                    break;
                case 36:
                    f.b.warn("Unsupported HEVC stream type found")
                }
                e += 5 + ((15 & t[e + 3]) << 8 | t[e + 4])
            }
            return a
        }
        function V(t) {
            var e, r, i, a, n, s = 0,
            o = t.data;
            if (!t || 0 === t.size) return null;
            for (; o[0].length < 19 && o.length > 1;) {
                var l = new Uint8Array(o[0].length + o[1].length);
                l.set(o[0]),
                l.set(o[1], o[0].length),
                o[0] = l,
                o.splice(1, 1)
            }
            if (1 === ((e = o[0])[0] << 16) + (e[1] << 8) + e[2]) {
                if ((r = (e[4] << 8) + e[5]) && r > t.size - 6) return null;
                var u = e[7];
                192 & u && (a = 536870912 * (14 & e[9]) + 4194304 * (255 & e[10]) + 16384 * (254 & e[11]) + 128 * (255 & e[12]) + (254 & e[13]) / 2, 64 & u ? a - (n = 536870912 * (14 & e[14]) + 4194304 * (255 & e[15]) + 16384 * (254 & e[16]) + 128 * (255 & e[17]) + (254 & e[18]) / 2) > 54e5 && (f.b.warn(Math.round((a - n) / 9e4) + "s delta between PTS and DTS, align them"), a = n) : n = a);
                var h = (i = e[8]) + 9;
                if (t.size <= h) return null;
                t.size -= h;
                for (var d = new Uint8Array(t.size), c = 0, g = o.length; c < g; c++) {
                    var v = (e = o[c]).byteLength;
                    if (h) {
                        if (h > v) {
                            h -= v;
                            continue
                        }
                        e = e.subarray(h),
                        v -= h,
                        h = 0
                    }
                    d.set(e, s),
                    s += v
                }
                return r && (r -= i + 3),
                {
                    data: d,
                    pts: a,
                    dts: n,
                    len: r
                }
            }
            return null
        }
        function W(t, e) {
            if (t.units.length && t.frame) {
                if (void 0 === t.pts) {
                    var r = e.samples,
                    i = r.length;
                    if (!i) return void e.dropped++;
                    var a = r[i - 1];
                    t.pts = a.pts,
                    t.dts = a.dts
                }
                e.samples.push(t)
            }
            t.debug.length && f.b.log(t.pts + "/" + t.dts + ":" + t.debug)
        }
        function Y(t, e) {
            var r = t.length;
            if (r > 0) {
                if (e.pts >= t[r - 1].pts) t.push(e);
                else for (var i = r - 1; i >= 0; i--) if (e.pts < t[i].pts) {
                    t.splice(i, 0, e);
                    break
                }
            } else t.push(e)
        }
        function q(t) {
            for (var e = t.byteLength,
            r = [], i = 1; i < e - 2;) 0 === t[i] && 0 === t[i + 1] && 3 === t[i + 2] ? (r.push(i + 2), i += 2) : i++;
            if (0 === r.length) return t;
            var a = e - r.length,
            n = new Uint8Array(a),
            s = 0;
            for (i = 0; i < a; s++, i++) s === r[0] && (s++, r.shift()),
            n[i] = t[s];
            return n
        }
        G.minProbeByteLength = 188;
        var X = G;
        function z(t, e) {
            return (z = Object.setPrototypeOf ||
            function(t, e) {
                return t.__proto__ = e,
                t
            })(t, e)
        }
        var Q = function(t) {
            var e, r;
            function i() {
                return t.apply(this, arguments) || this
            }
            r = t,
            (e = i).prototype = Object.create(r.prototype),
            e.prototype.constructor = e,
            z(e, r);
            var a = i.prototype;
            return a.resetInitSegment = function(e, r, i) {
                t.prototype.resetInitSegment.call(this, e, r, i),
                this._audioTrack = {
                    container: "audio/mpeg",
                    type: "audio",
                    id: 0,
                    pid: -1,
                    sequenceNumber: 0,
                    isAAC: !1,
                    samples: [],
                    manifestCodec: e,
                    duration: i,
                    inputTimeScale: 9e4,
                    dropped: 0
                }
            },
            i.probe = function(t) {
                if (!t) return ! 1;
                for (var e = (o.b(t, 0) || []).length, r = t.length; e < r; e++) if (F(t, e)) return f.b.log("MPEG Audio sync word found !"),
                !0;
                return ! 1
            },
            a.canParse = function(t, e) {
                return function(t, e) {
                    return P(t, e) && 4 <= t.length - e
                } (t, e)
            },
            a.appendFrame = function(t, e, r) {
                if (null !== this.initPTS) return O(t, e, r, this.initPTS, this.frameIndex)
            },
            i
        } (c);
        Q.minProbeByteLength = 4;
        var $ = Q,
        J = r(15),
        Z = r(4),
        tt = function() {
            function t() {
                this.emitInitSegment = !1,
                this.audioCodec = void 0,
                this.videoCodec = void 0,
                this.initData = void 0,
                this.initPTS = void 0,
                this.initTracks = void 0,
                this.lastEndDTS = null
            }
            var e = t.prototype;
            return e.destroy = function() {},
            e.resetTimeStamp = function(t) {
                this.initPTS = t,
                this.lastEndDTS = null
            },
            e.resetNextTimestamp = function() {
                this.lastEndDTS = null
            },
            e.resetInitSegment = function(t, e, r) {
                this.audioCodec = e,
                this.videoCodec = r,
                this.generateInitSegment(t),
                this.emitInitSegment = !0
            },
            e.generateInitSegment = function(t) {
                var e = this.audioCodec,
                r = this.videoCodec;
                if (!t || !t.byteLength) return this.initTracks = void 0,
                void(this.initData = void 0);
                var i = this.initData = Object(l.f)(t);
                e || (e = rt(i.audio, Z.a.AUDIO)),
                r || (r = rt(i.video, Z.a.VIDEO));
                var a = {};
                i.audio && i.video ? a.audiovideo = {
                    container: "video/mp4",
                    codec: e + "," + r,
                    initSegment: t,
                    id: "main"
                }: i.audio ? a.audio = {
                    container: "audio/mp4",
                    codec: e,
                    initSegment: t,
                    id: "audio"
                }: i.video ? a.video = {
                    container: "video/mp4",
                    codec: r,
                    initSegment: t,
                    id: "main"
                }: f.b.warn("[passthrough-remuxer.ts]: initSegment does not contain moov or trak boxes."),
                this.initTracks = a
            },
            e.remux = function(t, e, r, i, a) {
                var n = this.initPTS,
                o = this.lastEndDTS,
                u = {
                    audio: void 0,
                    video: void 0,
                    text: i,
                    id3: r,
                    initSegment: void 0
                };
                Object(s.a)(o) || (o = this.lastEndDTS = a || 0);
                var h = e.samples;
                if (!h || !h.length) return u;
                var d = {
                    initPTS: void 0,
                    timescale: 1
                },
                c = this.initData;
                if (c && c.length || (this.generateInitSegment(h), c = this.initData), !c || !c.length) return f.b.warn("[passthrough-remuxer.ts]: Failed to generate initSegment."),
                u;
                this.emitInitSegment && (d.tracks = this.initTracks, this.emitInitSegment = !1),
                Object(s.a)(n) || (this.initPTS = d.initPTS = n = et(c, h, o));
                var g = Object(l.c)(h, c),
                v = o,
                p = g + v;
                Object(l.e)(c, h, n),
                g > 0 ? this.lastEndDTS = p: (f.b.warn("Duration parsed from mp4 should be greater than zero"), this.resetNextTimestamp());
                var m = !!c.audio,
                y = !!c.video,
                T = "";
                m && (T += "audio"),
                y && (T += "video");
                var E = {
                    data1: h,
                    startPTS: v,
                    startDTS: v,
                    endPTS: p,
                    endDTS: p,
                    type: T,
                    hasAudio: m,
                    hasVideo: y,
                    nb: 1,
                    dropped: 0
                };
                return u.audio = "audio" === E.type ? E: void 0,
                u.video = "audio" !== E.type ? E: void 0,
                u.text = i,
                u.id3 = r,
                u.initSegment = d,
                u
            },
            t
        } (),
        et = function(t, e, r) {
            return Object(l.d)(t, e) - r
        };
        function rt(t, e) {
            var r = null == t ? void 0 : t.codec;
            return r && r.length > 4 ? r: "hvc1" === r ? "hvc1.1.c.L120.90": "av01" === r ? "av01.0.04M.08": "avc1" === r || e === Z.a.VIDEO ? "avc1.42e01e": "mp4a.40.5"
        }
        var it, at = tt,
        nt = r(12);
        try {
            it = self.performance.now.bind(self.performance)
        } catch(t) {
            f.b.debug("Unable to use Performance API on this environment"),
            it = self.Date.now
        }
        var st = [{
            demux: X,
            remux: J.a
        },
        {
            demux: D,
            remux: at
        },
        {
            demux: A,
            remux: J.a
        },
        {
            demux: $,
            remux: J.a
        }],
        ot = 1024;
        st.forEach((function(t) {
            var e = t.demux;
            ot = Math.max(ot, e.minProbeByteLength)
        }));
        var lt = function() {
            function t(t, e, r, i) {
                this.observer = void 0,
                this.typeSupported = void 0,
                this.config = void 0,
                this.vendor = void 0,
                this.demuxer = void 0,
                this.remuxer = void 0,
                this.decrypter = void 0,
                this.probe = void 0,
                this.decryptionPromise = null,
                this.transmuxConfig = void 0,
                this.currentTransmuxState = void 0,
                this.cache = new nt.a,
                this.observer = t,
                this.typeSupported = e,
                this.config = r,
                this.vendor = i
            }
            var e = t.prototype;
            return e.configure = function(t) {
                this.transmuxConfig = t,
                this.decrypter && this.decrypter.reset()
            },
            e.push = function(t, e, r, i) {
                var a = this,
                n = r.transmuxing;
                n.executeStart = it();
                var s = new Uint8Array(t),
                o = this.cache,
                u = this.config,
                h = this.currentTransmuxState,
                d = this.transmuxConfig;
                i && (this.currentTransmuxState = i);
                var c = function(t, e) {
                    var r = null;
                    t.byteLength > 0 && null != e && null != e.key && null !== e.iv && null != e.method && (r = e);
                    return r
                } (s, e);
                if (c && "AES-128" === c.method) {
                    var f = this.getDecrypter();
                    if (!u.enableSoftwareAES) return this.decryptionPromise = f.webCryptoDecrypt(s, c.key.buffer, c.iv.buffer).then((function(t) {
                        var e = a.push(t, null, r);
                        return a.decryptionPromise = null,
                        e
                    })),
                    this.decryptionPromise;
                    var g = f.softwareDecrypt(s, c.key.buffer, c.iv.buffer);
                    if (!g) return n.executeEnd = it(),
                    ut(r);
                    s = new Uint8Array(g)
                }
                var v = i || h,
                p = v.contiguous,
                m = v.discontinuity,
                y = v.trackSwitch,
                T = v.accurateTimeOffset,
                E = v.timeOffset,
                b = d.audioCodec,
                S = d.videoCodec,
                L = d.defaultInitPts,
                A = d.duration,
                R = d.initSegmentData;
                if ((m || y) && this.resetInitSegment(R, b, S, A), m && this.resetInitialTimestamp(L), p || this.resetContiguity(), this.needsProbing(s, m, y)) {
                    if (o.dataLength) {
                        var D = o.flush();
                        s = Object(l.a)(D, s)
                    }
                    this.configureTransmuxer(s, d)
                }
                var k = this.transmux(s, c, E, T, r),
                _ = this.currentTransmuxState;
                return _.contiguous = !0,
                _.discontinuity = !1,
                _.trackSwitch = !1,
                n.executeEnd = it(),
                k
            },
            e.flush = function(t) {
                var e = this,
                r = t.transmuxing;
                r.executeStart = it();
                var n = this.decrypter,
                s = this.cache,
                o = this.currentTransmuxState,
                l = this.decryptionPromise;
                if (l) return l.then((function() {
                    return e.flush(t)
                }));
                var u = [],
                h = o.timeOffset;
                if (n) {
                    var d = n.flush();
                    d && u.push(this.push(d, null, t))
                }
                var c = s.dataLength;
                s.reset();
                var f = this.demuxer,
                g = this.remuxer;
                if (!f || !g) return c >= ot && this.observer.emit(i.a.ERROR, i.a.ERROR, {
                    type: a.b.MEDIA_ERROR,
                    details: a.a.FRAG_PARSING_ERROR,
                    fatal: !0,
                    reason: "no demux matching with content found"
                }),
                r.executeEnd = it(),
                [ut(t)];
                var v = f.flush(h);
                return ht(v) ? v.then((function(r) {
                    return e.flushRemux(u, r, t),
                    u
                })) : (this.flushRemux(u, v, t), u)
            },
            e.flushRemux = function(t, e, r) {
                var i = e.audioTrack,
                a = e.avcTrack,
                n = e.id3Track,
                s = e.textTrack,
                o = this.currentTransmuxState,
                l = o.accurateTimeOffset,
                u = o.timeOffset;
                f.b.log("[transmuxer.ts]: Flushed fragment " + r.sn + (r.part > -1 ? " p: " + r.part: "") + " of level " + r.level);
                var h = this.remuxer.remux(i, a, n, s, u, l, !0);
                t.push({
                    remuxResult: h,
                    chunkMeta: r
                }),
                r.transmuxing.executeEnd = it()
            },
            e.resetInitialTimestamp = function(t) {
                var e = this.demuxer,
                r = this.remuxer;
                e && r && (e.resetTimeStamp(t), r.resetTimeStamp(t))
            },
            e.resetContiguity = function() {
                var t = this.demuxer,
                e = this.remuxer;
                t && e && (t.resetContiguity(), e.resetNextTimestamp())
            },
            e.resetInitSegment = function(t, e, r, i) {
                var a = this.demuxer,
                n = this.remuxer;
                a && n && (a.resetInitSegment(e, r, i), n.resetInitSegment(t, e, r))
            },
            e.destroy = function() {
                this.demuxer && (this.demuxer.destroy(), this.demuxer = void 0),
                this.remuxer && (this.remuxer.destroy(), this.remuxer = void 0)
            },
            e.transmux = function(t, e, r, i, a) {
                return e && "SAMPLE-AES" === e.method ? this.transmuxSampleAes(t, e, r, i, a) : this.transmuxUnencrypted(t, r, i, a)
            },
            e.transmuxUnencrypted = function(t, e, r, i) {
                var a = this.demuxer.demux(t, e, !1, !this.config.progressive),
                n = a.audioTrack,
                s = a.avcTrack,
                o = a.id3Track,
                l = a.textTrack;
                return {
                    remuxResult: this.remuxer.remux(n, s, o, l, e, r, !1),
                    chunkMeta: i
                }
            },
            e.transmuxSampleAes = function(t, e, r, i, a) {
                var n = this;
                return this.demuxer.demuxSampleAes(t, e, r).then((function(t) {
                    return {
                        remuxResult: n.remuxer.remux(t.audioTrack, t.avcTrack, t.id3Track, t.textTrack, r, i, !1),
                        chunkMeta: a
                    }
                }))
            },
            e.configureTransmuxer = function(t, e) {
                for (var r, i = this.config,
                a = this.observer,
                n = this.typeSupported,
                s = this.vendor,
                o = e.audioCodec,
                l = e.defaultInitPts,
                u = e.duration,
                h = e.initSegmentData,
                d = e.videoCodec,
                c = 0,
                g = st.length; c < g; c++) if (st[c].demux.probe(t)) {
                    r = st[c];
                    break
                }
                r || (f.b.warn("Failed to find demuxer by probing frag, treating as mp4 passthrough"), r = {
                    demux: D,
                    remux: at
                });
                var v = this.demuxer,
                p = this.remuxer,
                m = r.remux,
                y = r.demux;
                p && p instanceof m || (this.remuxer = new m(a, i, n, s)),
                v && v instanceof y || (this.demuxer = new y(a, i, n), this.probe = y.probe),
                this.resetInitSegment(h, o, d, u),
                this.resetInitialTimestamp(l)
            },
            e.needsProbing = function(t, e, r) {
                return ! this.demuxer || !this.remuxer || e || r
            },
            e.getDecrypter = function() {
                var t = this.decrypter;
                return t || (t = this.decrypter = new n.a(this.observer, this.config)),
                t
            },
            t
        } ();
        var ut = function(t) {
            return {
                remuxResult: {},
                chunkMeta: t
            }
        };
        function ht(t) {
            return "then" in t && t.then instanceof Function
        }
        var dt = function(t, e, r, i, a) {
            this.audioCodec = void 0,
            this.videoCodec = void 0,
            this.initSegmentData = void 0,
            this.duration = void 0,
            this.defaultInitPts = void 0,
            this.audioCodec = t,
            this.videoCodec = e,
            this.initSegmentData = r,
            this.duration = i,
            this.defaultInitPts = a
        },
        ct = function(t, e, r, i, a) {
            this.discontinuity = void 0,
            this.contiguous = void 0,
            this.accurateTimeOffset = void 0,
            this.trackSwitch = void 0,
            this.timeOffset = void 0,
            this.discontinuity = t,
            this.contiguous = e,
            this.accurateTimeOffset = r,
            this.trackSwitch = i,
            this.timeOffset = a
        }
    },
    12 : function(t, e, r) {
        "use strict";
        r.d(e, "a", (function() {
            return i
        }));
        var i = function() {
            function t() {
                this.chunks = [],
                this.dataLength = 0
            }
            var e = t.prototype;
            return e.push = function(t) {
                this.chunks.push(t),
                this.dataLength += t.length
            },
            e.flush = function() {
                var t, e = this.chunks,
                r = this.dataLength;
                return e.length ? (t = 1 === e.length ? e[0] : function(t, e) {
                    for (var r = new Uint8Array(e), i = 0, a = 0; a < t.length; a++) {
                        var n = t[a];
                        r.set(n, i),
                        i += n.length
                    }
                    return r
                } (e, r), this.reset(), t) : new Uint8Array(0)
            },
            e.reset = function() {
                this.chunks.length = 0,
                this.dataLength = 0
            },
            t
        } ()
    },
    4 : function(t, e, r) {
        "use strict";
        r.d(e, "a", (function() {
            return i
        })),
        r.d(e, "b", (function() {
            return g
        })),
        r.d(e, "c", (function() {
            return v
        }));
        var i, a = r(3),
        n = r(10),
        s = r(1),
        o = r(16),
        l = r(11);
        function u(t, e) {
            t.prototype = Object.create(e.prototype),
            t.prototype.constructor = t,
            h(t, e)
        }
        function h(t, e) {
            return (h = Object.setPrototypeOf ||
            function(t, e) {
                return t.__proto__ = e,
                t
            })(t, e)
        }
        function d(t, e) {
            for (var r = 0; r < e.length; r++) {
                var i = e[r];
                i.enumerable = i.enumerable || !1,
                i.configurable = !0,
                "value" in i && (i.writable = !0),
                Object.defineProperty(t, i.key, i)
            }
        }
        function c(t, e, r) {
            return e && d(t.prototype, e),
            r && d(t, r),
            t
        } !
        function(t) {
            t.AUDIO = "audio",
            t.VIDEO = "video",
            t.AUDIOVIDEO = "audiovideo"
        } (i || (i = {}));
        var f = function() {
            function t(t) {
                var e;
                this._byteRange = null,
                this._url = null,
                this.baseurl = void 0,
                this.relurl = void 0,
                this.elementaryStreams = ((e = {})[i.AUDIO] = null, e[i.VIDEO] = null, e[i.AUDIOVIDEO] = null, e),
                this.baseurl = t
            }
            return t.prototype.setByteRange = function(t, e) {
                var r = t.split("@", 2),
                i = [];
                1 === r.length ? i[0] = e ? e.byteRangeEndOffset: 0 : i[0] = parseInt(r[1]),
                i[1] = parseInt(r[0]) + i[0],
                this._byteRange = i
            },
            c(t, [{
                key: "byteRange",
                get: function() {
                    return this._byteRange ? this._byteRange: []
                }
            },
            {
                key: "byteRangeStartOffset",
                get: function() {
                    return this.byteRange[0]
                }
            },
            {
                key: "byteRangeEndOffset",
                get: function() {
                    return this.byteRange[1]
                }
            },
            {
                key: "url",
                get: function() {
                    return ! this._url && this.baseurl && this.relurl && (this._url = Object(n.buildAbsoluteURL)(this.baseurl, this.relurl, {
                        alwaysNormalize: !0
                    })),
                    this._url || ""
                },
                set: function(t) {
                    this._url = t
                }
            }]),
            t
        } (),
        g = function(t) {
            function e(e, r) {
                var i;
                return (i = t.call(this, r) || this)._decryptdata = null,
                i.rawProgramDateTime = null,
                i.programDateTime = null,
                i.tagList = [],
                i.duration = 0,
                i.sn = 0,
                i.levelkey = void 0,
                i.type = void 0,
                i.loader = null,
                i.level = -1,
                i.cc = 0,
                i.startPTS = void 0,
                i.endPTS = void 0,
                i.appendedPTS = void 0,
                i.startDTS = void 0,
                i.endDTS = void 0,
                i.start = 0,
                i.deltaPTS = void 0,
                i.maxStartPTS = void 0,
                i.minEndPTS = void 0,
                i.stats = new l.a,
                i.urlId = 0,
                i.data = void 0,
                i.bitrateTest = !1,
                i.title = null,
                i.type = e,
                i
            }
            u(e, t);
            var r = e.prototype;
            return r.createInitializationVector = function(t) {
                for (var e = new Uint8Array(16), r = 12; r < 16; r++) e[r] = t >> 8 * (15 - r) & 255;
                return e
            },
            r.setDecryptDataFromLevelKey = function(t, e) {
                var r = t;
                return "AES-128" === (null == t ? void 0 : t.method) && t.uri && !t.iv && ((r = o.a.fromURI(t.uri)).method = t.method, r.iv = this.createInitializationVector(e), r.keyFormat = "identity"),
                r
            },
            r.setElementaryStreamInfo = function(t, e, r, i, a, n) {
                void 0 === n && (n = !1);
                var s = this.elementaryStreams,
                o = s[t];
                o ? (o.startPTS = Math.min(o.startPTS, e), o.endPTS = Math.max(o.endPTS, r), o.startDTS = Math.min(o.startDTS, i), o.endDTS = Math.max(o.endDTS, a)) : s[t] = {
                    startPTS: e,
                    endPTS: r,
                    startDTS: i,
                    endDTS: a,
                    partial: n
                }
            },
            r.clearElementaryStreamInfo = function() {
                var t = this.elementaryStreams;
                t[i.AUDIO] = null,
                t[i.VIDEO] = null,
                t[i.AUDIOVIDEO] = null
            },
            c(e, [{
                key: "decryptdata",
                get: function() {
                    if (!this.levelkey && !this._decryptdata) return null;
                    if (!this._decryptdata && this.levelkey) {
                        var t = this.sn;
                        "number" != typeof t && (this.levelkey && "AES-128" === this.levelkey.method && !this.levelkey.iv && s.b.warn('missing IV for initialization segment with method="' + this.levelkey.method + '" - compliance issue'), t = 0),
                        this._decryptdata = this.setDecryptDataFromLevelKey(this.levelkey, t)
                    }
                    return this._decryptdata
                }
            },
            {
                key: "end",
                get: function() {
                    return this.start + this.duration
                }
            },
            {
                key: "endProgramDateTime",
                get: function() {
                    if (null === this.programDateTime) return null;
                    if (!Object(a.a)(this.programDateTime)) return null;
                    var t = Object(a.a)(this.duration) ? this.duration: 0;
                    return this.programDateTime + 1e3 * t
                }
            },
            {
                key: "encrypted",
                get: function() {
                    var t;
                    return ! (null === (t = this.decryptdata) || void 0 === t || !t.keyFormat || !this.decryptdata.uri)
                }
            }]),
            e
        } (f),
        v = function(t) {
            function e(e, r, i, a, n) {
                var s; (s = t.call(this, i) || this).fragOffset = 0,
                s.duration = 0,
                s.gap = !1,
                s.independent = !1,
                s.relurl = void 0,
                s.fragment = void 0,
                s.index = void 0,
                s.stats = new l.a,
                s.duration = e.decimalFloatingPoint("DURATION"),
                s.gap = e.bool("GAP"),
                s.independent = e.bool("INDEPENDENT"),
                s.relurl = e.enumeratedString("URI"),
                s.fragment = r,
                s.index = a;
                var o = e.enumeratedString("BYTERANGE");
                return o && s.setByteRange(o, n),
                n && (s.fragOffset = n.fragOffset + n.duration),
                s
            }
            return u(e, t),
            c(e, [{
                key: "start",
                get: function() {
                    return this.fragment.start + this.fragOffset
                }
            },
            {
                key: "end",
                get: function() {
                    return this.start + this.duration
                }
            },
            {
                key: "loaded",
                get: function() {
                    var t = this.elementaryStreams;
                    return !! (t.audio || t.video || t.audiovideo)
                }
            }]),
            e
        } (f)
    },
    11 : function(t, e, r) {
        "use strict";
        r.d(e, "a", (function() {
            return i
        }));
        var i = function() {
            this.aborted = !1,
            this.loaded = 0,
            this.retry = 0,
            this.total = 0,
            this.chunkCount = 0,
            this.bwEstimate = 0,
            this.loading = {
                start: 0,
                first: 0,
                end: 0
            },
            this.parsing = {
                start: 0,
                end: 0
            },
            this.buffering = {
                start: 0,
                first: 0,
                end: 0
            }
        }
    },
    16 : function(t, e, r) {
        "use strict";
        r.d(e, "a", (function() {
            return n
        }));
        var i = r(10);
        function a(t, e) {
            for (var r = 0; r < e.length; r++) {
                var i = e[r];
                i.enumerable = i.enumerable || !1,
                i.configurable = !0,
                "value" in i && (i.writable = !0),
                Object.defineProperty(t, i.key, i)
            }
        }
        var n = function() {
            function t(t, e) {
                this._uri = null,
                this.method = null,
                this.keyFormat = null,
                this.keyFormatVersions = null,
                this.keyID = null,
                this.key = null,
                this.iv = null,
                this._uri = e ? Object(i.buildAbsoluteURL)(t, e, {
                    alwaysNormalize: !0
                }) : t
            }
            var e, r, n;
            return t.fromURL = function(e, r) {
                return new t(e, r)
            },
            t.fromURI = function(e) {
                return new t(e)
            },
            e = t,
            (r = [{
                key: "uri",
                get: function() {
                    return this._uri
                }
            }]) && a(e.prototype, r),
            n && a(e, n),
            t
        } ()
    },
    10 : function(t, e, r) {
        var i, a, n, s, o;
        i = /^((?:[a-zA-Z0-9+\-.]+:)?)(\/\/[^\/?#]*)?((?:[^\/?#]*\/)*[^;?#]*)?(;[^?#]*)?(\?[^#]*)?(#[^]*)?$/,
        a = /^([^\/?#]*)([^]*)$/,
        n = /(?:\/|^)\.(?=\/)/g,
        s = /(?:\/|^)\.\.\/(?!\.\.\/)[^\/]*(?=\/)/g,
        o = {
            buildAbsoluteURL: function(t, e, r) {
                if (r = r || {},
                t = t.trim(), !(e = e.trim())) {
                    if (!r.alwaysNormalize) return t;
                    var i = o.parseURL(t);
                    if (!i) throw new Error("Error trying to parse base URL.");
                    return i.path = o.normalizePath(i.path),
                    o.buildURLFromParts(i)
                }
                var n = o.parseURL(e);
                if (!n) throw new Error("Error trying to parse relative URL.");
                if (n.scheme) return r.alwaysNormalize ? (n.path = o.normalizePath(n.path), o.buildURLFromParts(n)) : e;
                var s = o.parseURL(t);
                if (!s) throw new Error("Error trying to parse base URL.");
                if (!s.netLoc && s.path && "/" !== s.path[0]) {
                    var l = a.exec(s.path);
                    s.netLoc = l[1],
                    s.path = l[2]
                }
                s.netLoc && !s.path && (s.path = "/");
                var u = {
                    scheme: s.scheme,
                    netLoc: n.netLoc,
                    path: null,
                    params: n.params,
                    query: n.query,
                    fragment: n.fragment
                };
                if (!n.netLoc && (u.netLoc = s.netLoc, "/" !== n.path[0])) if (n.path) {
                    var h = s.path,
                    d = h.substring(0, h.lastIndexOf("/") + 1) + n.path;
                    u.path = o.normalizePath(d)
                } else u.path = s.path,
                n.params || (u.params = s.params, n.query || (u.query = s.query));
                return null === u.path && (u.path = r.alwaysNormalize ? o.normalizePath(n.path) : n.path),
                o.buildURLFromParts(u)
            },
            parseURL: function(t) {
                var e = i.exec(t);
                return e ? {
                    scheme: e[1] || "",
                    netLoc: e[2] || "",
                    path: e[3] || "",
                    params: e[4] || "",
                    query: e[5] || "",
                    fragment: e[6] || ""
                }: null
            },
            normalizePath: function(t) {
                for (t = t.split("").reverse().join("").replace(n, ""); t.length !== (t = t.replace(s, "")).length;);
                return t.split("").reverse().join("")
            },
            buildURLFromParts: function(t) {
                return t.scheme + t.netLoc + t.path + t.params + t.query + t.fragment
            }
        },
        t.exports = o
    },
    3 : function(t, e, r) {
        "use strict";
        r.d(e, "a", (function() {
            return i
        }));
        var i = Number.isFinite ||
        function(t) {
            return "number" == typeof t && isFinite(t)
        };
        Number.MAX_SAFE_INTEGER
    },
    15 : function(t, e, r) {
        "use strict";
        r.d(e, "a", (function() {
            return p
        })),
        r.d(e, "b", (function() {
            return m
        }));
        var i = r(3),
        a = function() {
            function t() {}
            return t.getSilentFrame = function(t, e) {
                switch (t) {
                case "mp4a.40.2":
                    if (1 === e) return new Uint8Array([0, 200, 0, 128, 35, 128]);
                    if (2 === e) return new Uint8Array([33, 0, 73, 144, 2, 25, 0, 35, 128]);
                    if (3 === e) return new Uint8Array([0, 200, 0, 128, 32, 132, 1, 38, 64, 8, 100, 0, 142]);
                    if (4 === e) return new Uint8Array([0, 200, 0, 128, 32, 132, 1, 38, 64, 8, 100, 0, 128, 44, 128, 8, 2, 56]);
                    if (5 === e) return new Uint8Array([0, 200, 0, 128, 32, 132, 1, 38, 64, 8, 100, 0, 130, 48, 4, 153, 0, 33, 144, 2, 56]);
                    if (6 === e) return new Uint8Array([0, 200, 0, 128, 32, 132, 1, 38, 64, 8, 100, 0, 130, 48, 4, 153, 0, 33, 144, 2, 0, 178, 0, 32, 8, 224]);
                    break;
                default:
                    if (1 === e) return new Uint8Array([1, 64, 34, 128, 163, 78, 230, 128, 186, 8, 0, 0, 0, 28, 6, 241, 193, 10, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 94]);
                    if (2 === e) return new Uint8Array([1, 64, 34, 128, 163, 94, 230, 128, 186, 8, 0, 0, 0, 0, 149, 0, 6, 241, 161, 10, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 94]);
                    if (3 === e) return new Uint8Array([1, 64, 34, 128, 163, 94, 230, 128, 186, 8, 0, 0, 0, 0, 149, 0, 6, 241, 161, 10, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 90, 94])
                }
            },
            t
        } (),
        n = Math.pow(2, 32) - 1,
        s = function() {
            function t() {}
            return t.init = function() {
                var e;
                for (e in t.types = {
                    avc1: [],
                    avcC: [],
                    btrt: [],
                    dinf: [],
                    dref: [],
                    esds: [],
                    ftyp: [],
                    hdlr: [],
                    mdat: [],
                    mdhd: [],
                    mdia: [],
                    mfhd: [],
                    minf: [],
                    moof: [],
                    moov: [],
                    mp4a: [],
                    ".mp3": [],
                    mvex: [],
                    mvhd: [],
                    pasp: [],
                    sdtp: [],
                    stbl: [],
                    stco: [],
                    stsc: [],
                    stsd: [],
                    stsz: [],
                    stts: [],
                    tfdt: [],
                    tfhd: [],
                    traf: [],
                    trak: [],
                    trun: [],
                    trex: [],
                    tkhd: [],
                    vmhd: [],
                    smhd: []
                },
                t.types) t.types.hasOwnProperty(e) && (t.types[e] = [e.charCodeAt(0), e.charCodeAt(1), e.charCodeAt(2), e.charCodeAt(3)]);
                var r = new Uint8Array([0, 0, 0, 0, 0, 0, 0, 0, 118, 105, 100, 101, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 86, 105, 100, 101, 111, 72, 97, 110, 100, 108, 101, 114, 0]),
                i = new Uint8Array([0, 0, 0, 0, 0, 0, 0, 0, 115, 111, 117, 110, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 83, 111, 117, 110, 100, 72, 97, 110, 100, 108, 101, 114, 0]);
                t.HDLR_TYPES = {
                    video: r,
                    audio: i
                };
                var a = new Uint8Array([0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 12, 117, 114, 108, 32, 0, 0, 0, 1]),
                n = new Uint8Array([0, 0, 0, 0, 0, 0, 0, 0]);
                t.STTS = t.STSC = t.STCO = n,
                t.STSZ = new Uint8Array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]),
                t.VMHD = new Uint8Array([0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0]),
                t.SMHD = new Uint8Array([0, 0, 0, 0, 0, 0, 0, 0]),
                t.STSD = new Uint8Array([0, 0, 0, 0, 0, 0, 0, 1]);
                var s = new Uint8Array([105, 115, 111, 109]),
                o = new Uint8Array([97, 118, 99, 49]),
                l = new Uint8Array([0, 0, 0, 1]);
                t.FTYP = t.box(t.types.ftyp, s, l, s, o),
                t.DINF = t.box(t.types.dinf, t.box(t.types.dref, a))
            },
            t.box = function(t) {
                for (var e = 8,
                r = arguments.length,
                i = new Array(r > 1 ? r - 1 : 0), a = 1; a < r; a++) i[a - 1] = arguments[a];
                for (var n = i.length,
                s = n; n--;) e += i[n].byteLength;
                var o = new Uint8Array(e);
                for (o[0] = e >> 24 & 255, o[1] = e >> 16 & 255, o[2] = e >> 8 & 255, o[3] = 255 & e, o.set(t, 4), n = 0, e = 8; n < s; n++) o.set(i[n], e),
                e += i[n].byteLength;
                return o
            },
            t.hdlr = function(e) {
                return t.box(t.types.hdlr, t.HDLR_TYPES[e])
            },
            t.mdat = function(e) {
                return t.box(t.types.mdat, e)
            },
            t.mdhd = function(e, r) {
                r *= e;
                var i = Math.floor(r / (n + 1)),
                a = Math.floor(r % (n + 1));
                return t.box(t.types.mdhd, new Uint8Array([1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 3, e >> 24 & 255, e >> 16 & 255, e >> 8 & 255, 255 & e, i >> 24, i >> 16 & 255, i >> 8 & 255, 255 & i, a >> 24, a >> 16 & 255, a >> 8 & 255, 255 & a, 85, 196, 0, 0]))
            },
            t.mdia = function(e) {
                return t.box(t.types.mdia, t.mdhd(e.timescale, e.duration), t.hdlr(e.type), t.minf(e))
            },
            t.mfhd = function(e) {
                return t.box(t.types.mfhd, new Uint8Array([0, 0, 0, 0, e >> 24, e >> 16 & 255, e >> 8 & 255, 255 & e]))
            },
            t.minf = function(e) {
                return "audio" === e.type ? t.box(t.types.minf, t.box(t.types.smhd, t.SMHD), t.DINF, t.stbl(e)) : t.box(t.types.minf, t.box(t.types.vmhd, t.VMHD), t.DINF, t.stbl(e))
            },
            t.moof = function(e, r, i) {
                return t.box(t.types.moof, t.mfhd(e), t.traf(i, r))
            },
            t.moov = function(e) {
                for (var r = e.length,
                i = []; r--;) i[r] = t.trak(e[r]);
                return t.box.apply(null, [t.types.moov, t.mvhd(e[0].timescale, e[0].duration)].concat(i).concat(t.mvex(e)))
            },
            t.mvex = function(e) {
                for (var r = e.length,
                i = []; r--;) i[r] = t.trex(e[r]);
                return t.box.apply(null, [t.types.mvex].concat(i))
            },
            t.mvhd = function(e, r) {
                r *= e;
                var i = Math.floor(r / (n + 1)),
                a = Math.floor(r % (n + 1)),
                s = new Uint8Array([1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 3, e >> 24 & 255, e >> 16 & 255, e >> 8 & 255, 255 & e, i >> 24, i >> 16 & 255, i >> 8 & 255, 255 & i, a >> 24, a >> 16 & 255, a >> 8 & 255, 255 & a, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 64, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 255, 255, 255, 255]);
                return t.box(t.types.mvhd, s)
            },
            t.sdtp = function(e) {
                var r, i, a = e.samples || [],
                n = new Uint8Array(4 + a.length);
                for (r = 0; r < a.length; r++) i = a[r].flags,
                n[r + 4] = i.dependsOn << 4 | i.isDependedOn << 2 | i.hasRedundancy;
                return t.box(t.types.sdtp, n)
            },
            t.stbl = function(e) {
                return t.box(t.types.stbl, t.stsd(e), t.box(t.types.stts, t.STTS), t.box(t.types.stsc, t.STSC), t.box(t.types.stsz, t.STSZ), t.box(t.types.stco, t.STCO))
            },
            t.avc1 = function(e) {
                var r, i, a, n = [],
                s = [];
                for (r = 0; r < e.sps.length; r++) a = (i = e.sps[r]).byteLength,
                n.push(a >>> 8 & 255),
                n.push(255 & a),
                n = n.concat(Array.prototype.slice.call(i));
                for (r = 0; r < e.pps.length; r++) a = (i = e.pps[r]).byteLength,
                s.push(a >>> 8 & 255),
                s.push(255 & a),
                s = s.concat(Array.prototype.slice.call(i));
                var o = t.box(t.types.avcC, new Uint8Array([1, n[3], n[4], n[5], 255, 224 | e.sps.length].concat(n).concat([e.pps.length]).concat(s))),
                l = e.width,
                u = e.height,
                h = e.pixelRatio[0],
                d = e.pixelRatio[1];
                return t.box(t.types.avc1, new Uint8Array([0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, l >> 8 & 255, 255 & l, u >> 8 & 255, 255 & u, 0, 72, 0, 0, 0, 72, 0, 0, 0, 0, 0, 0, 0, 1, 18, 100, 97, 105, 108, 121, 109, 111, 116, 105, 111, 110, 47, 104, 108, 115, 46, 106, 115, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 24, 17, 17]), o, t.box(t.types.btrt, new Uint8Array([0, 28, 156, 128, 0, 45, 198, 192, 0, 45, 198, 192])), t.box(t.types.pasp, new Uint8Array([h >> 24, h >> 16 & 255, h >> 8 & 255, 255 & h, d >> 24, d >> 16 & 255, d >> 8 & 255, 255 & d])))
            },
            t.esds = function(t) {
                var e = t.config.length;
                return new Uint8Array([0, 0, 0, 0, 3, 23 + e, 0, 1, 0, 4, 15 + e, 64, 21, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5].concat([e]).concat(t.config).concat([6, 1, 2]))
            },
            t.mp4a = function(e) {
                var r = e.samplerate;
                return t.box(t.types.mp4a, new Uint8Array([0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, e.channelCount, 0, 16, 0, 0, 0, 0, r >> 8 & 255, 255 & r, 0, 0]), t.box(t.types.esds, t.esds(e)))
            },
            t.mp3 = function(e) {
                var r = e.samplerate;
                return t.box(t.types[".mp3"], new Uint8Array([0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, e.channelCount, 0, 16, 0, 0, 0, 0, r >> 8 & 255, 255 & r, 0, 0]))
            },
            t.stsd = function(e) {
                return "audio" === e.type ? e.isAAC || "mp3" !== e.codec ? t.box(t.types.stsd, t.STSD, t.mp4a(e)) : t.box(t.types.stsd, t.STSD, t.mp3(e)) : t.box(t.types.stsd, t.STSD, t.avc1(e))
            },
            t.tkhd = function(e) {
                var r = e.id,
                i = e.duration * e.timescale,
                a = e.width,
                s = e.height,
                o = Math.floor(i / (n + 1)),
                l = Math.floor(i % (n + 1));
                return t.box(t.types.tkhd, new Uint8Array([1, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 3, r >> 24 & 255, r >> 16 & 255, r >> 8 & 255, 255 & r, 0, 0, 0, 0, o >> 24, o >> 16 & 255, o >> 8 & 255, 255 & o, l >> 24, l >> 16 & 255, l >> 8 & 255, 255 & l, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 64, 0, 0, 0, a >> 8 & 255, 255 & a, 0, 0, s >> 8 & 255, 255 & s, 0, 0]))
            },
            t.traf = function(e, r) {
                var i = t.sdtp(e),
                a = e.id,
                s = Math.floor(r / (n + 1)),
                o = Math.floor(r % (n + 1));
                return t.box(t.types.traf, t.box(t.types.tfhd, new Uint8Array([0, 0, 0, 0, a >> 24, a >> 16 & 255, a >> 8 & 255, 255 & a])), t.box(t.types.tfdt, new Uint8Array([1, 0, 0, 0, s >> 24, s >> 16 & 255, s >> 8 & 255, 255 & s, o >> 24, o >> 16 & 255, o >> 8 & 255, 255 & o])), t.trun(e, i.length + 16 + 20 + 8 + 16 + 8 + 8), i)
            },
            t.trak = function(e) {
                return e.duration = e.duration || 4294967295,
                t.box(t.types.trak, t.tkhd(e), t.mdia(e))
            },
            t.trex = function(e) {
                var r = e.id;
                return t.box(t.types.trex, new Uint8Array([0, 0, 0, 0, r >> 24, r >> 16 & 255, r >> 8 & 255, 255 & r, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1]))
            },
            t.trun = function(e, r) {
                var i, a, n, s, o, l, u = e.samples || [],
                h = u.length,
                d = 12 + 16 * h,
                c = new Uint8Array(d);
                for (r += 8 + d, c.set([0, 0, 15, 1, h >>> 24 & 255, h >>> 16 & 255, h >>> 8 & 255, 255 & h, r >>> 24 & 255, r >>> 16 & 255, r >>> 8 & 255, 255 & r], 0), i = 0; i < h; i++) n = (a = u[i]).duration,
                s = a.size,
                o = a.flags,
                l = a.cts,
                c.set([n >>> 24 & 255, n >>> 16 & 255, n >>> 8 & 255, 255 & n, s >>> 24 & 255, s >>> 16 & 255, s >>> 8 & 255, 255 & s, o.isLeading << 2 | o.dependsOn, o.isDependedOn << 6 | o.hasRedundancy << 4 | o.paddingValue << 1 | o.isNonSync, 61440 & o.degradPrio, 15 & o.degradPrio, l >>> 24 & 255, l >>> 16 & 255, l >>> 8 & 255, 255 & l], 12 + 16 * i);
                return t.box(t.types.trun, c)
            },
            t.initSegment = function(e) {
                t.types || t.init();
                var r = t.moov(e),
                i = new Uint8Array(t.FTYP.byteLength + r.byteLength);
                return i.set(t.FTYP),
                i.set(r, t.FTYP.byteLength),
                i
            },
            t
        } ();
        s.types = void 0,
        s.HDLR_TYPES = void 0,
        s.STTS = void 0,
        s.STSC = void 0,
        s.STCO = void 0,
        s.STSZ = void 0,
        s.VMHD = void 0,
        s.SMHD = void 0,
        s.STSD = void 0,
        s.FTYP = void 0,
        s.DINF = void 0;
        var o = s,
        l = r(0),
        u = r(2),
        h = r(1),
        d = r(7);
        function c() {
            return (c = Object.assign ||
            function(t) {
                for (var e = 1; e < arguments.length; e++) {
                    var r = arguments[e];
                    for (var i in r) Object.prototype.hasOwnProperty.call(r, i) && (t[i] = r[i])
                }
                return t
            }).apply(this, arguments)
        }
        var f = null,
        g = null,
        v = !1,
        p = function() {
            function t(t, e, r, i) {
                if (void 0 === i && (i = ""), this.observer = void 0, this.config = void 0, this.typeSupported = void 0, this.ISGenerated = !1, this._initPTS = void 0, this._initDTS = void 0, this.nextAvcDts = null, this.nextAudioPts = null, this.isAudioContiguous = !1, this.isVideoContiguous = !1, this.observer = t, this.config = e, this.typeSupported = r, this.ISGenerated = !1, null === f) {
                    var a = (navigator.userAgent || "").match(/Chrome\/(\d+)/i);
                    f = a ? parseInt(a[1]) : 0
                }
                if (null === g) {
                    var n = navigator.userAgent.match(/Safari\/(\d+)/i);
                    g = n ? parseInt(n[1]) : 0
                }
                v = !!f && f < 75 || !!g && g < 600
            }
            var e = t.prototype;
            return e.destroy = function() {},
            e.resetTimeStamp = function(t) {
                h.b.log("[mp4-remuxer]: initPTS & initDTS reset"),
                this._initPTS = this._initDTS = t
            },
            e.resetNextTimestamp = function() {
                h.b.log("[mp4-remuxer]: reset next timestamp"),
                this.isVideoContiguous = !1,
                this.isAudioContiguous = !1
            },
            e.resetInitSegment = function() {
                h.b.log("[mp4-remuxer]: ISGenerated flag reset"),
                this.ISGenerated = !1
            },
            e.getVideoStartPts = function(t) {
                var e = !1,
                r = t.reduce((function(t, r) {
                    var i = r.pts - t;
                    return i < -4294967296 ? (e = !0, m(t, r.pts)) : i > 0 ? t: r.pts
                }), t[0].pts);
                return e && h.b.debug("PTS rollover detected"),
                r
            },
            e.remux = function(t, e, r, i, a, n, s) {
                var o, l, u, d, c, f, g = a,
                v = a,
                p = t.pid > -1,
                y = e.pid > -1,
                T = e.samples.length,
                E = t.samples.length > 0,
                b = T > 1;
                if ((!p || E) && (!y || b) || this.ISGenerated || s) {
                    this.ISGenerated || (u = this.generateIS(t, e, a));
                    var S = this.isVideoContiguous,
                    L = -1;
                    if (b && (L = function(t) {
                        for (var e = 0; e < t.length; e++) if (t[e].key) return e;
                        return - 1
                    } (e.samples), !S && this.config.forceKeyFrameOnDiscontinuity)) if (f = !0, L > 0) {
                        h.b.warn("[mp4-remuxer]: Dropped " + L + " out of " + T + " video samples due to a missing keyframe");
                        var A = this.getVideoStartPts(e.samples);
                        e.samples = e.samples.slice(L),
                        e.dropped += L,
                        v += (e.samples[0].pts - A) / (e.timescale || 9e4)
                    } else - 1 === L && (h.b.warn("[mp4-remuxer]: No keyframe found out of " + T + " video samples"), f = !1);
                    if (this.ISGenerated) {
                        if (E && b) {
                            var R = this.getVideoStartPts(e.samples),
                            D = (m(t.samples[0].pts, R) - R) / e.inputTimeScale;
                            g += Math.max(0, D),
                            v += Math.max(0, -D)
                        }
                        if (E) {
                            if (t.samplerate || (h.b.warn("[mp4-remuxer]: regenerate InitSegment as audio detected"), u = this.generateIS(t, e, a)), l = this.remuxAudio(t, g, this.isAudioContiguous, n, b ? v: void 0), b) {
                                var k = l ? l.endPTS - l.startPTS: 0;
                                e.inputTimeScale || (h.b.warn("[mp4-remuxer]: regenerate InitSegment as video detected"), u = this.generateIS(t, e, a)),
                                o = this.remuxVideo(e, v, S, k)
                            }
                        } else b && (o = this.remuxVideo(e, v, S, 0));
                        o && (o.firstKeyFrame = L, o.independent = -1 !== L)
                    }
                }
                return this.ISGenerated && (r.samples.length && (c = this.remuxID3(r, a)), i.samples.length && (d = this.remuxText(i, a))),
                {
                    audio: l,
                    video: o,
                    initSegment: u,
                    independent: f,
                    text: d,
                    id3: c
                }
            },
            e.generateIS = function(t, e, r) {
                var a, n, s, l = t.samples,
                u = e.samples,
                h = this.typeSupported,
                d = {},
                c = !Object(i.a)(this._initPTS),
                f = "audio/mp4";
                if (c && (a = n = 1 / 0), t.config && l.length && (t.timescale = t.samplerate, t.isAAC || (h.mpeg ? (f = "audio/mpeg", t.codec = "") : h.mp3 && (t.codec = "mp3")), d.audio = {
                    id: "audio",
                    container: f,
                    codec: t.codec,
                    initSegment: !t.isAAC && h.mpeg ? new Uint8Array(0) : o.initSegment([t]),
                    metadata: {
                        channelCount: t.channelCount
                    }
                },
                c && (s = t.inputTimeScale, a = n = l[0].pts - Math.round(s * r))), e.sps && e.pps && u.length && (e.timescale = e.inputTimeScale, d.video = {
                    id: "main",
                    container: "video/mp4",
                    codec: e.codec,
                    initSegment: o.initSegment([e]),
                    metadata: {
                        width: e.width,
                        height: e.height
                    }
                },
                c)) {
                    s = e.inputTimeScale;
                    var g = this.getVideoStartPts(u),
                    v = Math.round(s * r);
                    n = Math.min(n, m(u[0].dts, g) - v),
                    a = Math.min(a, g - v)
                }
                if (Object.keys(d).length) return this.ISGenerated = !0,
                c && (this._initPTS = a, this._initDTS = n),
                {
                    tracks: d,
                    initPTS: a,
                    timescale: s
                }
            },
            e.remuxVideo = function(t, e, r, i) {
                var a, n, s, g = t.inputTimeScale,
                p = t.samples,
                T = [],
                E = p.length,
                b = this._initPTS,
                S = this.nextAvcDts,
                L = 8,
                A = Number.POSITIVE_INFINITY,
                R = Number.NEGATIVE_INFINITY,
                D = 0,
                k = !1;
                r && null !== S || (S = e * g - (p[0].pts - m(p[0].dts, p[0].pts)));
                for (var _ = 0; _ < E; _++) {
                    var I = p[_];
                    if (I.pts = m(I.pts - b, S), I.dts = m(I.dts - b, S), I.dts > I.pts) {
                        D = Math.max(Math.min(D, I.pts - I.dts), -18e3)
                    }
                    I.dts < p[_ > 0 ? _ - 1 : _].dts && (k = !0)
                }
                k && p.sort((function(t, e) {
                    var r = t.dts - e.dts,
                    i = t.pts - e.pts;
                    return r || i
                })),
                n = p[0].dts,
                s = p[p.length - 1].dts;
                var C = Math.round((s - n) / (E - 1));
                if (D < 0) {
                    if (D < -2 * C) {
                        h.b.warn("PTS < DTS detected in video samples, offsetting DTS from PTS by " + Object(d.b)( - C, !0) + " ms");
                        for (var w = D,
                        O = 0; O < E; O++) p[O].dts = w = Math.max(w, p[O].pts - C),
                        p[O].pts = Math.max(w, p[O].pts)
                    } else {
                        h.b.warn("PTS < DTS detected in video samples, shifting DTS by " + Object(d.b)(D, !0) + " ms to overcome this issue");
                        for (var x = 0; x < E; x++) p[x].dts = p[x].dts + D
                    }
                    n = p[0].dts
                }
                if (r) {
                    var P = n - S,
                    M = P > C;
                    if (M || P < -1) {
                        M ? h.b.warn("AVC: " + Object(d.b)(P, !0) + " ms (" + P + "dts) hole between fragments detected, filling it") : h.b.warn("AVC: " + Object(d.b)( - P, !0) + " ms (" + P + "dts) overlapping between fragments detected"),
                        n = S;
                        var F = p[0].pts - P;
                        p[0].dts = n,
                        p[0].pts = F,
                        h.b.log("Video: First PTS/DTS adjusted: " + Object(d.b)(F, !0) + "/" + Object(d.b)(n, !0) + ", delta: " + Object(d.b)(P, !0) + " ms")
                    }
                }
                v && (n = Math.max(0, n));
                for (var N = 0,
                U = 0,
                B = 0; B < E; B++) {
                    for (var G = p[B], K = G.units, j = K.length, H = 0, V = 0; V < j; V++) H += K[V].data.length;
                    U += H,
                    N += j,
                    G.length = H,
                    G.dts = Math.max(G.dts, n),
                    G.pts = Math.max(G.pts, G.dts, 0),
                    A = Math.min(G.pts, A),
                    R = Math.max(G.pts, R)
                }
                s = p[E - 1].dts;
                var W, Y = U + 4 * N + 8;
                try {
                    W = new Uint8Array(Y)
                } catch(t) {
                    return void this.observer.emit(l.a.ERROR, l.a.ERROR, {
                        type: u.b.MUX_ERROR,
                        details: u.a.REMUX_ALLOC_ERROR,
                        fatal: !1,
                        bytes: Y,
                        reason: "fail allocating video mdat " + Y
                    })
                }
                var q = new DataView(W.buffer);
                q.setUint32(0, Y),
                W.set(o.types.mdat, 4);
                for (var X = 0; X < E; X++) {
                    for (var z = p[X], Q = z.units, $ = 0, J = 0, Z = Q.length; J < Z; J++) {
                        var tt = Q[J],
                        et = tt.data,
                        rt = tt.data.byteLength;
                        q.setUint32(L, rt),
                        L += 4,
                        W.set(et, L),
                        L += rt,
                        $ += 4 + rt
                    }
                    if (X < E - 1) a = p[X + 1].dts - z.dts;
                    else {
                        var it = this.config,
                        at = z.dts - p[X > 0 ? X - 1 : X].dts;
                        if (it.stretchShortVideoTrack && null !== this.nextAudioPts) {
                            var nt = Math.floor(it.maxBufferHole * g),
                            st = (i ? A + i * g: this.nextAudioPts) - z.pts;
                            st > nt ? ((a = st - at) < 0 && (a = at), h.b.log("[mp4-remuxer]: It is approximately " + st / 90 + " ms to the next segment; using duration " + a / 90 + " ms for the last video frame.")) : a = at
                        } else a = at
                    }
                    var ot = Math.round(z.pts - z.dts);
                    T.push(new y(z.key, a, $, ot))
                }
                if (T.length && f && f < 70) {
                    var lt = T[0].flags;
                    lt.dependsOn = 2,
                    lt.isNonSync = 0
                }
                this.nextAvcDts = S = s + a,
                this.isVideoContiguous = !0;
                var ut = {
                    data1: o.moof(t.sequenceNumber++, n, c({},
                    t, {
                        samples: T
                    })),
                    data2: W,
                    startPTS: A / g,
                    endPTS: (R + a) / g,
                    startDTS: n / g,
                    endDTS: S / g,
                    type: "video",
                    hasAudio: !1,
                    hasVideo: !0,
                    nb: T.length,
                    dropped: t.dropped
                };
                return t.samples = [],
                t.dropped = 0,
                ut
            },
            e.remuxAudio = function(t, e, r, i, n) {
                var s, d = t.inputTimeScale,
                f = d / (t.samplerate ? t.samplerate: d),
                g = t.isAAC ? 1024 : 1152,
                v = g * f,
                p = this._initPTS,
                T = !t.isAAC && this.typeSupported.mpeg,
                E = [],
                b = t.samples,
                S = T ? 0 : 8,
                L = this.nextAudioPts || -1,
                A = e * d;
                if (this.isAudioContiguous = r = r || b.length && L > 0 && (i && Math.abs(A - L) < 9e3 || Math.abs(m(b[0].pts - p, A) - L) < 20 * v), b.forEach((function(t) {
                    t.pts = t.dts = m(t.pts - p, A)
                })), !r || L < 0) {
                    if (! (b = b.filter((function(t) {
                        return t.pts >= 0
                    }))).length) return;
                    L = 0 === n ? 0 : i ? Math.max(0, A) : b[0].pts
                }
                if (t.isAAC) for (var R = this.config.maxAudioFramesDrift,
                D = 0,
                k = L; D < b.length;) {
                    var _ = b[D],
                    I = _.pts,
                    C = I - k,
                    w = Math.abs(1e3 * C / d);
                    if (C <= -R * v && void 0 !== n) r || D > 0 ? (h.b.warn("[mp4-remuxer]: Dropping 1 audio frame @ " + (k / d).toFixed(3) + "s due to " + Math.round(w) + " ms overlap."), b.splice(D, 1)) : (h.b.warn("Audio frame @ " + (I / d).toFixed(3) + "s overlaps nextAudioPts by " + Math.round(1e3 * C / d) + " ms."), k = I + v, D++);
                    else if (C >= R * v && w < 1e4 && void 0 !== n) {
                        var O = Math.floor(C / v);
                        k = I - O * v,
                        h.b.warn("[mp4-remuxer]: Injecting " + O + " audio frame @ " + (k / d).toFixed(3) + "s due to " + Math.round(1e3 * C / d) + " ms gap.");
                        for (var x = 0; x < O; x++) {
                            var P = Math.max(k, 0); (s = a.getSilentFrame(t.manifestCodec || t.codec, t.channelCount)) || (h.b.log("[mp4-remuxer]: Unable to get silent frame for given audio codec; duplicating last frame instead."), s = _.unit.subarray()),
                            b.splice(D, 0, {
                                unit: s,
                                pts: P,
                                dts: P
                            }),
                            k += v,
                            D++
                        }
                        _.pts = _.dts = k,
                        k += v,
                        D++
                    } else _.pts = _.dts = k,
                    k += v,
                    D++
                }
                for (var M, F = null,
                N = null,
                U = 0,
                B = b.length; B--;) U += b[B].unit.byteLength;
                for (var G = 0,
                K = b.length; G < K; G++) {
                    var j = b[G],
                    H = j.unit,
                    V = j.pts;
                    if (null !== N) {
                        E[G - 1].duration = Math.round((V - N) / f)
                    } else {
                        var W = Math.round(1e3 * (V - L) / d),
                        Y = 0;
                        if (r && t.isAAC) {
                            if (W > 0 && W < 1e4) Y = Math.round((V - L) / v),
                            h.b.log("[mp4-remuxer]: " + W + " ms hole between AAC samples detected,filling it"),
                            Y > 0 && ((s = a.getSilentFrame(t.manifestCodec || t.codec, t.channelCount)) || (s = H.subarray()), U += Y * s.length);
                            else if (W < -12) {
                                h.b.log("[mp4-remuxer]: drop overlapping AAC sample, expected/parsed/delta:" + (L / d).toFixed(3) + "s/" + (V / d).toFixed(3) + "s/" + -W + "ms"),
                                U -= H.byteLength;
                                continue
                            }
                            V = L
                        }
                        if (F = V, !(U > 0)) return;
                        U += S;
                        try {
                            M = new Uint8Array(U)
                        } catch(t) {
                            return void this.observer.emit(l.a.ERROR, l.a.ERROR, {
                                type: u.b.MUX_ERROR,
                                details: u.a.REMUX_ALLOC_ERROR,
                                fatal: !1,
                                bytes: U,
                                reason: "fail allocating audio mdat " + U
                            })
                        }
                        T || (new DataView(M.buffer).setUint32(0, U), M.set(o.types.mdat, 4));
                        for (var q = 0; q < Y; q++)(s = a.getSilentFrame(t.manifestCodec || t.codec, t.channelCount)) || (h.b.log("[mp4-remuxer]: Unable to get silent frame for given audio codec; duplicating the current frame instead"), s = H.subarray()),
                        M.set(s, S),
                        S += s.byteLength,
                        E.push(new y(!0, 1024, s.byteLength, 0))
                    }
                    M.set(H, S);
                    var X = H.byteLength;
                    S += X,
                    E.push(new y(!0, g, X, 0)),
                    N = V
                }
                var z = E.length;
                if (z) {
                    var Q = E[E.length - 1];
                    this.nextAudioPts = L = N + f * Q.duration;
                    var $ = T ? new Uint8Array(0) : o.moof(t.sequenceNumber++, F / f, c({},
                    t, {
                        samples: E
                    }));
                    t.samples = [];
                    var J = F / d,
                    Z = L / d,
                    tt = {
                        data1: $,
                        data2: M,
                        startPTS: J,
                        endPTS: Z,
                        startDTS: J,
                        endDTS: Z,
                        type: "audio",
                        hasAudio: !0,
                        hasVideo: !1,
                        nb: z
                    };
                    return this.isAudioContiguous = !0,
                    tt
                }
            },
            e.remuxEmptyAudio = function(t, e, r, i) {
                var n = t.inputTimeScale,
                s = n / (t.samplerate ? t.samplerate: n),
                o = this.nextAudioPts,
                l = (null !== o ? o: i.startDTS * n) + this._initDTS,
                u = i.endDTS * n + this._initDTS,
                d = 1024 * s,
                c = Math.ceil((u - l) / d),
                f = a.getSilentFrame(t.manifestCodec || t.codec, t.channelCount);
                if (h.b.warn("[mp4-remuxer]: remux empty Audio"), f) {
                    for (var g = [], v = 0; v < c; v++) {
                        var p = l + v * d;
                        g.push({
                            unit: f,
                            pts: p,
                            dts: p
                        })
                    }
                    return t.samples = g,
                    this.remuxAudio(t, e, r, !1)
                }
                h.b.trace("[mp4-remuxer]: Unable to remuxEmptyAudio since we were unable to get a silent frame for given audio codec")
            },
            e.remuxID3 = function(t, e) {
                var r = t.samples.length;
                if (r) {
                    for (var i = t.inputTimeScale,
                    a = this._initPTS,
                    n = this._initDTS,
                    s = 0; s < r; s++) {
                        var o = t.samples[s];
                        o.pts = m(o.pts - a, e * i) / i,
                        o.dts = m(o.dts - n, e * i) / i
                    }
                    var l = t.samples;
                    return t.samples = [],
                    {
                        samples: l
                    }
                }
            },
            e.remuxText = function(t, e) {
                var r = t.samples.length;
                if (r) {
                    for (var i = t.inputTimeScale,
                    a = this._initPTS,
                    n = 0; n < r; n++) {
                        var s = t.samples[n];
                        s.pts = m(s.pts - a, e * i) / i
                    }
                    t.samples.sort((function(t, e) {
                        return t.pts - e.pts
                    }));
                    var o = t.samples;
                    return t.samples = [],
                    {
                        samples: o
                    }
                }
            },
            t
        } ();
        function m(t, e) {
            var r;
            if (null === e) return t;
            for (r = e < t ? -8589934592 : 8589934592; Math.abs(t - e) > 4294967296;) t += r;
            return t
        }
        var y = function(t, e, r, i) {
            this.size = void 0,
            this.duration = void 0,
            this.cts = void 0,
            this.flags = void 0,
            this.duration = e,
            this.size = r,
            this.cts = i,
            this.flags = new T(t)
        },
        T = function(t) {
            this.isLeading = 0,
            this.isDependedOn = 0,
            this.hasRedundancy = 0,
            this.degradPrio = 0,
            this.dependsOn = 1,
            this.isNonSync = 1,
            this.dependsOn = t ? 2 : 1,
            this.isNonSync = t ? 0 : 1
        }
    },
    7 : function(t, e, r) {
        "use strict";
        r.d(e, "c", (function() {
            return a
        })),
        r.d(e, "b", (function() {
            return n
        })),
        r.d(e, "a", (function() {
            return s
        }));
        function i(t, e, r, i) {
            void 0 === r && (r = 1),
            void 0 === i && (i = !1);
            var a = t * e * r;
            return i ? Math.round(a) : a
        }
        function a(t, e, r, a) {
            return void 0 === r && (r = 1),
            void 0 === a && (a = !1),
            i(t, e, 1 / r, a)
        }
        function n(t, e) {
            return void 0 === e && (e = !1),
            i(t, 1e3, 1 / 9e4, e)
        }
        function s(t, e) {
            return void 0 === e && (e = 1),
            i(t, 9e4, 1 / e)
        }
    },
    2 : function(t, e, r) {
        "use strict";
        var i, a;
        r.d(e, "b", (function() {
            return i
        })),
        r.d(e, "a", (function() {
            return a
        })),
        function(t) {
            t.NETWORK_ERROR = "networkError",
            t.MEDIA_ERROR = "mediaError",
            t.KEY_SYSTEM_ERROR = "keySystemError",
            t.MUX_ERROR = "muxError",
            t.OTHER_ERROR = "otherError"
        } (i || (i = {})),
        function(t) {
            t.KEY_SYSTEM_NO_KEYS = "keySystemNoKeys",
            t.KEY_SYSTEM_NO_ACCESS = "keySystemNoAccess",
            t.KEY_SYSTEM_NO_SESSION = "keySystemNoSession",
            t.KEY_SYSTEM_LICENSE_REQUEST_FAILED = "keySystemLicenseRequestFailed",
            t.KEY_SYSTEM_NO_INIT_DATA = "keySystemNoInitData",
            t.MANIFEST_LOAD_ERROR = "manifestLoadError",
            t.MANIFEST_LOAD_TIMEOUT = "manifestLoadTimeOut",
            t.MANIFEST_PARSING_ERROR = "manifestParsingError",
            t.MANIFEST_INCOMPATIBLE_CODECS_ERROR = "manifestIncompatibleCodecsError",
            t.LEVEL_EMPTY_ERROR = "levelEmptyError",
            t.LEVEL_LOAD_ERROR = "levelLoadError",
            t.LEVEL_LOAD_TIMEOUT = "levelLoadTimeOut",
            t.LEVEL_SWITCH_ERROR = "levelSwitchError",
            t.AUDIO_TRACK_LOAD_ERROR = "audioTrackLoadError",
            t.AUDIO_TRACK_LOAD_TIMEOUT = "audioTrackLoadTimeOut",
            t.SUBTITLE_LOAD_ERROR = "subtitleTrackLoadError",
            t.SUBTITLE_TRACK_LOAD_TIMEOUT = "subtitleTrackLoadTimeOut",
            t.FRAG_LOAD_ERROR = "fragLoadError",
            t.FRAG_LOAD_TIMEOUT = "fragLoadTimeOut",
            t.FRAG_DECRYPT_ERROR = "fragDecryptError",
            t.FRAG_PARSING_ERROR = "fragParsingError",
            t.REMUX_ALLOC_ERROR = "remuxAllocError",
            t.KEY_LOAD_ERROR = "keyLoadError",
            t.KEY_LOAD_TIMEOUT = "keyLoadTimeOut",
            t.BUFFER_ADD_CODEC_ERROR = "bufferAddCodecError",
            t.BUFFER_INCOMPATIBLE_CODECS_ERROR = "bufferIncompatibleCodecsError",
            t.BUFFER_APPEND_ERROR = "bufferAppendError",
            t.BUFFER_APPENDING_ERROR = "bufferAppendingError",
            t.BUFFER_STALLED_ERROR = "bufferStalledError",
            t.BUFFER_FULL_ERROR = "bufferFullError",
            t.BUFFER_SEEK_OVER_HOLE = "bufferSeekOverHole",
            t.BUFFER_NUDGE_ON_STALL = "bufferNudgeOnStall",
            t.INTERNAL_EXCEPTION = "internalException",
            t.INTERNAL_ABORTED = "aborted",
            t.UNKNOWN = "unknown"
        } (a || (a = {}))
    },
    8 : function(t, e, r) {
        "use strict";
        function i(t, e, r) {
            return Uint8Array.prototype.slice ? t.slice(e, r) : new Uint8Array(Array.prototype.slice.call(t, e, r))
        }
        r.d(e, "a", (function() {
            return i
        }))
    },
    5 : function(t, e, r) {
        "use strict";
        r.d(e, "b", (function() {
            return h
        })),
        r.d(e, "g", (function() {
            return d
        })),
        r.d(e, "f", (function() {
            return c
        })),
        r.d(e, "d", (function() {
            return f
        })),
        r.d(e, "c", (function() {
            return g
        })),
        r.d(e, "e", (function() {
            return p
        })),
        r.d(e, "h", (function() {
            return m
        })),
        r.d(e, "a", (function() {
            return y
        }));
        var i = r(8),
        a = r(4),
        n = Math.pow(2, 32) - 1,
        s = [].push;
        function o(t) {
            return String.fromCharCode.apply(null, t)
        }
        function l(t, e) {
            "data" in t && (e += t.start, t = t.data);
            var r = t[e] << 24 | t[e + 1] << 16 | t[e + 2] << 8 | t[e + 3];
            return r < 0 ? 4294967296 + r: r
        }
        function u(t, e, r) {
            "data" in t && (e += t.start, t = t.data),
            t[e] = r >> 24,
            t[e + 1] = r >> 16 & 255,
            t[e + 2] = r >> 8 & 255,
            t[e + 3] = 255 & r
        }
        function h(t, e) {
            var r, i, a, n = [];
            if (!e.length) return n;
            "data" in t ? (r = t.data, i = t.start, a = t.end) : (i = 0, a = (r = t).byteLength);
            for (var u = i; u < a;) {
                var d = l(r, u),
                c = d > 1 ? u + d: a;
                if (o(r.subarray(u + 4, u + 8)) === e[0]) if (1 === e.length) n.push({
                    data: r,
                    start: u + 8,
                    end: c
                });
                else {
                    var f = h({
                        data: r,
                        start: u + 8,
                        end: c
                    },
                    e.slice(1));
                    f.length && s.apply(n, f)
                }
                u = c
            }
            return n
        }
        function d(t) {
            var e = h(t, ["moov"])[0],
            r = e ? e.end: null,
            i = h(t, ["sidx"]);
            if (!i || !i[0]) return null;
            var a = [],
            n = i[0],
            s = n.data[0],
            o = 0 === s ? 8 : 16,
            u = l(n, o);
            o += 4;
            o += 0 === s ? 8 : 16,
            o += 2;
            var d = n.end + 0,
            c = function(t, e) {
                "data" in t && (e += t.start, t = t.data);
                var r = t[e] << 8 | t[e + 1];
                return r < 0 ? 65536 + r: r
            } (n, o);
            o += 2;
            for (var f = 0; f < c; f++) {
                var g = o,
                v = l(n, g);
                g += 4;
                var p = 2147483647 & v;
                if (1 === (2147483648 & v) >>> 31) return console.warn("SIDX has hierarchical references (not supported)"),
                null;
                var m = l(n, g);
                g += 4,
                a.push({
                    referenceSize: p,
                    subsegmentDuration: m,
                    info: {
                        duration: m / u,
                        start: d,
                        end: d + p - 1
                    }
                }),
                d += p,
                o = g += 4
            }
            return {
                earliestPresentationTime: 0,
                timescale: u,
                version: s,
                referencesCount: c,
                references: a,
                moovEndOffset: r
            }
        }
        function c(t) {
            for (var e = [], r = h(t, ["moov", "trak"]), i = 0; i < r.length; i++) {
                var n = r[i],
                s = h(n, ["tkhd"])[0];
                if (s) {
                    var u = s.data[s.start],
                    d = 0 === u ? 12 : 20,
                    c = l(s, d),
                    f = h(n, ["mdia", "mdhd"])[0];
                    if (f) {
                        var g = l(f, d = 0 === (u = f.data[f.start]) ? 12 : 20),
                        v = h(n, ["mdia", "hdlr"])[0];
                        if (v) {
                            var p = o(v.data.subarray(v.start + 8, v.start + 12)),
                            m = {
                                soun: a.a.AUDIO,
                                vide: a.a.VIDEO
                            } [p];
                            if (m) {
                                var y = h(n, ["mdia", "minf", "stbl", "stsd"])[0],
                                T = void 0;
                                y && (T = o(y.data.subarray(y.start + 12, y.start + 16))),
                                e[c] = {
                                    timescale: g,
                                    type: m
                                },
                                e[m] = {
                                    timescale: g,
                                    id: c,
                                    codec: T
                                }
                            }
                        }
                    }
                }
            }
            return h(t, ["moov", "mvex", "trex"]).forEach((function(t) {
                var r = l(t, 4),
                i = e[r];
                i && (i.
            default = {
                    duration: l(t, 12),
                    flags: l(t, 20)
                })
            })),
            e
        }
        function f(t, e) {
            return h(e, ["moof", "traf"]).reduce((function(e, r) {
                var i = h(r, ["tfdt"])[0],
                a = i.data[i.start],
                n = h(r, ["tfhd"]).reduce((function(e, r) {
                    var n = l(r, 4),
                    s = t[n];
                    if (s) {
                        var o = l(i, 4);
                        1 === a && (o *= Math.pow(2, 32), o += l(i, 8));
                        var u = o / (s.timescale || 9e4);
                        if (isFinite(u) && (null === e || u < e)) return u
                    }
                    return e
                }), null);
                return null !== n && isFinite(n) && (null === e || n < e) ? n: e
            }), null) || 0
        }
        function g(t, e) {
            for (var r = 0,
            i = 0,
            n = 0,
            s = h(t, ["moof", "traf"]), o = 0; o < s.length; o++) {
                var u = s[o],
                c = h(u, ["tfhd"])[0],
                f = e[l(c, 4)];
                if (f) {
                    var g = f.
                default,
                    p = l(c, 0) | (null == g ? void 0 : g.flags),
                    m = null == g ? void 0 : g.duration;
                    8 & p && (m = l(c, 2 & p ? 12 : 8));
                    for (var y = f.timescale || 9e4,
                    T = h(u, ["trun"]), E = 0; E < T.length; E++) {
                        if (m) r = m * l(T[E], 4);
                        else r = v(T[E]);
                        f.type === a.a.VIDEO ? i += r / y: f.type === a.a.AUDIO && (n += r / y)
                    }
                }
            }
            if (0 === i && 0 === n) {
                var b = d(t);
                if (null != b && b.references) return b.references.reduce((function(t, e) {
                    return t + e.info.duration || 0
                }), 0)
            }
            return i || n
        }
        function v(t) {
            var e = l(t, 0),
            r = 8;
            1 & e && (r += 4),
            4 & e && (r += 4);
            for (var i = 0,
            a = l(t, 4), n = 0; n < a; n++) {
                if (256 & e) i += l(t, r),
                r += 4;
                512 & e && (r += 4),
                1024 & e && (r += 4),
                2048 & e && (r += 4)
            }
            return i
        }
        function p(t, e, r) {
            h(e, ["moof", "traf"]).forEach((function(e) {
                h(e, ["tfhd"]).forEach((function(i) {
                    var a = l(i, 4),
                    s = t[a];
                    if (s) {
                        var o = s.timescale || 9e4;
                        h(e, ["tfdt"]).forEach((function(t) {
                            var e = t.data[t.start],
                            i = l(t, 4);
                            if (0 === e) u(t, 4, i - r * o);
                            else {
                                i *= Math.pow(2, 32),
                                i += l(t, 8),
                                i -= r * o,
                                i = Math.max(i, 0);
                                var a = Math.floor(i / (n + 1)),
                                s = Math.floor(i % (n + 1));
                                u(t, 4, a),
                                u(t, 8, s)
                            }
                        }))
                    }
                }))
            }))
        }
        function m(t) {
            var e = {
                valid: null,
                remainder: null
            },
            r = h(t, ["moof"]);
            if (!r) return e;
            if (r.length < 2) return e.remainder = t,
            e;
            var a = r[r.length - 1];
            return e.valid = Object(i.a)(t, 0, a.start - 8),
            e.remainder = Object(i.a)(t, a.start - 8),
            e
        }
        function y(t, e) {
            var r = new Uint8Array(t.length + e.length);
            return r.set(t),
            r.set(e, t.length),
            r
        }
    },
    6 : function(t, e, r) {
        "use strict";
        r.d(e, "b", (function() {
            return s
        })),
        r.d(e, "a", (function() {
            return l
        })),
        r.d(e, "d", (function() {
            return u
        })),
        r.d(e, "e", (function() {
            return h
        })),
        r.d(e, "c", (function() {
            return c
        })),
        r.d(e, "f", (function() {
            return y
        }));
        var i, a = function(t, e) {
            return e + 10 <= t.length && 73 === t[e] && 68 === t[e + 1] && 51 === t[e + 2] && t[e + 3] < 255 && t[e + 4] < 255 && t[e + 6] < 128 && t[e + 7] < 128 && t[e + 8] < 128 && t[e + 9] < 128
        },
        n = function(t, e) {
            return e + 10 <= t.length && 51 === t[e] && 68 === t[e + 1] && 73 === t[e + 2] && t[e + 3] < 255 && t[e + 4] < 255 && t[e + 6] < 128 && t[e + 7] < 128 && t[e + 8] < 128 && t[e + 9] < 128
        },
        s = function(t, e) {
            for (var r = e,
            i = 0; a(t, e);) {
                i += 10,
                i += o(t, e + 6),
                n(t, e + 10) && (i += 10),
                e += i
            }
            if (i > 0) return t.subarray(r, r + i)
        },
        o = function(t, e) {
            var r = 0;
            return r = (127 & t[e]) << 21,
            r |= (127 & t[e + 1]) << 14,
            r |= (127 & t[e + 2]) << 7,
            r |= 127 & t[e + 3]
        },
        l = function(t, e) {
            return a(t, e) && o(t, e + 6) + 10 <= t.length - e
        },
        u = function(t) {
            for (var e = c(t), r = 0; r < e.length; r++) {
                var i = e[r];
                if (h(i)) return m(i)
            }
        },
        h = function(t) {
            return t && "PRIV" === t.key && "com.apple.streaming.transportStreamTimestamp" === t.info
        },
        d = function(t) {
            var e = String.fromCharCode(t[0], t[1], t[2], t[3]),
            r = o(t, 4);
            return {
                type: e,
                size: r,
                data: t.subarray(10, 10 + r)
            }
        },
        c = function(t) {
            for (var e = 0,
            r = []; a(t, e);) {
                for (var i = o(t, e + 6), s = (e += 10) + i; e + 8 < s;) {
                    var l = d(t.subarray(e)),
                    u = f(l);
                    u && r.push(u),
                    e += l.size + 10
                }
                n(t, e) && (e += 10)
            }
            return r
        },
        f = function(t) {
            return "PRIV" === t.type ? g(t) : "W" === t.type[0] ? p(t) : v(t)
        },
        g = function(t) {
            if (! (t.size < 2)) {
                var e = y(t.data, !0),
                r = new Uint8Array(t.data.subarray(e.length + 1));
                return {
                    key: t.type,
                    info: e,
                    data: r.buffer
                }
            }
        },
        v = function(t) {
            if (! (t.size < 2)) {
                if ("TXXX" === t.type) {
                    var e = 1,
                    r = y(t.data.subarray(e), !0);
                    e += r.length + 1;
                    var i = y(t.data.subarray(e));
                    return {
                        key: t.type,
                        info: r,
                        data: i
                    }
                }
                var a = y(t.data.subarray(1));
                return {
                    key: t.type,
                    data: a
                }
            }
        },
        p = function(t) {
            if ("WXXX" === t.type) {
                if (t.size < 2) return;
                var e = 1,
                r = y(t.data.subarray(e), !0);
                e += r.length + 1;
                var i = y(t.data.subarray(e));
                return {
                    key: t.type,
                    info: r,
                    data: i
                }
            }
            var a = y(t.data);
            return {
                key: t.type,
                data: a
            }
        },
        m = function(t) {
            if (8 === t.data.byteLength) {
                var e = new Uint8Array(t.data),
                r = 1 & e[3],
                i = (e[4] << 23) + (e[5] << 15) + (e[6] << 7) + e[7];
                return i /= 45,
                r && (i += 47721858.84),
                Math.round(i)
            }
        },
        y = function(t, e) {
            void 0 === e && (e = !1);
            var r = T();
            if (r) {
                var i = r.decode(t);
                if (e) {
                    var a = i.indexOf("\0");
                    return - 1 !== a ? i.substring(0, a) : i
                }
                return i.replace(/\0/g, "")
            }
            for (var n, s, o, l = t.length,
            u = "",
            h = 0; h < l;) {
                if (0 === (n = t[h++]) && e) return u;
                if (0 !== n && 3 !== n) switch (n >> 4) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                    u += String.fromCharCode(n);
                    break;
                case 12:
                case 13:
                    s = t[h++],
                    u += String.fromCharCode((31 & n) << 6 | 63 & s);
                    break;
                case 14:
                    s = t[h++],
                    o = t[h++],
                    u += String.fromCharCode((15 & n) << 12 | (63 & s) << 6 | (63 & o) << 0)
                }
            }
            return u
        };
        function T() {
            return i || void 0 === self.TextDecoder || (i = new self.TextDecoder("utf-8")),
            i
        }
    },
    14 : function(t, e, r) {
        "use strict";
        r.d(e, "a", (function() {
            return u
        }));
        var i = function() {
            function t(t, e) {
                this.subtle = void 0,
                this.aesIV = void 0,
                this.subtle = t,
                this.aesIV = e
            }
            return t.prototype.decrypt = function(t, e) {
                return this.subtle.decrypt({
                    name: "AES-CBC",
                    iv: this.aesIV
                },
                e, t)
            },
            t
        } (),
        a = function() {
            function t(t, e) {
                this.subtle = void 0,
                this.key = void 0,
                this.subtle = t,
                this.key = e
            }
            return t.prototype.expandKey = function() {
                return this.subtle.importKey("raw", this.key, {
                    name: "AES-CBC"
                },
                !1, ["encrypt", "decrypt"])
            },
            t
        } (),
        n = r(8);
        var s = function() {
            function t() {
                this.rcon = [0, 1, 2, 4, 8, 16, 32, 64, 128, 27, 54],
                this.subMix = [new Uint32Array(256), new Uint32Array(256), new Uint32Array(256), new Uint32Array(256)],
                this.invSubMix = [new Uint32Array(256), new Uint32Array(256), new Uint32Array(256), new Uint32Array(256)],
                this.sBox = new Uint32Array(256),
                this.invSBox = new Uint32Array(256),
                this.key = new Uint32Array(0),
                this.ksRows = 0,
                this.keySize = 0,
                this.keySchedule = void 0,
                this.invKeySchedule = void 0,
                this.initTable()
            }
            var e = t.prototype;
            return e.uint8ArrayToUint32Array_ = function(t) {
                for (var e = new DataView(t), r = new Uint32Array(4), i = 0; i < 4; i++) r[i] = e.getUint32(4 * i);
                return r
            },
            e.initTable = function() {
                var t = this.sBox,
                e = this.invSBox,
                r = this.subMix,
                i = r[0],
                a = r[1],
                n = r[2],
                s = r[3],
                o = this.invSubMix,
                l = o[0],
                u = o[1],
                h = o[2],
                d = o[3],
                c = new Uint32Array(256),
                f = 0,
                g = 0,
                v = 0;
                for (v = 0; v < 256; v++) c[v] = v < 128 ? v << 1 : v << 1 ^ 283;
                for (v = 0; v < 256; v++) {
                    var p = g ^ g << 1 ^ g << 2 ^ g << 3 ^ g << 4;
                    p = p >>> 8 ^ 255 & p ^ 99,
                    t[f] = p,
                    e[p] = f;
                    var m = c[f],
                    y = c[m],
                    T = c[y],
                    E = 257 * c[p] ^ 16843008 * p;
                    i[f] = E << 24 | E >>> 8,
                    a[f] = E << 16 | E >>> 16,
                    n[f] = E << 8 | E >>> 24,
                    s[f] = E,
                    E = 16843009 * T ^ 65537 * y ^ 257 * m ^ 16843008 * f,
                    l[p] = E << 24 | E >>> 8,
                    u[p] = E << 16 | E >>> 16,
                    h[p] = E << 8 | E >>> 24,
                    d[p] = E,
                    f ? (f = m ^ c[c[c[T ^ m]]], g ^= c[c[g]]) : f = g = 1
                }
            },
            e.expandKey = function(t) {
                for (var e = this.uint8ArrayToUint32Array_(t), r = !0, i = 0; i < e.length && r;) r = e[i] === this.key[i],
                i++;
                if (!r) {
                    this.key = e;
                    var a = this.keySize = e.length;
                    if (4 !== a && 6 !== a && 8 !== a) throw new Error("Invalid aes key size=" + a);
                    var n, s, o, l, u = this.ksRows = 4 * (a + 6 + 1),
                    h = this.keySchedule = new Uint32Array(u),
                    d = this.invKeySchedule = new Uint32Array(u),
                    c = this.sBox,
                    f = this.rcon,
                    g = this.invSubMix,
                    v = g[0],
                    p = g[1],
                    m = g[2],
                    y = g[3];
                    for (n = 0; n < u; n++) n < a ? o = h[n] = e[n] : (l = o, n % a == 0 ? (l = c[(l = l << 8 | l >>> 24) >>> 24] << 24 | c[l >>> 16 & 255] << 16 | c[l >>> 8 & 255] << 8 | c[255 & l], l ^= f[n / a | 0] << 24) : a > 6 && n % a == 4 && (l = c[l >>> 24] << 24 | c[l >>> 16 & 255] << 16 | c[l >>> 8 & 255] << 8 | c[255 & l]), h[n] = o = (h[n - a] ^ l) >>> 0);
                    for (s = 0; s < u; s++) n = u - s,
                    l = 3 & s ? h[n] : h[n - 4],
                    d[s] = s < 4 || n <= 4 ? l: v[c[l >>> 24]] ^ p[c[l >>> 16 & 255]] ^ m[c[l >>> 8 & 255]] ^ y[c[255 & l]],
                    d[s] = d[s] >>> 0
                }
            },
            e.networkToHostOrderSwap = function(t) {
                return t << 24 | (65280 & t) << 8 | (16711680 & t) >> 8 | t >>> 24
            },
            e.decrypt = function(t, e, r) {
                for (var i, a, n, s, o, l, u, h, d, c, f, g, v, p, m = this.keySize + 6,
                y = this.invKeySchedule,
                T = this.invSBox,
                E = this.invSubMix,
                b = E[0], S = E[1], L = E[2], A = E[3], R = this.uint8ArrayToUint32Array_(r), D = R[0], k = R[1], _ = R[2], I = R[3], C = new Int32Array(t), w = new Int32Array(C.length), O = this.networkToHostOrderSwap; e < C.length;) {
                    for (d = O(C[e]), c = O(C[e + 1]), f = O(C[e + 2]), g = O(C[e + 3]), o = d ^ y[0], l = g ^ y[1], u = f ^ y[2], h = c ^ y[3], v = 4, p = 1; p < m; p++) i = b[o >>> 24] ^ S[l >> 16 & 255] ^ L[u >> 8 & 255] ^ A[255 & h] ^ y[v],
                    a = b[l >>> 24] ^ S[u >> 16 & 255] ^ L[h >> 8 & 255] ^ A[255 & o] ^ y[v + 1],
                    n = b[u >>> 24] ^ S[h >> 16 & 255] ^ L[o >> 8 & 255] ^ A[255 & l] ^ y[v + 2],
                    s = b[h >>> 24] ^ S[o >> 16 & 255] ^ L[l >> 8 & 255] ^ A[255 & u] ^ y[v + 3],
                    o = i,
                    l = a,
                    u = n,
                    h = s,
                    v += 4;
                    i = T[o >>> 24] << 24 ^ T[l >> 16 & 255] << 16 ^ T[u >> 8 & 255] << 8 ^ T[255 & h] ^ y[v],
                    a = T[l >>> 24] << 24 ^ T[u >> 16 & 255] << 16 ^ T[h >> 8 & 255] << 8 ^ T[255 & o] ^ y[v + 1],
                    n = T[u >>> 24] << 24 ^ T[h >> 16 & 255] << 16 ^ T[o >> 8 & 255] << 8 ^ T[255 & l] ^ y[v + 2],
                    s = T[h >>> 24] << 24 ^ T[o >> 16 & 255] << 16 ^ T[l >> 8 & 255] << 8 ^ T[255 & u] ^ y[v + 3],
                    w[e] = O(i ^ D),
                    w[e + 1] = O(s ^ k),
                    w[e + 2] = O(n ^ _),
                    w[e + 3] = O(a ^ I),
                    D = d,
                    k = c,
                    _ = f,
                    I = g,
                    e += 4
                }
                return w.buffer
            },
            t
        } (),
        o = r(1),
        l = r(5),
        u = function() {
            function t(t, e, r) {
                var i = (void 0 === r ? {}: r).removePKCS7Padding,
                a = void 0 === i || i;
                if (this.logEnabled = !0, this.observer = void 0, this.config = void 0, this.removePKCS7Padding = void 0, this.subtle = null, this.softwareDecrypter = null, this.key = null, this.fastAesKey = null, this.remainderData = null, this.currentIV = null, this.currentResult = null, this.observer = t, this.config = e, this.removePKCS7Padding = a, a) try {
                    var n = self.crypto;
                    n && (this.subtle = n.subtle || n.webkitSubtle)
                } catch(t) {}
                null === this.subtle && (this.config.enableSoftwareAES = !0)
            }
            var e = t.prototype;
            return e.destroy = function() {
                this.observer = null
            },
            e.isSync = function() {
                return this.config.enableSoftwareAES
            },
            e.flush = function() {
                var t = this.currentResult;
                if (t) {
                    var e, r, i, a = new Uint8Array(t);
                    return this.reset(),
                    this.removePKCS7Padding ? (r = (e = a).byteLength, (i = r && new DataView(e.buffer).getUint8(r - 1)) ? Object(n.a)(e, 0, r - i) : e) : a
                }
                this.reset()
            },
            e.reset = function() {
                this.currentResult = null,
                this.currentIV = null,
                this.remainderData = null,
                this.softwareDecrypter && (this.softwareDecrypter = null)
            },
            e.decrypt = function(t, e, r, i) {
                if (this.config.enableSoftwareAES) {
                    this.softwareDecrypt(new Uint8Array(t), e, r);
                    var a = this.flush();
                    a && i(a.buffer)
                } else this.webCryptoDecrypt(new Uint8Array(t), e, r).then(i)
            },
            e.softwareDecrypt = function(t, e, r) {
                var i = this.currentIV,
                a = this.currentResult,
                o = this.remainderData;
                this.logOnce("JS AES decrypt"),
                o && (t = Object(l.a)(o, t), this.remainderData = null);
                var u = this.getValidChunk(t);
                if (!u.length) return null;
                i && (r = i);
                var h = this.softwareDecrypter;
                h || (h = this.softwareDecrypter = new s),
                h.expandKey(e);
                var d = a;
                return this.currentResult = h.decrypt(u.buffer, 0, r),
                this.currentIV = Object(n.a)(u, -16).buffer,
                d || null
            },
            e.webCryptoDecrypt = function(t, e, r) {
                var n = this,
                s = this.subtle;
                return this.key === e && this.fastAesKey || (this.key = e, this.fastAesKey = new a(s, e)),
                this.fastAesKey.expandKey().then((function(e) {
                    return s ? new i(s, r).decrypt(t.buffer, e) : Promise.reject(new Error("web crypto not initialized"))
                })).
                catch((function(i) {
                    return n.onWebCryptoError(i, t, e, r)
                }))
            },
            e.onWebCryptoError = function(t, e, r, i) {
                return o.b.warn("[decrypter.ts]: WebCrypto Error, disable WebCrypto API:", t),
                this.config.enableSoftwareAES = !0,
                this.logEnabled = !0,
                this.softwareDecrypt(e, r, i)
            },
            e.getValidChunk = function(t) {
                var e = t,
                r = t.length - t.length % 16;
                return r !== t.length && (e = Object(n.a)(t, 0, r), this.remainderData = Object(n.a)(t, r)),
                e
            },
            e.logOnce = function(t) {
                this.logEnabled && (o.b.log("[decrypter.ts]: " + t), this.logEnabled = !1)
            },
            t
        } ()
    }
}))(self);