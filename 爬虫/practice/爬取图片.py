
# 爬取彼岸桌面壁纸：http://www.netbian.com/index.htm
import requests
from bs4 import BeautifulSoup


# 爬取模块：4k美女图片：https://pic.netbian.com/4kmeinv/index.html
# https://pic.netbian.com/4kmeinv/index_2.html
domin = "https://pic.netbian.com"
all_encoding = "gbk"
for page in range(1,20):
    girl_index_url = "https://pic.netbian.com/4kmeinv/index.html"
    if page > 1:
        girl_index_url = f'https://pic.netbian.com/4kmeinv/index_{page}.html'
    # 模拟人工登陆的cookie
    had = {
        "cookie":"__yjs_duid=1_ea7d8f8660c2f703dbe28bbe54fae45b1620455790088; Hm_lvt_14b14198b6e26157b7eba06b390ab763=1620455790,1620455861; Hm_lpvt_14b14198b6e26157b7eba06b390ab763=1620456022; Hm_lvt_526caf4e20c21f06a4e9209712d6a20e=1620455800,1620456137; zkhanecookieclassrecord=%2C66%2C54%2C53%2C; PHPSESSID=ghna84jfmdma26tjg3evjfa361; zkhanmlusername=qq_Pluto298; zkhanmluserid=4901508; zkhanmlgroupid=1; zkhanmlrnd=qbJZBuIhNCelkwoaqtDz; zkhanmlauth=75d96a6b3a3b0bc84a2aa03d1b126dd9; Hm_lpvt_526caf4e20c21f06a4e9209712d6a20e=1620456411",
        "sec-fetch-dest": "iframe",
        "sec-fetch-mode": "navigate",
        "sec-fetch-site": "same-origin",
        "upgrade-insecure-requests": "1",
        "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36"
    }
    index_resp = requests.get(girl_index_url,headers=had)
    index_resp.encoding = all_encoding
    index_soup = BeautifulSoup(index_resp.text,"html.parser")
    div = index_soup.find("div",class_ = "slist")
    li_list = div.find_all("li")
    for li in li_list:
        # find标签，get属性  .text内容
        li_url = li.find("a").get("href")
        img_id =li_url.split(".")[0].split("/")[-1]  #图片对应ID，下载高清图片使用
        # print(li.find("a").get("href")) # https://pic.netbian.com/tupian/27351.html   /tupian/27351.html
        pic_resp = requests.get(domin + li_url,headers=had)
        pic_resp.encoding = all_encoding
        pic_soup = BeautifulSoup(pic_resp.text,"html.parser")
        pic_src = pic_soup.find("div",class_ = "photo-pic").find("img").get("src")
        print(pic_src)
        download_url = domin + pic_src # 拼域名
        download_resp = requests.get(download_url,headers = had)
        with open("img/" + str(pic_src).split("/")[-1],mode="wb") as f:
            f.write(download_resp.content)
    index_resp.close()
    download_resp.close()

