
from selenium.webdriver import Chrome
from chaojiying import Chaojiying_Client
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
import time
import configparser


# 读取local.ini文件
config = configparser.ConfigParser()
config.read("local.ini",'utf-8')
# 超级鹰配置
cjy_user=config.get("chaojiying","user")
cjy_password=config.get("chaojiying","password")
cjy_12306code=config.get("chaojiying","12306_code")
# 12306配置
train_user=config.get("train_login","user")
train_password=config.get("train_login","password")

option = Options()
option.add_argument("--disable-blink-features=AutomationControlled")
web = Chrome(options = option)
web.get("https://kyfw.12306.cn/otn/resources/login.html")
time.sleep(2)

web.find_element_by_xpath("/html/body/div[2]/div[2]/ul/li[2]/a").click()
web.find_element_by_xpath('//*[@id="J-userName"]').send_keys(train_user)
web.find_element_by_xpath('//*[@id="J-password"]').send_keys(train_password)
code_element = web.find_element_by_xpath('//*[@id="J-loginImg"]')
time.sleep(2)
# 处理验证码
chaojiying = Chaojiying_Client(cjy_user, cjy_password, cjy_12306code)
dic = chaojiying.PostPic(code_element.screenshot_as_png,9004)
print(dic)
# 解析坐标
pic_str = dic["pic_str"]
str_list = pic_str.split("|")
for i in str_list:
    p_temp = i.split(",")
    x = int(p_temp[0])
    y = int(p_temp[1])
    # 鼠标事件 必须加perform 相当于提交这次操作
    ActionChains(web).move_to_element_with_offset(code_element,x,y).click().perform()

# 点击登陆
web.find_element_by_xpath('//*[@id="J-login"]').click()

time.sleep(2)
wrapper_but = web.find_element_by_xpath('//*[@id="nc_1_n1z"]')
ActionChains(web).drag_and_drop_by_offset(wrapper_but,300,0).perform()

