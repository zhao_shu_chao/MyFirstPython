

from selenium.webdriver import Chrome
import time
from selenium.webdriver.chrome.options import Options
# 基本操作博客：https://blog.csdn.net/weixin_36279318/article/details/79475388
'''
定位一个元素	定位多个元素	含义
find_element_by_id	find_elements_by_id	通过元素id定位
find_element_by_name	find_elements_by_name	通过元素name定位
find_element_by_xpath	find_elements_by_xpath	通过xpath表达式定位
find_element_by_link_text	find_elements_by_link_tex	通过完整超链接定位
find_element_by_partial_link_text	find_elements_by_partial_link_text	通过部分链接定位
find_element_by_tag_name	find_elements_by_tag_name	通过标签定位
find_element_by_class_name	find_elements_by_class_name	通过类名进行定位
find_elements_by_css_selector	find_elements_by_css_selector	通过css选择器进行定位

'''
'''
#将滚动条移动到页面的底部
js="var q=document.documentElement.scrollTop=100000"  
driver.execute_script(js)  
time.sleep(3)  
#将滚动条移动到页面的顶部  
js="var q=document.documentElement.scrollTop=0"  
driver.execute_script(js)  
time.sleep(3)  
#若要对页面中的内嵌窗口中的滚动条进行操作，要先定位到该内嵌窗口，在进行滚动条操作 
js="var q=document.getElementById('id').scrollTop=100000" 
driver.execute_script(js) 
'''

web = Chrome()
web.get("https://jianshu.com")
# 通过xpath定位一个元素
# click()点击
# web.find_element_by_xpath('//*[@id="note-85880445"]/div/a').click()
# 通过超链接的value点击进入
# web.find_element_by_link_text("真实故事").click()
# web.find_element_by_partial_link_text("真实").click
# web.refresh()

# 配置无头浏览器，也就是后台运行，不展示窗口
# opt = Options()
# opt.add_argument("--headless")
# opt.add_argument("--disbale-gpu")

# 循环几次下拉
for i  in range(3):
    js = "var q=document.documentElement.scrollTop=100000"
    web.execute_script(js)
    time.sleep(1)

time.sleep(1)

# 下拉到最后要回到最上面，不然将会报找不到该元素异常
js1="var q=document.documentElement.scrollTop=0"
web.execute_script(js1)
# 获取简书窗口的句柄
jianshu_domain_windows = web.current_window_handle
# 抓取简书首页数据：
title_list = web.find_elements_by_xpath('//*[@id="list-container"]/ul/li')
for title in title_list:
    title_name = title.find_element_by_class_name("title").text
    print(title_name)
    title.find_element_by_class_name("title").click()
    # 切换窗口
    web.switch_to_window(web.window_handles[-1])
    # 抓取数据
    p_list = web.find_elements_by_xpath('//*[@id="__next"]/div[1]/div/div/section[1]/article/p')
    # 把数据写入txt文件
    with open(f"file/{title_name}.txt",mode="a+",encoding="utf-8") as f:
        for p in p_list:
            f.write(p.text)
            f.write('\r\n')
    #关闭当前窗口
    web.close()
    web.switch_to_window(web.window_handles[0])

    time.sleep(2)

