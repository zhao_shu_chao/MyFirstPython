import aiohttp
import asyncio


urls = [
    "https://pic.netbian.com/uploads/allimg/210419/203429-1618835669e1dd.jpg",
    "https://pic.netbian.com/uploads/allimg/210419/172907-16188245474148.jpg",
    "https://pic.netbian.com/uploads/allimg/210419/164036-16188216361725.jpg"
]
# 下载图片
async def downLoadImg(url):
    name = url.rsplit("/",1)[1]
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as resp:
            with open("img2/"+ name,mode="wb") as fo:
                fo.write(await resp.content.read())
    print("搞定")


async def main():
    tasks = []
    for url in urls:
        tasks.append(downLoadImg(url))

    await asyncio.wait(tasks)


if __name__ == '__main__':
    asyncio.get_event_loop().run_until_complete(main())

