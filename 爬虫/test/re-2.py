

import re

str = '''
<div class='Jay'><span id='1'>郭麒麟</span></div>
<div class='JJ'><span id='2'>宋铁</span></div>
<div class='VAE'><span id='3'>大聪明</span></div>
<div class='GEM'><span id='4'>范思哲</span></div>
'''
#  (?P<group_name>正则) 可以单独从正则中进一步提取
obj = re.compile(r"<div class='.*?'><span id='(?P<id>\d)+'>(?P<name>.*?)</span></div>",re.S)

it = obj.finditer(str)
for x in it:
    print(x.group("name"))
    print(x.group("id"))