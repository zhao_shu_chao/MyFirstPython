

# 正则模块
import re

# findall:全部匹配，返回一个list
result = re.findall(r"\d+","我的电话号码是10086，你的电话是10010")
print(result)


# finditer:全部匹配，返回一个迭代器
result = re.finditer(r"\d+","我的电话号码是10086，你的电话是10010")
for x in result:
    print(x.group())

# 找到第一个
result = re.search(r"\d+","我的电话号码是10086，你的电话是10010")
print(result.group())

# 开头是不是
result = re.match(r"\d+","10086，你的电话是10010")
print(result.group())

print("****************************")
# 预加载正则
object = re.compile(r"\d+")
list = object.findall("我的电话号码是10086，你的电话是10010")
print(list)






