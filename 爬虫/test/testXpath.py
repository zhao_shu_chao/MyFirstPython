import requests
from bs4 import BeautifulSoup
from lxml import etree


parser = etree.HTMLParser(encoding="utf-8")
tree = etree.parse("test.html", parser=parser)
result = tree.xpath("/html/body/div/ol/li[1]/text()")
print(result)