# py中的协程
import asyncio
import time


async def func1():
    print("你好，我是王建国")
    await asyncio.sleep(3)
    print("你好，我是王建国")


async def func2():
    print("你好，我是李雪琴")
    await asyncio.sleep(2)
    print("你好，我是李雪琴")


async def func3():
    print("你好，我是大池子")
    await asyncio.sleep(4)
    print("你好，我是大池子")


async def main():
    tasks = [func1(),func2(),func3()]
    await asyncio.wait(tasks)

if __name__ == '__main__':
    # 1. 直接在main中启动
    # f1 = func1()
    # f2 = func2()
    # f3 = func3()
    # # 将三个任务一起启动
    # tasks = [f1,f2,f3]
    # t1 = time.time()
    # loop = asyncio.get_event_loop()
    # result = loop.run_until_complete(asyncio.wait(tasks))
    # print(result)
    # t2 = time.time()
    # print(t2 - t1)

    # 写进函数启动
    asyncio.get_event_loop().run_until_complete(main())


'''
py3.7以上才支持asyncio.run形式启动多任务
3.6只能采取：
loop = asyncio.get_event_loop()
result = loop.run_until_complete(asyncio.wait(tasks))
'''
