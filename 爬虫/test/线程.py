from threading import Thread

# 创建线程的两种方式

# def func():
#     for i in range(100):
#         print("线程：" , i)


# 创建一个类继承Thread
class MyThread(Thread):
    def run(self):
        for i in range(100):
            print("线程：", i)

if __name__ == '__main__':
    # t1 = Thread(target=func)
    # t1.start()
    t =  MyThread()
    t.start();

    for y in range(100):
        print("main:", y)

