from concurrent .futures import ThreadPoolExecutor,ProcessPoolExecutor

def fn(name):
    for i in range(100):
        print(name,i)

if __name__ == '__main__':
    with ThreadPoolExecutor(50) as th:
        for i in range(100):
            th.submit(fn, name=f"线程{i}")